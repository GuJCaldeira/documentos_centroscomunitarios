-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 10-Maio-2018 às 01:16
-- Versão do servidor: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_cc`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `associacao`
--

CREATE TABLE `associacao` (
  `id_associacao` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A' COMMENT 'A = Ativo | I = Initivo',
  `data_criacao` datetime NOT NULL,
  `data_alteracao` datetime NULL,
  `nome_fantasia` varchar(150) NULL,
  `razao_social` varchar(150) NULL,
  `cnpj` varchar(14)  NULL,
  `telefone1` varchar(16) NOT NULL,
  `telefone2` varchar(16) NULL,
  `data_constituicao` datetime NULL,
  `finalidade_estatutaria` text NULL,
  `email` varchar(80) NOT NULL,
  `detalhes` text NULL,
  `cep` varchar(9) DEFAULT NULL,
  `endereco` varchar(150) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `estado` char(2) DEFAULT NULL,
  `complemento` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `auth_sessions`
--

CREATE TABLE `auth_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `expiration_timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cargo`
--

CREATE TABLE `cargo` (
  `id_cargo` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime NOT NULL,
  `data_alteracao` datetime NULL,
  `nome_cargo` varchar(50) NOT NULL,
  `detalhes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `centro_comunitario`
--

CREATE TABLE `centro_comunitario` (
  `id_centro_comunitario` int(11) NOT NULL,
  `status` char(1) DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `nome_centro` varchar(250) DEFAULT NULL,
  `telefone1` varchar(16) DEFAULT NULL,
  `telefone2` varchar(16) DEFAULT NULL,
  `detalhes` text,
  `cep` varchar(9) DEFAULT NULL,
  `endereco` varchar(150) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `estado` char(2) DEFAULT NULL,
  `complemento` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `denuncia`
--

CREATE TABLE `denuncia` (
  `id_denuncia` int(11) NOT NULL,
  `status` char(1) DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `destino` char(1) DEFAULT NULL COMMENT '1 = Associação | 2 = Prefeitura | 3 = Ambos',
  `descricao` text,
  `nome` varchar(150) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `anexo` longblob,
  `detalhes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `evento`
--

CREATE TABLE `evento` (
  `id_evento` int(11) NOT NULL,
  `status` char(1) DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `nome_evento` varchar(150) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `data_inicio` datetime DEFAULT NULL,
  `data_final` datetime DEFAULT NULL,
  `tipo_evento` char(2) COMMENT 'PU = Publico | PV = Privado',
  `recorrencia` tinyint(1) DEFAULT NULL,
  `tipo_recorencia` char(1) DEFAULT NULL,
  `id_responsavel` int(11) DEFAULT NULL,
  `dados_evento` text,
  `valor` decimal(10,0) DEFAULT NULL,
  `id_centro_comunitario` int(11) DEFAULT NULL,
  `detalhes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `gestao`
--

CREATE TABLE `gestao` (
  `id_gestao` int(11) NOT NULL,
  `status` char(1) DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `id_associacao` int(11) DEFAULT NULL,
  `id_centro_comunitario` int(11) DEFAULT NULL,
  `data_inicio` datetime DEFAULT NULL,
  `data_final` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `postagem`
--

CREATE TABLE `postagem` (
  `id_postagem` int(11) NOT NULL,
  `status` char(1) DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `id_centro_comunitario` int(11) DEFAULT NULL,
  `titulo` varchar(150) DEFAULT NULL,
  `imagem` longblob,
  `descricao` text,
  `id_usuario` int(11) DEFAULT NULL,
  `destaque` char(1) DEFAULT NULL,
  `detalhes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `responsavel`
--

CREATE TABLE `responsavel` (
  `id_responsavel` int(11) NOT NULL,
  `status` char(1) DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `cpf` varchar(14) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `telefone1` varchar(16) DEFAULT NULL,
  `telefone2` varchar(16) DEFAULT NULL,
  `cep` varchar(9) DEFAULT NULL,
  `endereco` varchar(150) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `estado` char(2) DEFAULT NULL,
  `complemento` varchar(150) DEFAULT NULL,
  `detalhes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `status` char(1) DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `nome` varchar(150) DEFAULT NULL,
  `data_nasc` datetime DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `telefone1` varchar(16) DEFAULT NULL,
  `telefone2` varchar(16) DEFAULT NULL,
  `foto` longblob,
  `id_associacao` int(11) DEFAULT NULL,
  `id_cargo` int(11) DEFAULT NULL,
  `data_admissao` datetime DEFAULT NULL,
  `data_demissao` datetime DEFAULT NULL,
  `detalhes` text,
  `cep` varchar(9) DEFAULT NULL,
  `endereco` varchar(150) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `estado` char(2) DEFAULT NULL,
  `complemento` varchar(150) DEFAULT NULL,
  `senha` varchar(100) DEFAULT NULL,
  `cpf` varchar(14) DEFAULT NULL,
  `sexo` char(1) COMMENT 'M = Masculino | F = Feminino | O = Outros'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `status`, `data_criacao`, `data_alteracao`, `nome`, `data_nasc`, `email`, `telefone1`, `telefone2`, `foto`, `id_associacao`, `id_cargo`, `data_admissao`, `data_demissao`, `cep`, `endereco`, `numero`, `bairro`, `cidade`, `estado`, `senha`, `cpf`) VALUES
(1, 'A', '2018-05-08 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$4t13l5W5jCkAOLXMy/PCoOxQr6Qd5b9My7y.fPqYQmvXY/WVR9wZG', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `associacao`
--
ALTER TABLE `associacao`
  ADD PRIMARY KEY (`id_associacao`);

--
-- Indexes for table `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id_cargo`);

--
-- Indexes for table `centro_comunitario`
--
ALTER TABLE `centro_comunitario`
  ADD PRIMARY KEY (`id_centro_comunitario`);

--
-- Indexes for table `denuncia`
--
ALTER TABLE `denuncia`
  ADD PRIMARY KEY (`id_denuncia`);

--
-- Indexes for table `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`id_evento`),
  ADD KEY `id_responsavel` (`id_responsavel`),
  ADD KEY `id_centro_comunitario` (`id_centro_comunitario`);

--
-- Indexes for table `gestao`
--
ALTER TABLE `gestao`
  ADD PRIMARY KEY (`id_gestao`),
  ADD KEY `id_associacao` (`id_associacao`),
  ADD KEY `id_centro_comunitario` (`id_centro_comunitario`);

--
-- Indexes for table `postagem`
--
ALTER TABLE `postagem`
  ADD PRIMARY KEY (`id_postagem`),
  ADD KEY `id_centro_comunitario` (`id_centro_comunitario`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `responsavel`
--
ALTER TABLE `responsavel`
  ADD PRIMARY KEY (`id_responsavel`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_associacao` (`id_associacao`),
  ADD KEY `id_cargo` (`id_cargo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `associacao`
--
ALTER TABLE `associacao`
  MODIFY `id_associacao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id_cargo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `centro_comunitario`
--
ALTER TABLE `centro_comunitario`
  MODIFY `id_centro_comunitario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `denuncia`
--
ALTER TABLE `denuncia`
  MODIFY `id_denuncia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `evento`
--
ALTER TABLE `evento`
  MODIFY `id_evento` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gestao`
--
ALTER TABLE `gestao`
  MODIFY `id_gestao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `postagem`
--
ALTER TABLE `postagem`
  MODIFY `id_postagem` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `responsavel`
--
ALTER TABLE `responsavel`
  MODIFY `id_responsavel` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `evento`
--
ALTER TABLE `evento`
  ADD CONSTRAINT `evento_ibfk_1` FOREIGN KEY (`id_centro_comunitario`) REFERENCES `centro_comunitario` (`id_centro_comunitario`),
  ADD CONSTRAINT `evento_ibfk_2` FOREIGN KEY (`id_responsavel`) REFERENCES `responsavel` (`id_responsavel`);

--
-- Limitadores para a tabela `gestao`
--
ALTER TABLE `gestao`
  ADD CONSTRAINT `gestao_ibfk_1` FOREIGN KEY (`id_associacao`) REFERENCES `associacao` (`id_associacao`),
  ADD CONSTRAINT `gestao_ibfk_2` FOREIGN KEY (`id_centro_comunitario`) REFERENCES `centro_comunitario` (`id_centro_comunitario`);

--
-- Limitadores para a tabela `postagem`
--
ALTER TABLE `postagem`
  ADD CONSTRAINT `postagem_ibfk_1` FOREIGN KEY (`id_centro_comunitario`) REFERENCES `centro_comunitario` (`id_centro_comunitario`),
  ADD CONSTRAINT `postagem_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_associacao`) REFERENCES `associacao` (`id_associacao`),
  ADD CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`id_cargo`) REFERENCES `cargo` (`id_cargo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



CREATE TABLE `grupo` (
  `id_grupo` INT(11) AUTO_INCREMENT,
  `nome_grupo` VARCHAR(300),
  `id_associacao` INT(11),
  `json` BLOB,
  `data_criacao` BIGINT(22),
  `data_alteracao` datetime DEFAULT NULL,
  `status` CHAR(2),
  CONSTRAINT Pk_grupo_idgrupo PRIMARY KEY(id_grupo)
);


CREATE TABLE `usuario_grupo` (
  `id_usuario_grupo` INT(11) AUTO_INCREMENT,
  `id_usuario` INT(11),
  `id_grupo` INT(11),
  `data_criacao` BIGINT(22),
  `data_alteracao` datetime DEFAULT NULL,
  `status` CHAR(2),
  CONSTRAINT Pk_usuariogrupo_idusuariogrupo PRIMARY KEY(id_usuario_grupo),
  CONSTRAINT Fk_usuariogrupo_idusuario FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario),
  CONSTRAINT Fk_usuariogrupo_idgrupo FOREIGN KEY(id_grupo) REFERENCES grupo(id_grupo)
);

CREATE TABLE `conta` (
  `id_conta` INT(11) AUTO_INCREMENT,
  `nome_conta` VARCHAR(300),
  `id_associacao` INT(11),
  `status` char(1) DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  CONSTRAINT Pk_conta_idconta PRIMARY KEY(id_conta)
);

CREATE TABLE `caixa` (
  `id_caixa` INT(11) AUTO_INCREMENT,
  `id_usuario` INT(11),
  `id_associacao` INT(11),
  `id_conta` INT(11),
  `descricao` VARCHAR(300),
  `data_lancamento` BIGINT(22),
  `movimento` CHAR(1) COMMENT 'E = Entrada | S = Saida',
  `valor` DECIMAL(10,2),
  `data_criacao` datetime DEFAULT NULL,
  CONSTRAINT Pk_caixa_idcaixa PRIMARY KEY(id_caixa),
  CONSTRAINT Fk_caixa_idusuario FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario),
  CONSTRAINT Fk_caixa_idassociacao FOREIGN KEY(id_associacao) REFERENCES associacao(id_associacao),
  CONSTRAINT Fk_caixa_idconta FOREIGN KEY(id_conta) REFERENCES conta(id_conta)
);




ALTER TABLE `evento` ADD `locacao_evento` CHAR(1) DEFAULT 'E' AFTER `status`; 

ALTER TABLE `grupo` CHANGE `data_criacao` `data_criacao` DATETIME NULL DEFAULT NULL; 

ALTER TABLE `usuario` ADD `id_grupo` INT(11) AFTER `id_associacao`; 

ALTER TABLE `cargo` ADD `id_associacao` INT(11) AFTER `id_cargo`; 
ALTER TABLE `responsavel` ADD `id_associacao` INT(11) AFTER `id_responsavel`; 


CREATE TABLE `postagem` (
  `id_postagem` INT(11) AUTO_INCREMENT,
  `status` char(1) DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `id_centro_comunitario` INT(11),
  `id_usuario` INT(11),
  `titulo` varchar(300),
  `destaque` char(1),
  `menu` char(1),
  `conteudo` TEXT,
  `id_postagem_categoria` INT(11) DEFAULT NULL,
  CONSTRAINT Pk_postagem_idpostagem PRIMARY KEY(id_postagem)
);

CREATE TABLE `postagem_uploads` (
  `id_postagem_upload` INT(11) AUTO_INCREMENT,
  `id_postagem` INT(11),
  `status` char(1) DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `filename` VARCHAR(300),
  `tipo` CHAR(1),
  CONSTRAINT Pk_postagemuploads_idpostagemupload PRIMARY KEY(id_postagem_upload)
);


CREATE TABLE `documento` (
  `id_documento` INT(11) AUTO_INCREMENT,
  `status` char(1) DEFAULT 'A' COMMENT 'A = Ativo | I = Inativo',
  `data_criacao` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `nome_arquivo` VARCHAR(300),
  `documento` longblob,
  `filename` varchar(300),
  `id_usuario` int(11),

  CONSTRAINT Pk_documentos_iddocumento PRIMARY KEY(id_documento)
)

ALTER TABLE `associacao` ADD `saldo` decimal(10,2) DEFAULT 0 AFTER `complemento`; 
