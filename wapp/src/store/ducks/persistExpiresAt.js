export const Types = {
  SET_EXPIRE: 'persistExpiresAt/SET_EXPIRE'
}

const INITIAL_STATE = ''

export default function persistExpiresAt(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.SET_EXPIRE:
      return action.payload
    default:
      return state
  }
}

export const Creators = {
  setPersistExpiresAt: data => ({
    payload: data
  })
}
