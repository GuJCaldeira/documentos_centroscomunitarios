export const Types = {
  CHANGE_CENTRO: 'centro/CHANGE_CENTRO',
  CENTROS_REQUEST: 'auth/CENTROS_REQUEST',
  CENTROS_SUCCESS: 'auth/CENTROS_SUCCESS',
  CENTROS_FAILURE: 'auth/CENTROS_FAILURE'
}

const INITIAL_STATE = {
  centroSelecionado: null,
  centros: [],
  loading: false,
  error: null
}

export default function centro(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.CHANGE_CENTRO:
      return { ...state, centroSelecionado: action.payload }
    case Types.CENTROS_REQUEST:
      return { ...state, loading: true }
    case Types.CENTROS_SUCCESS:
      return {
        ...state,
        loading: false,
        centros: action.payload
      }
    case Types.CENTROS_FAILURE:
      return { ...state, loading: false, error: action.payload.error }
    default:
      return state
  }
}

export const Creators = {
  changeCentro: data => ({
    type: Types.CHANGE_CENTRO,
    payload: data
  }),
  centrosRequest: () => ({
    type: Types.CENTROS_REQUEST
  }),
  centrosSuccess: action => ({
    type: Types.CENTROS_SUCCESS,
    payload: action.payload
  }),
  centrosFailure: action => ({
    type: Types.CENTROS_FAILURE,
    error: action.error
  })
}
