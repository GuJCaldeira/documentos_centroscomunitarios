import { call, put } from 'redux-saga/effects'

import axiosClient from 'helpers/axiosClient'

import { Creators as CentroActions } from '../ducks/centro'

const getCentros = () => {
  return axiosClient.post('/site/centrocomunitario/listar')
}

export function* centrosRequest(action) {
  try {
    const response = yield call(getCentros)

    if (response.data.status) {
      yield put(
        CentroActions.centrosSuccess({
          payload: response.data.obj.centros
        })
      )
    } else {
      yield put(
        CentroActions.centrosFailure(
          response.data.erro ||
            'Falha ao carregar os centros comunitários, por favor, tente novamente.'
        )
      )
    }
  } catch (err) {
    yield put(
      CentroActions.centrosFailure(
        err ||
          'Falha ao carregar os centros comunitários, por favor, tente novamente.'
      )
    )
  }
}
