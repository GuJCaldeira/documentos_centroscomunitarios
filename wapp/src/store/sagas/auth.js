import { call, put, all } from 'redux-saga/effects'

import axiosClient from 'helpers/axiosClient'

import { Creators as AuthActions } from '../ducks/auth'
import { Creators as ProfileActions } from '../ducks/profile'

const loginEndpoint = data => {
  return axiosClient.post('/auth/login', data)
}

export function* login(action) {
  try {
    const response = yield call(loginEndpoint, action.payload)

    if (response.data.status) {
      const { sessao, expiration_timestamp, usuario } = response.data.obj
      axiosClient.defaults.headers.common['Authorization'] = `Bearer ${sessao}`
      yield all([
        put(
          AuthActions.loginSuccess({
            token: sessao,
            expire: expiration_timestamp * 1000
          })
        ),
        put(ProfileActions.setProfileData(usuario))
      ])
    } else {
      yield put(
        AuthActions.loginFailure(
          response.data.erro || 'Falha no login, por favor, tente novamente.'
        )
      )
    }
  } catch (err) {
    yield put(
      AuthActions.loginFailure(
        err.response
          ? err.response.data['msg']
          : 'Houve algum problema de conexão, por favor, verifique sua internet e tente novamente.'
      )
    )
  }
}
