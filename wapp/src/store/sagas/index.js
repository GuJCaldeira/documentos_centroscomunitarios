import { all, takeLatest } from 'redux-saga/effects'
import { REHYDRATE } from 'redux-persist/lib/constants'

import { Types as AuthTypes } from '../ducks/auth'
import { Types as CentroTypes } from '../ducks/centro'

import { login } from './auth'
import { centrosRequest } from './centro'
import { rehydrate } from './persist'

export default function* rootSaga() {
  yield all([
    takeLatest(AuthTypes.LOGIN_REQUEST, login),
    takeLatest(CentroTypes.CENTROS_REQUEST, centrosRequest),
    takeLatest(REHYDRATE, rehydrate)
  ])
}
