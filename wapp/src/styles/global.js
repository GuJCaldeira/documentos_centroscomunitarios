import { injectGlobal } from 'styled-components'

// import 'bootstrap/dist/css/bootstrap.min.css'
// import 'animate.css/animate.min.css'
import 'flatpickr/dist/flatpickr.min.css'
import 'react-datepicker/dist/react-datepicker.css'
import 'react-datepicker/dist/react-datepicker-cssmodules.css'
import 'react-table/react-table.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'react-toastify/dist/ReactToastify.css'
import 'react-circular-progressbar/dist/styles.css'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import 'react-image-gallery/styles/css/image-gallery.css'
import 'react-image-gallery/styles/css/image-gallery-no-icon.css'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

injectGlobal`
  *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }

  html, body, #root {
    height: 100%;
  }

  body{
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing: antialiased !important;
  }

  .react-datepicker {
    display: flex !important;
  }
`
