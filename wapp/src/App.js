import React, { Fragment } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { ToastContainer } from 'react-toastify'
import 'moment/locale/pt-br'

import { Store, persistor } from 'store'

import Routes from './routes'

import './styles/global'

const App = () => (
  <Provider store={Store}>
    <PersistGate loading={null} persistor={persistor}>
      <Fragment>
        <Routes />
        <ToastContainer />
      </Fragment>
    </PersistGate>
  </Provider>
)

export default App
