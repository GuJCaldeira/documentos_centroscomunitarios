import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import HomePage from '../home'
import LocacaoPage from '../locacao'
import FaleconoscoPage from '../faleconosco'
import EventosPage from '../eventos'

class PortalRoutes extends Component {
  state = {}
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/locacao" component={LocacaoPage} />
          <Route exact path="/faleconosco" component={FaleconoscoPage} />
          <Route exact path="/eventos" component={EventosPage} />
        </Switch>
      </Router>
    )
  }
}

export default PortalRoutes
