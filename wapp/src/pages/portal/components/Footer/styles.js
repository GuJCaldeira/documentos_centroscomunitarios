import styled from 'styled-components'

export const Container = styled.div`
  .footerint {
    width: 100%;
    background-color: #e09816;
    color: #fff;
    text-align: center;
    padding: 15px;
  }
  .imgredes {
    width: 32px;
    height: 32px;
    float: left;
    margin: 2px;
  }
  .colorfooter {
    background-color: #f4f3f4;
  }
  .row {
    display: flex !important;
    -ms-flex-wrap: wrap !important;
    flex-wrap: wrap !important;
    margin-left: -15px !important;
    margin-right: 0 !important;
  }
  .alink {
    color: #000000 !important;
    text-decoration: none;
    font-size: 15px;
  }
  a:hover {
    text-decoration: none !important;
  }
  .row {
    display: flex !important;
    -ms-flex-wrap: wrap !important;
    flex-wrap: wrap !important;
    margin-left: -15px !important;
    margin-right: 0 !important;
  }
  .hcol {
    color: #1b387e;
  }
`

export const WelcomeModal = styled.div`
  height: 100%;
  width: 100%;
  position: fixed; /* Stay in place */
  z-index: 99999999; /* Sit on top */
  left: 0;
  top: 0;
  background-color: #fff;
  display: ${props => (props.open ? 'flex' : 'none')};
  justify-content: center;
  align-items: center;
`
