import React, { Component } from 'react'
import imgbrasao from 'images/footer/icon_brasao.png'
import imgface from 'images/footer/icon_facebook.png'
import imgtwit from 'images/footer/icon_twitter.png'
import imgyout from 'images/footer/icon_youtube.png'
import imginsta from 'images/footer/icon_instagram.png'
import imglogotiporodape from 'images/footer/logotiporodape.png'

import Overlay from './Overlay'
import { Container } from './styles'

class Portal extends Component {
  render() {
    return (
      <Container>
        <div className="container-fluid p-0">
          <footer className="colorfooter">
            <div className="row mt-4">
              <div className="col-2 bg-primary" style={{ height: '5px' }} />
              <div className="col-2 bg-success" style={{ height: '5px' }} />
              <div className="col-2 bg-danger" style={{ height: '5px' }} />
              <div className="col-2 bg-warning" style={{ height: '5px' }} />
              <div className="col-2 bg-dark" style={{ height: '5px' }} />
              <div className="col-2 bg-info" style={{ height: '5px' }} />
            </div>
            <div className="row p-3">
              <div className="col-md-4 d-flex justify-content-center align-items-center flex-column">
                <img className="p-2" src={imglogotiporodape} alt="Logotipo" />
                <img className="p-1" src={imgbrasao} alt="Brasão" />

                {/* <img className="p-3" src="<?php echo base_url('assets/img/logo.png'); ?>" alt="Logo">
										<img className="img-fluid p-3" src="<?php echo base_url('assets/img/brasao_piracicaba.png'); ?>" alt="Logo"> */}
              </div>
              <div className="col-md-2 col-4">
                <ul className="list-unstyled">
                  <h5 className="hcol">Principal</h5>
                  <li className="mb-1">
                    <a
                      className="alink"
                      href="http://www.piracicaba.sp.gov.br/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Prefeitura de Piracicaba
                    </a>
                  </li>
                  <li className="mb-1">
                    <a
                      className="alink"
                      href="http://www.piracicaba.sp.gov.br/contato"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Fale com a Prefeitura
                    </a>
                  </li>
                  <li className="mb-1">
                    <a
                      className="alink"
                      href="http://siave.camarapiracicaba.sp.gov.br/index/79/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Leis e Decretos
                    </a>
                  </li>
                  <li className="mb-1">
                    <a
                      className="alink"
                      href="http://www.piracicaba.sp.gov.br/telefones+uteis.aspx"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Telefones Úteis
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-md-2 col-4">
                <ul className="list-unstyled">
                  <h5 className="hcol">Serviços</h5>
                  <li className="mb-1">
                    <a
                      href="https://sistemas.pmp.sp.gov.br/SEMAD/SSERVIDOR/frm_Login/"
                      target="_top"
                      rel="noopener noreferrer"
                      className="alink"
                    >
                      Portal do Servidor
                    </a>
                  </li>
                  <li className="mb-1">
                    <a
                      href="http://conselhos.piracicaba.sp.gov.br/"
                      target="_blank"
                      rel="noopener noreferrer"
                      className="alink"
                    >
                      Portal do Conselho
                    </a>
                  </li>
                  <li className="mb-1">
                    <a
                      href="http://www.piracicaba.sp.gov.br/prefeito+e+secretarios+gestao+2017+2020.aspx"
                      target="_blank"
                      rel="noopener noreferrer"
                      className="alink"
                    >
                      Prefeitos e Secretários
                    </a>
                  </li>
                  <li className="mb-1">
                    <a
                      href="http://www.piracicaba.sp.gov.br/categoria/procon.aspx"
                      target="_blank"
                      rel="noopener noreferrer"
                      className="alink"
                    >
                      PROCON Piracicaba
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-md-2 col-4">
                <ul className="list-unstyled">
                  <h5 className="hcol">Utilidades</h5>
                  <li className="mb-1">
                    <a
                      href="http://www.piracicaba.sp.gov.br/editais/1"
                      target="_blank"
                      rel="noopener noreferrer"
                      className="alink"
                    >
                      Concurso Público
                    </a>
                  </li>
                  <li className="mb-1">
                    <a
                      href="http://consultaspublicas.piracicaba.sp.gov.br/"
                      target="_blank"
                      rel="noopener noreferrer"
                      className="alink"
                    >
                      Consultas Públicas
                    </a>
                  </li>
                  <li className="mb-1">
                    <a
                      href="http://www.financas.piracicaba.sp.gov.br/requerimento"
                      target="_blank"
                      rel="noopener noreferrer"
                      className="alink"
                    >
                      Requerimentos Diversos
                    </a>
                  </li>
                  <li className="mb-1">
                    <a
                      href="http://viaagil.com.br/linhas-e-horarios/"
                      target="_blank"
                      rel="noopener noreferrer"
                      className="alink"
                    >
                      Ônibus Urbano
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-md-2 d-flex justify-content-center">
                <ul className="list-unstyled">
                  <h5 className="hcol">Siga-nos</h5>
                  <li>
                    <a href="https://www.facebook.com/prefeituradepiracicaba/">
                      <img
                        className="imgredes"
                        src={imgface}
                        alt="Facebook"
                        title="Facebook"
                      />
                    </a>
                    <a href="https://twitter.com/search?q=Prefeitura%20de%20piracicaba&src=typd">
                      <img
                        className="imgredes"
                        src={imgtwit}
                        alt="Twitter"
                        title="Twitter"
                      />
                    </a>
                    <a href="https://www.youtube.com/user/Prefeiturapiracicaba">
                      <img
                        className="imgredes"
                        src={imgyout}
                        alt="Youtube"
                        title="Youtube"
                      />
                    </a>
                    <a href="https://www.instagram.com/prefeituradepiracicaba">
                      <img
                        className="imgredes"
                        src={imginsta}
                        alt="Instagram"
                        title="Instagram"
                      />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </footer>
          <div className="footerint">
            <div className="container">
              2018 © Alunos da Unimep - Todos os direitos reservados
            </div>
          </div>
        </div>
        <Overlay />
      </Container>
    )
  }
}

export default Portal
