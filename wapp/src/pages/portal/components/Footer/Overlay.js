import React, { Component } from 'react'
import imglogotipo from 'images/logotipo.png'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Creators as CentroActions } from 'store/ducks/centro'
import { WelcomeModal } from './styles'

class Overlay extends Component {
  state = {
    openModal: true,
    centros: []
  }

  async componentDidMount() {
    const { centrosRequest } = this.props
    centrosRequest()
  }

  toggle = () => {
    this.setState({
      openModal: !this.state.openModal
    })
  }

  closeOverlay = () => {
    this.setState({
      openModal: false
    })
  }

  formSubmit = e => {
    e.preventDefault()
    this.closeOverlay()
  }

  changeSelect = e => {
    const { changeCentro } = this.props
    changeCentro(e.target.value)
  }

  render() {
    const { centroState } = this.props

    return (
      <WelcomeModal open={this.state.openModal}>
        <form onSubmit={this.formSubmit}>
          <div className="form-row d-flex justify-content-center align-items-center">
            <div className="mb-3">
              <img className="p-3" src={imglogotipo} alt="Logo" />
            </div>
            <div className="form-group col-md-12">
              <select
                className="form-control"
                onChange={this.changeSelect}
                disabled={centroState.loading}
                value={centroState.centroSelecionado || ''}
              >
                <option value={null}>Buscar por centro comunitário</option>
                {centroState.centros.map(centro => (
                  <option
                    key={centro.id_centro_comunitario}
                    value={centro.id_centro_comunitario}
                  >
                    {centro.nome_centro}
                  </option>
                ))}
              </select>
            </div>
          </div>
          <button
            type="submit"
            className="btn btn-success btn-block"
            disabled={!centroState.centroSelecionado}
          >
            Entrar
          </button>
        </form>
      </WelcomeModal>
    )
  }
}

const mapStateToProps = state => ({
  centroState: state.centro
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(CentroActions, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Overlay)
