import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import imglogotipo from 'images/logotipo.png'
import { UncontrolledTooltip } from 'reactstrap'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Creators as CentroActions } from 'store/ducks/centro'
import { Container } from './styles'

class Header extends Component {
  state = {
    bodyFontSize: 1.0
  }

  mudarFonteSize = size => {
    const { bodyFontSize } = this.state
    const limiteMaximo = 1.6
    const limiteMinimo = 0.8
    const newFontSize = bodyFontSize + size
    if (newFontSize <= limiteMaximo && newFontSize >= limiteMinimo) {
      this.setState({
        bodyFontSize: newFontSize
      })
      document.body.style.fontSize = `${newFontSize}em`
    }
  }

  changeSelect = e => {
    const { changeCentro } = this.props
    changeCentro(e.target.value)
  }

  render() {
    const { centroState } = this.props

    return (
      <Container className="container-fluid p-0">
        <header>
          <div className="pl-2 pr-2 corbarra text-white justify-content-end align-items-center d-flex">
            Recursos de Acessibilidade: &nbsp;{' '}
            <button
              onClick={() => this.mudarFonteSize(-0.2)}
              className="btn btn-link text-white p-0"
              data-toggle="tooltip"
              data-placement="bottom"
              title="Texto Menor"
              id="menosZoom"
            >
              A-
            </button>{' '}
            &nbsp; | &nbsp;{' '}
            <UncontrolledTooltip placement="bottom" target="menosZoom">
              Diminuir tamanho
            </UncontrolledTooltip>
            <button
              onClick={() => this.mudarFonteSize(0.2)}
              className="btn btn-link text-white p-0"
              data-toggle="tooltip"
              data-placement="bottom"
              title="Texto Maior"
              id="maisZoom"
            >
              A+
            </button>{' '}
            &nbsp; | &nbsp;{' '}
            <UncontrolledTooltip placement="bottom" target="maisZoom">
              Aumentar tamanho
            </UncontrolledTooltip>
            <a className="text-white" href="/login">
              Área do Admin
            </a>
          </div>
          <div className="bg-light text-white border-bottom">
            <div className="row justify-content-between pl-2 pr-2 pb-4">
              <div className="col-sm-5">
                <Link to="/login">
                  <img className="p-3" src={imglogotipo} alt="Logo" />
                </Link>
              </div>
              <div className="col-sm-7 align-self-center px-4">
                <div className="form-inline">
                  <select
                    className="form-control"
                    onChange={this.changeSelect}
                    disabled={centroState.loading}
                    value={centroState.centroSelecionado || ''}
                  >
                    <option value={''}>Buscar por centro comunitário</option>
                    {centroState.centros.map(centro => (
                      <option
                        key={centro.id_centro_comunitario}
                        value={centro.id_centro_comunitario}
                      >
                        {centro.nome_centro}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>

            <nav className="navhome">
              <ul className="nav justify-content-end">
                <li className="nav-item border-left border-primary">
                  <Link className="nav-link menuhome" to="/">
                    Página Inicial
                  </Link>
                </li>
                <li className="nav-item border-left border-danger">
                  <Link className="nav-link" to="/locacao">
                    Locação
                  </Link>
                </li>

                <li className="nav-item border-left border-success">
                  <Link className="nav-link" to="/eventos">
                    Eventos
                  </Link>
                </li>
                <li className="nav-item border-left border-primary">
                  <Link className="nav-link" to="/faleconosco">
                    Fale Conosco
                  </Link>
                </li>
              </ul>
            </nav>
          </div>
        </header>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  centroState: state.centro
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(CentroActions, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header)
