import React, { Component } from 'react'
import Styled from 'styled-components'
import moment from 'moment'
import Flatpickr from 'react-flatpickr'
import { Portuguese } from 'flatpickr/dist/l10n/pt'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

const Container = Styled.div`
  .flatpickr-input{
    display:none;
    }
    .event {
    position: absolute;
    width: 3px;
    height: 3px;
    border-radius: 150px;
    bottom: 3px;
    left: calc(50% - 1.5px);
    content: " ";
    display: block;
    background: #3d8eb9;
    }
    .event.busy {
    background: #f64747;
}
`

class Calendar extends Component {
  constructor() {
    super()

    this.state = {
      date: new Date(),
      openModal: false,
      events: [
        {
          nome: 'Evento Teste',
          descricao: 'Nossa Locação',
          data_inicio: moment(
            '2018-09-19 08:00:00',
            'YYYY-MM-DD HH:mm:ss'
          ).toDate(),
          data_fim: moment(
            '2018-09-20 12:05:00',
            'YYYY-MM-DD HH:mm:ss'
          ).toDate(),
          tipo_evento: 'Festa',
          recorrencia: 'Uma vez por semana',
          status: 'a',
          detalhes: 'Sem Detalhes.'
        },
        {
          nome: 'Evento Teste 2',
          descricao: 'Nossa Locação 2',
          data_inicio: moment(
            '2018-09-18 08:00:00',
            'YYYY-MM-DD HH:mm:ss'
          ).toDate(),
          data_fim: moment(
            '2018-09-21 12:05:00',
            'YYYY-MM-DD HH:mm:ss'
          ).toDate(),
          tipo_evento: 'Festa 2',
          recorrencia: 'Uma vez por semana 2',
          status: 'a',
          detalhes: 'Sem Detalhes 2.'
        }
      ]
    }
  }

  toggleModal = () =>
    this.setState({
      openModal: !this.state.openModal
    })

  render() {
    const { date } = this.state
    return (
      <Container>
        <Flatpickr
          onDayCreate={(dObj, dStr, fp, dayElem) => {
            this.state.events.map(event => {
              if (
                moment(dayElem.dateObj).isSame(moment(event.data_inicio), 'day')
              )
                dayElem.innerHTML += "<span class='event busy'></span>"
              // eslint-disable-next-line
              return
            })
          }}
          options={{
            inline: true,
            locale: Portuguese
          }}
          value={date}
          onChange={date => {
            this.setState({ date })
            this.toggleModal()
          }}
        />
        <Modal isOpen={this.state.openModal} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
          <ModalBody>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleModal}>
              Do Something
            </Button>{' '}
            <Button color="secondary" onClick={this.toggleModal}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </Container>
    )
  }
}

export default Calendar
