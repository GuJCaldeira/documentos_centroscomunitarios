import styled from 'styled-components'

export const Container = styled.div`
  .colorlocacao {
    background-color: #469b1d;
    font-size: 15px;
    padding-left: 14px;
    font-weight: 600;
    margin-bottom: 10px;
    padding: 10px;
  }
  .colordenuncia {
    background-color: #a72020;
    font-size: 15px;
    padding-left: 14px;
    font-weight: 500;
    margin-bottom: 10px;
    padding: 10px;
  }
  .colordoacao {
    background-color: #57978d;
    font-size: 15px;
    padding-left: 14px;
    font-weight: 500;
    margin-bottom: 10px;
    padding: 10px;
  }
  .colorouvidoria {
    background-color: #638dd2;
    font-size: 15px;
    padding-left: 14px;
    font-weight: 500;
    margin-bottom: 10px;
    padding: 10px;
  }
  .colorprefeitura {
    background-color: #c77a12;
    font-size: 15px;
    padding-left: 14px;
    font-weight: 500;
    margin-bottom: 10px;
    padding: 10px;
  }
  .colorgovfed {
    background-color: #ff6600;
    font-size: 15px;
    padding-left: 14px;
    font-weight: 500;
    margin-bottom: 10px;
    padding: 10px;
  }
  .colorcaixaeco {
    background-color: #3c8518;
    font-size: 15px;
    padding-left: 14px;
    font-weight: 500;
    margin-bottom: 10px;
    padding: 10px;
  }
  .colorreceita {
    background-color: #6b1414;
    font-size: 15px;
    padding-left: 14px;
    font-weight: 500;
    margin-bottom: 10px;
    padding: 10px;
  }
  .colorfooter {
    background-color: #f4f3f4;
  }

  li {
    list-style: none;
  }
  .tdfor {
    padding: 0.25rem !important;
  }
  .tdforativo {
    padding: 0.25rem !important;
    background: #c77a12;
    text-align: center;
    color: #fff;
  }
  .image-gallery-content {
    width: 100%;
    margin-left: 15px;
  }
  .card-text {
    font-size: 14px;
    color: #777777;
    font-family: Arial, Helvetica, sans-serif;
  }
  .badge-secondary {
    color: #fff;
    background-color: #507494;
    padding: 8px;
  }
  .text-muted {
    font-weight: 600;
    font-size: 13px;
  }
  .noticias {
    padding-left: 15px;
    padding: 0.25rem !important;
  }
  .bg-primary {
    background-color: #05468b !important;
    font-weight: 500;
  }
  .imgpublicidade {
    width: 100%;
    padding: 25px;
  }
  .card-deck .card {
    display: flex;
    flex-direction: column;
    margin-right: 12px;
    margin-bottom: 0;
    margin-left: 31px;
    flex: 1 0;
  }
  .btn-outline-primary {
    color: #ffffff;
    border-color: #469b1d;
    background-color: #469b1d;
    background-image: none;
    margin-left: 4px;
    height: 37px;
    width: 36px;
  }
  .nav-link {
    display: block;
    padding: 0.5rem 3rem;
    text-transform: uppercase;
    color: #095279;
    font-weight: 400;
  }
  .nav {
    margin-top: -15px;
  }
`
