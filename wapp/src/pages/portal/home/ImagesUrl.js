const ImagesUrl = [
  {
    original: '/assets/img/noticias/img_not03.jpg',
    thumbnail: '/assets/img/noticias/img_not03.jpg',
    originalTitle:
      'AgTech Valley Summit está com inscrições abertas e contará com principais lideranças do agronegócio',
    description:
      'AgTech Valley Summit está com inscrições abertas e contará com principais lideranças do agronegócio'
  },
  {
    original: '/assets/img/noticias/img_not02.jpg',
    thumbnail: '/assets/img/noticias/img_not02.jpg',
    originalTitle:
      ' O Engenho Central é um dos mais belos cenários arquitetônicos do Estado de São Paulo',
    description:
      ' O Engenho Central é um dos mais belos cenários arquitetônicos do Estado de São Paulo'
  },
  {
    original: '/assets/img/noticias/img_not01.jpg',
    thumbnail: '/assets/img/noticias/img_not01.jpg',
    originalTitle:
      'AgTech Valley Summit está com inscrições abertas e contará com principais lideranças do agronegócio',
    description:
      'AgTech Valley Summit está com inscrições abertas e contará com principais lideranças do agronegócio'
  },
  {
    original: '/assets/img/noticias/img_not04.jpg',
    thumbnail: '/assets/img/noticias/img_not04.jpg',
    originalTitle: 'O amarelo se destaca na paisagem de Piracicaba',
    description: 'O amarelo se destaca na paisagem de Piracicaba'
  },
  {
    original: '/assets/img/noticias/img_not05.jpg',
    thumbnail: '/assets/img/noticias/img_not05.jpg',
    originalTitle: 'Piracicaba o 29º Campeonato de Balonismo',
    description: 'Piracicaba o 29º Campeonato de Balonismo'
  },
  {
    original: '/assets/img/noticias/img_not06.jpg',
    thumbnail: '/assets/img/noticias/img_not06.jpg',
    originalTitle:
      'Dia do Rio Piracicaba é celebrado com Arrastão Ecológico e Campanha Rio Vivo',
    description:
      'Dia do Rio Piracicaba é celebrado com Arrastão Ecológico e Campanha Rio Vivo'
  },
  {
    original: '/assets/img/noticias/img_not07.jpg',
    thumbnail: '/assets/img/noticias/img_not07.jpg',
    originalTitle:
      'Piracicaba Trabalha Assim leva Associação dos Aposentados Eclética ao Museu da Água e Parque Automotivo',
    description:
      'Piracicaba Trabalha Assim leva Associação dos Aposentados Eclética ao Museu da Água e Parque Automotivo'
  },
  {
    original: '/assets/img/noticias/img_not08.jpg',
    thumbnail: '/assets/img/noticias/img_not08.jpg',
    originalTitle:
      'O Zoológico Municipal de Piracicaba abriga aproximadamente 422 animais',
    description:
      'O Zoológico Municipal de Piracicaba abriga aproximadamente 422 animais'
  }
]

export default ImagesUrl
