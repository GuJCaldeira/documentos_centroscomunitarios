import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ImageGallery from 'react-image-gallery'

import Header from '../components/Header'
import Footer from '../components/Footer'
import ImagesUrl from './ImagesUrl'
import Calendar from './components/Calendar'

import imgpublicidade from 'images/banner_todosnot.jpg'

import { Container } from './styles'

class Home extends Component {
  render() {
    return (
      <Container>
        <Header />
        <div className="container-fluid p-0">
          <div className="container-fluid">
            <div className="row mt-3">
              <div className="col-md-9 row">
                <div className="col-12 ">
                  <div className="col-3">
                    <div className="bg-primary p-1 text-white">EVENTOS</div>
                  </div>
                </div>
                <div className="col-md-12">
                  <ImageGallery
                    thumbnailPosition="right"
                    showFullscreenButton={false}
                    slideDuration={300}
                    showPlayButton={false}
                    showBullets={true}
                    autoPlay={true}
                    items={ImagesUrl}
                  />
                </div>

                <div className="col-12">
                  <div className="row">
                    <div className="col-12 mt-2 mb-2">
                      <div className="col-3">
                        <div className="bg-primary p-1 text-white">
                          NOTÍCIAS
                        </div>
                      </div>
                    </div>

                    <div className="card-deck m-0">
                      <div className="card border-0">
                        {/* <img className='card-img-top' src='http://via.placeholder.com/126x126' alt='Card image cap'> */}
                        <div className="card-body">
                          <span className="badge badge-secondary">
                            Fundo Social
                          </span>
                          <br />
                          <time className="text-muted">12/06/2018</time>
                          <p className="card-text">
                            Some quick example text to build on the card title
                            and make up the bulk of the card's content.
                          </p>
                        </div>
                      </div>

                      <div className="card border-0">
                        {/* <img className='card-img-top' src='http://via.placeholder.com/126x126' alt='Card image cap'> */}
                        <div className="card-body">
                          <span className="badge badge-secondary">Saúde</span>
                          <br />
                          <time className="text-muted">12/06/2018</time>
                          <p className="card-text">
                            Some quick example text to build on the card title
                            and make up the bulk of the card's content.
                          </p>
                        </div>
                      </div>

                      <div className="card border-0">
                        {/* <img className='card-img-top' src='http://via.placeholder.com/126x126' alt='Card image cap'> */}
                        <div className="card-body">
                          <span className="badge badge-secondary">
                            Ação Social
                          </span>
                          <br />
                          <time className="text-muted">12/06/2018</time>
                          <p className="card-text">
                            Some quick example text to build on the card title
                            and make up the bulk of the card's content.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-12">
                      <img
                        className="imgpublicidade"
                        src={imgpublicidade}
                        alt="Publicidade"
                      />
                    </div>

                    <div className="card-deck m-0">
                      <div className="card border-0">
                        {/* <img className='card-img-top' src='http://via.placeholder.com/126x126' alt='Card image cap'> */}
                        <div className="card-body">
                          <span className="badge badge-secondary">
                            Fundo Social
                          </span>
                          <br />
                          <time className="text-muted">12/06/2018</time>
                          <p className="card-text">
                            Some quick example text to build on the card title
                            and make up the bulk of the card's content.
                          </p>
                        </div>
                      </div>

                      <div className="card border-0">
                        {/* <img className='card-img-top' src='http://via.placeholder.com/126x126' alt='Card image cap'> */}
                        <div className="card-body">
                          <span className="badge badge-secondary">Saúde</span>
                          <br />
                          <time className="text-muted">12/06/2018</time>
                          <p className="card-text">
                            Some quick example text to build on the card title
                            and make up the bulk of the card's content.
                          </p>
                        </div>
                      </div>

                      <div className="card border-0">
                        {/* <img className='card-img-top' src='http://via.placeholder.com/126x126' alt='Card image cap'> */}
                        <div className="card-body">
                          <span className="badge badge-secondary">
                            Ação Social
                          </span>
                          <br />
                          <time className="text-muted">12/06/2018</time>
                          <p className="card-text">
                            Some quick example text to build on the card title
                            and make up the bulk of the card's content.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-12">
                  {/* <img className='img-fluid mx-auto d-block' src='<?php echo base_url('assets/img/examples/banner.png'); ?>' alt=''> */}
                </div>
              </div>

              <div className="col-md-3">
                <div className="col-12 p-0 mt-2 mb-2">
                  <div className="bg-primary p-1 text-white">
                    <center>AGENDA DE EVENTOS</center>
                  </div>
                </div>
                <div className="col-12 p-0">
                  <div className="table-responsive-sm">
                    <Calendar />
                  </div>
                </div>

                <br />
                <div className="col-12 p-0">
                  <Link className="text-white" to="/locacao">
                    <li className="colorlocacao text-white">LOCAÇÃO</li>
                  </Link>
                  <a
                    href="http://sistemas3.piracicaba.sp.gov.br/semad/Protocolo/ctr_MenuProtocolo156/ctr_MenuProtocolo156.php"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="alink"
                  >
                    <li className="colordenuncia text-white">DENÚNCIAS</li>
                  </a>

                  <a
                    href="http://www.piracicaba.sp.gov.br/"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="alink"
                  >
                    <li className="colorprefeitura text-white">
                      PREFEITURA DE PIRACICABA
                    </li>
                  </a>
                  <a
                    href="http://www.brasil.gov.br/"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="alink"
                  >
                    <li className="colorgovfed text-white">GOVERNO FEDERAL</li>
                  </a>
                  <a
                    href="http://www.caixa.gov.br/Paginas/home-caixa.aspx"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="alink"
                  >
                    <li className="colorcaixaeco text-white">
                      CAIXA ECONÔMICA
                    </li>
                  </a>
                  <a
                    href="http://idg.receita.fazenda.gov.br/"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="alink"
                  >
                    <li className="colorreceita text-white">RECEITA FEDERAL</li>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <Footer />
        </div>
      </Container>
    )
  }
}

export default Home
