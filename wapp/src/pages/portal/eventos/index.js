import React, { Component } from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'

import BigCalendar from 'react-big-calendar'
import moment from 'moment'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import { Container, CalendarContainer } from './styles'
import axiosClient from 'helpers/axiosClient'
import { ListGroup, ListGroupItem } from 'reactstrap'

const localizer = BigCalendar.momentLocalizer(moment)

class Eventos extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false,
      eventos: [],
      eventoView: {}
    }
  }

  componentDidMount() {
    this.setState({ loading: true })
    axiosClient
      .get('site/evento/listar')
      .then(response => {
        if (
          response.data.status &&
          response.data.obj &&
          Array.isArray(response.data.obj.evento)
        ) {
          this.setState({
            eventos: response.data.obj.evento.map(evento => {
              return {
                titulo: evento.titulo,
                data_inicio: moment(
                  evento.data_inicio,
                  'YYYY-MM-DD HH:mm:ss'
                ).toDate(),
                data_final: moment(
                  evento.data_final,
                  'YYYY-MM-DD HH:mm:ss'
                ).toDate(),
                id_centro_comunitario: evento.id_centro_comunitario,
                descricao: evento.descricao,
                tipo_evento: evento.tipo_evento,
                nome_centro: evento.nome_centro
              }
            })
          })
        } else {
          console.error(response.data.erro)
        }
        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)

        this.setState({ loading: false })
      })
  }

  componentDidUpdate(_prevProps, prevState) {
    if (
      JSON.stringify(prevState.eventoView) !==
        JSON.stringify(this.state.eventoView) &&
      this.state.eventoView.codigoResponsavel
    ) {
      axiosClient
        .get(
          '/painel/responsavel/listar/' +
            this.state.eventoView.codigoResponsavel
        )
        .then(response => {
          if (response.data.status) {
            this.setState({
              eventoView: {
                ...this.state.eventoView,
                responsavel_nome: response.data.obj.responsavel[0].nome
              }
            })
          }
        })
    }
  }

  toggle = evento => {
    this.setState(prevState => ({
      modal: !prevState.modal,
      eventoView: evento
    }))
  }

  render() {
    const { eventoView } = this.state
    const EventosCalendario = this.state.eventos.map(evento => {
      return {
        id: evento.id_evento,
        title: evento.titulo,
        start: evento.data_inicio,
        end: evento.data_final,
        descricao: evento.descricao,
        codigoCentroComunitario: evento.id_centro_comunitario,
        tipo_evento: evento.tipo_evento,
        nome_centro: evento.nome_centro
      }
    })
    return (
      <div>
        <Header />
        <Container>
          <div className="card">
            <div className="card-body">
              <div className="row">
                <div className="col col-lg-6">
                  <div
                    style={{
                      height: '500px',
                      width: '600px'
                    }}
                  >
                    <h1 className="card-title text-center">
                      Calendário de eventos
                    </h1>
                    <CalendarContainer>
                      <BigCalendar
                        messages={{
                          next: 'Próximo',
                          previous: 'Anterior',
                          today: 'Hoje',
                          month: 'Mês',
                          week: 'Semana',
                          day: 'Dia',
                          Agenda: 'Agenda',
                          showMore: total => `+ ${total} Ver todos`
                        }}
                        popup
                        localizer={localizer}
                        events={EventosCalendario.filter(
                          evento => evento.tipo_evento === 'E'
                        )}
                        startAccessor="start"
                        endAccessor="end"
                        style={{
                          height: '100%'
                        }}
                        onSelectEvent={evento => {
                          console.log(evento)
                          this.toggle(evento)
                        }}
                      />
                    </CalendarContainer>
                  </div>
                </div>
                <div className="col col-lg-6">
                  <h1 className="card-title text-center">Próximos Eventos</h1>
                  <ListGroup>
                    {EventosCalendario.filter(
                      evento =>
                        moment(evento.start).isSameOrAfter(moment()) &&
                        evento.tipo_evento === 'E'
                    )
                      .sort(function(a, b) {
                        return moment(a.start).isBefore(moment(b.start))
                          ? -1
                          : moment(a.start).isAfter(moment(b.start))
                            ? 1
                            : 0
                      })
                      .slice(0, 5)
                      .map((evento, index) => (
                        <ListGroupItem key={index}>
                          <h3>{evento.title}</h3>
                          <p>{evento.descricao}</p>
                          <div className="row">
                            <div className="col col-lg-6">
                              Data de início:{' '}
                              {moment(evento.start).format('DD/MM/YYYY HH:mm')}
                            </div>
                            <div className="col col-lg-6">
                              Data de término:{' '}
                              {moment(evento.end).format('DD/MM/YYYY HH:mm')}
                            </div>
                          </div>
                        </ListGroupItem>
                      ))}
                  </ListGroup>
                </div>
              </div>
            </div>
          </div>
        </Container>
        <Footer />
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
          size="lg"
        >
          <ModalHeader>{`Editar evento`}</ModalHeader>

          <ModalBody>
            <dl className="row">
              <dt className="col-sm-3">Titulo</dt>
              <dd className="col-sm-9">{eventoView.title}</dd>

              <dt className="col-sm-3">Descrição do evento</dt>
              <dd className="col-sm-9">{eventoView.descricao}</dd>

              <dt className="col-sm-3">Data de início</dt>
              <dd className="col-sm-9">
                <p>
                  {moment(eventoView.start).format('DD/MM/YYYY ')} ás{' '}
                  {moment(eventoView.start).format('HH:mm')}
                </p>
              </dd>

              <dt className="col-sm-3">Data de término</dt>
              <dd className="col-sm-9">
                <p>
                  {moment(eventoView.start).format('DD/MM/YYYY ')} ás{' '}
                  {moment(eventoView.end).format('HH:mm')}
                </p>
              </dd>
            </dl>
          </ModalBody>
          <ModalFooter>
            <Button
              type="button"
              color="secondary"
              onClick={() => this.toggle({})}
            >
              Voltar
            </Button>{' '}
          </ModalFooter>
        </Modal>
      </div>
    )
  }
}

export default Eventos
