import styled from 'styled-components'

export const Container = styled.div``

export const CalendarContainer = styled.div`
  height: 450px;
  .rbc-toolbar {
    span {
      display: none;
    }
    span {
      &:first-child {
        display: block;
        button:first-child {
          display: none;
        }
      }
      &.rbc-toolbar-label {
        display: block;
      }
    }
  }
  .rbc-toolbar-label {
    text-transform: uppercase !important;
    font-weight: 700;
  }
  @media (max-width: 1040px) {
    .rbc-toolbar {
      flex-direction: column;
      display: none !important;
    }
  }
`
