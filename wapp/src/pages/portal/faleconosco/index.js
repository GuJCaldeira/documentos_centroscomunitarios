import React, { Component } from 'react'
import MaskedInput from 'react-maskedinput'
import classNames from 'classnames'
import { connect } from 'react-redux'

import Header from '../components/Header'
import Footer from '../components/Footer'
import imglogotipo from 'images/logotipo.png'

import { axiosSite } from 'helpers/axiosClient'
import FormValidator from 'helpers/FormValidator'

class FaleConosco extends Component {
  constructor(props) {
    super(props)

    this.validator = new FormValidator([
      {
        field: 'nome',
        method: 'isEmpty',
        validWhen: false,
        message: 'Por favor, digite seu nome'
      },
      {
        field: 'email',
        method: 'isEmpty',
        validWhen: false,
        message: 'Por favor, digite seu Email'
      },
      {
        field: 'email',
        method: 'isEmail',
        validWhen: true,
        message: 'Por favor, digite um Email válido'
      },
      {
        field: 'telefone',
        method: 'isEmpty',
        validWhen: false,
        message: 'Por favor, digite seu telefone'
      },
      {
        field: 'mensagem',
        method: 'isEmpty',
        validWhen: false,
        message: 'Por favor, digite uma descrição da sua solicitação'
      }
    ])

    this.state = {
      nome: '',
      email: '',
      telefone: '',
      mensagem: '',

      validation: this.validator.valid(),
      submitted: false
    }
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSubmit = e => {
    e.preventDefault()
    const { centroState } = this.props
    const { nome, email, telefone, mensagem } = this.state

    const validation = this.validator.validate(this.state)
    this.setState({ validation, submitted: true })

    if (validation.isValid) {
      axiosSite
        .post('faleconosco', {
          nome,
          email,
          telefone,
          mensagem,
          id_centro_comunitario: centroState.centroSelecionado
            ? centroState.centroSelecionado.value
            : ''
        })
        .then(resp => console.log(resp))
    }
  }
  render() {
    const { nome, email, telefone, mensagem } = this.state
    const validation = this.state.submitted
      ? this.validator.validate(this.state)
      : this.state.validation
    const inputClassNames = isInvalid => {
      return classNames('form-control', { 'is-invalid': isInvalid })
    }
    return (
      <>
        <Header />
        <div className="container">
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-md-5 mt-4 row justify-content-center align-items-center">
                <img className="img-responsive" src={imglogotipo} alt="logo" />
              </div>

              <div className="col-md-6 offset-1 mt-4">
                <div className="form-row">
                  <div className="col-md-5 mt-2">
                    <h1> Fale Conosco</h1>
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-10">
                    <label htmlFor="nome">Nome Completo</label>
                    <input
                      type="text"
                      className={inputClassNames(validation.nome.isInvalid)}
                      onChange={this.handleChange}
                      value={nome}
                      id="nome"
                      name="nome"
                      placeholder="Insira seu nome completo"
                    />
                    <div className="invalid-feedback">
                      {validation.nome.message}
                    </div>
                  </div>
                </div>

                <div className="form-row">
                  <div className="col-md-5 mt-1">
                    <label htmlFor="email">E-mail</label>
                    <input
                      type="email"
                      className={inputClassNames(validation.email.isInvalid)}
                      onChange={this.handleChange}
                      value={email}
                      id="email"
                      name="email"
                      placeholder="Informe seu e-mail"
                    />
                    <div className="invalid-feedback">
                      {validation.email.message}
                    </div>
                  </div>

                  <div className="col-md-5 mt-1">
                    <label htmlFor="telefone">Telefone</label>
                    <MaskedInput
                      mask="(19) 1111-1111"
                      name="telefone"
                      className={inputClassNames(validation.telefone.isInvalid)}
                      onChange={this.handleChange}
                      value={telefone}
                      size="10"
                      placeholder="(00) 0000-0000"
                    />
                    <div className="invalid-feedback">
                      {validation.telefone.message}
                    </div>
                  </div>
                </div>

                <div className="form-row">
                  <div className="col-md-10 mt-1">
                    <label htmlFor="comment">Mensagem:</label>
                    <textarea
                      className={inputClassNames(validation.mensagem.isInvalid)}
                      onChange={this.handleChange}
                      rows="5"
                      value={mensagem}
                      id="mensagem"
                      name="mensagem"
                      placeholder="Escreva aqui sua mensagem..."
                    />
                    <div className="invalid-feedback">
                      {validation.mensagem.message}
                    </div>
                  </div>
                </div>

                <div className="form-row mt-2">
                  <div className="col-md-10">
                    <button type="submit" className="btn btn-outline-primary">
                      Enviar
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <Footer />
      </>
    )
  }
}

const mapStateToProps = state => ({
  centroState: state.centro
})

export default connect(mapStateToProps)(FaleConosco)
