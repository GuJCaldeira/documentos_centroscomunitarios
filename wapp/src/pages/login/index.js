import React from 'react'
import { Route } from 'react-router-dom'

import Signin from './components/Signin'
import RecoveryPassword from './components/RecoveryPassword'

const Login = () => (
  <div className="h-100 d-flex align-items-center bg-auth border-top border-top-2 border-primary">
    <div className="container">
      <Route exact path="/login" component={Signin} />
      <Route path="/login/recuperar-senha" component={RecoveryPassword} />
    </div>
  </div>
)

export default Login
