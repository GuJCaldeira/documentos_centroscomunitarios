import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import moment from 'moment'
import classnames from 'classnames'

import { Creators as AuthActions } from 'store/ducks/auth'
import Logo from 'images/logo.png'

class Signin extends Component {
  state = {
    cpf: '',
    senha: '',
    error: false,
    errorMessage: '',
    visiblePassword: false,
    redirect: false
  }

  componentDidUpdate(prevProps, prevState) {
    const { auth } = this.props
    if (
      auth.token &&
      prevProps.auth.token !== auth.token &&
      moment(auth.expire).isAfter(moment())
    ) {
      this.setState({ redirect: true })
    }

    if (auth.error !== prevProps.auth.error) {
      this.setState({
        errorMessage: <div dangerouslySetInnerHTML={{ __html: auth.error }} />
      })
    }
  }

  onSubmit = e => {
    e.preventDefault()

    const { cpf, senha } = this.state

    this.props.loginRequest({ cpf, senha })
  }

  handleChange = e => {
    let { name, value } = e.target
    this.setState({ [name]: value })
  }

  render() {
    const { visiblePassword, errorMessage, cpf, senha, redirect } = this.state
    const { auth } = this.props
    if (redirect) return <Redirect to="/admin" />
    return (
      <div className="row align-items-center">
        <div className="col-12 col-md-6 offset-xl-2 offset-md-1 order-md-2 mb-5 mb-md-0">
          {/* Image */}
          <div className="text-center">
            <img
              // src="assets/img/illustrations/happiness.svg"
              src={Logo}
              alt="Logo Centros Comunitários"
              className="img-fluid"
            />
          </div>
        </div>
        <div className="col-12 col-md-5 col-xl-4 order-md-1 my-5">
          {/* Heading */}
          <h1 className="display-4 text-center mb-3">Entrar</h1>
          {/* Subheading */}
          <p className="text-muted text-center mb-5">
            Acesso ao painel de administração.
          </p>
          {/* Form */}
          <form onSubmit={this.onSubmit}>
            {/* Email address */}
            <div className="form-group">
              {/* Label */}
              <label>CPF</label>
              {/* Input */}
              <input
                type="text"
                className="form-control"
                name="cpf"
                placeholder="000.000.000-00"
                required
                onChange={this.handleChange}
                value={cpf}
                tabIndex={1}
              />
            </div>
            {/* Password */}
            <div className="form-group">
              <div className="row">
                <div className="col">
                  {/* Label */}
                  <label>Senha</label>
                </div>
                <div className="col-auto">
                  {/* Help text */}
                  <Link
                    to="/login/recuperar-senha"
                    className="form-text small text-muted"
                    tabIndex={4}
                  >
                    Esqueceu a senha?
                  </Link>
                </div>
              </div>{' '}
              {/* / .row */}
              {/* Input group */}
              <div className="input-group input-group-merge">
                {/* Input */}
                <input
                  type={visiblePassword ? 'text' : 'password'}
                  className="form-control form-control-appended"
                  placeholder="Digite sua senha"
                  name="senha"
                  // required
                  onChange={this.handleChange}
                  value={senha}
                  tabIndex={2}
                />
                {/* Icon */}
                <div
                  className="input-group-append"
                  onClick={() =>
                    this.setState(prevState => ({
                      visiblePassword: !prevState.visiblePassword
                    }))
                  }
                >
                  <span className="input-group-text">
                    {visiblePassword ? (
                      <i className="far fa-eye" />
                    ) : (
                      <i className="far fa-eye-slash" />
                    )}
                  </span>
                </div>
              </div>
            </div>
            {/* Submit */}
            <button
              className={classnames('btn btn-lg btn-block btn-primary mb-3', {
                'is-loading': auth.loading
              })}
              tabIndex={3}
              disabled={auth.loading}
            >
              Entrar
            </button>
          </form>
          {errorMessage && (
            <div className="form-text text-danger"> {errorMessage} </div>
          )}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth
})

const mapDispatchToProps = dispatch => bindActionCreators(AuthActions, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Signin)
