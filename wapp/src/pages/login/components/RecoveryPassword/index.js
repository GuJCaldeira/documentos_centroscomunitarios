import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class RecoveryPassword extends Component {
  state = {}
  render() {
    return (
      <div className="row justify-content-center">
        <div className="col-12 col-md-5 col-xl-4 my-5">
          {/* Heading */}
          <h1 className="display-4 text-center mb-3">Recuperar senha</h1>
          {/* Subheading */}
          <p className="text-muted text-center mb-5">
            Digite seu e-mail para obter um link de redefinição de senha.
          </p>
          {/* Form */}
          <form>
            {/* Email address */}
            <div className="form-group">
              {/* Label */}
              <label>Email</label>
              {/* Input */}
              <input
                type="email"
                className="form-control"
                placeholder="nome@email.com"
              />
            </div>
            {/* Submit */}
            <button className="btn btn-lg btn-block btn-primary mb-3">
              Redefinir Senha
            </button>
            {/* Link */}
            <div className="text-center">
              <small className="text-muted text-center">
                Lembre-se da sua senha?{' '}
                <Link to="/login">Página de acesso</Link>.
              </small>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

export default RecoveryPassword
