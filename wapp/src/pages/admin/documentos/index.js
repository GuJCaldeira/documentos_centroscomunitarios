import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import ReactTable from 'react-table'
import { ButtonGroup } from 'reactstrap'
import moment from 'moment'
import axiosClient from 'helpers/axiosClient'

import filterCaseInsensitive from 'helpers/filterCaseInsensitive'

class Conta extends Component {
  constructor(props) {
    super(props)

    // definindo o estado inicial das variáveis que serão usadas no componente
    this.state = {
      conta: [],
      loading: false
    }

    this.excluirConta = this.excluirConta.bind(this)
  }

  // após a renderização do componente
  componentDidMount() {
    this.setState({ loading: true })
    axiosClient
      .get('painel/documentos/listar')
      .then(response => {
        if (
          response.data.status &&
          response.data.obj &&
          Array.isArray(response.data.obj.conta)
        ) {
          this.setState({
            conta: response.data.obj.conta.map(conta => {
              return {
                id_conta: conta.id_conta,
                nome_conta: conta.nome_conta,
                data_criacao: moment(
                  conta.data_criacao,
                  'YYYY-MM-DD HH:mm:ss'
                ).format('DD/MM/YYYY'),
                status: conta.status,
                id_associacao: conta.id_associacao
              }
            })
          })
        } else {
          console.error(response.data.erro)
        }

        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)

        this.setState({ loading: false })
      })
  }

  componentDidCatch(error, info) {
    console.log(error, info)

    this.setState({
      conta: []
    })
  }

  excluirConta(id) {
    // Confirmação de exclusão
    Swal({
      title: 'Você tem certeza?',
      text: 'Você não poderá reverter isso!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, quero excluir o grupo de conta!'
    }).then(result => {
      // Se "result.value" for igual a "true" excluí o usuário
      if (result.value) {
        axiosClient
          .post('/painel/documentos/deletar', { id_conta: id })
          .then(response => {
            // Excluindo um registro da lista
            if (response.data.status) {
              this.setState(prevState => ({
                conta: prevState.conta.filter(conta => id !== conta.id_conta)
              }))
              Swal('Excluído!', response.data.message, 'success')
            } else {
              Swal('Erro!', response.data.erro, 'error')
            }
          })
      }
    })
  }

  render() {
    // definindo colunas para a tabela
    const columns = [
      {
        Header: 'Código', // Cabeçalho da coluna
        accessor: 'id_conta', // String-based value accessors!
        filterable: false,
        width: 70
      },
      {
        Header: 'Titulo',
        accessor: 'nome_conta' // String-based value accessors!
      },
      {
        Header: 'Data de Inclusão',
        accessor: 'data_criacao' // String-based value accessors!
      },
      {
        Header: 'Download',
        filterable: false
        // accessor: 'data_criacao' // String-based value accessors!
      },
      {
        Header: 'Ações',
        Cell: props => (
          <div className="d-flex justify-content-center align-items-center">
            <ButtonGroup size="sm">
              <Link
                to={`/admin/documentos/alterar/${props.row.id_conta}`}
                className="btn btn-warning text-white"
              >
                <i className="fas fa-pencil-alt" />{' '}
                <span className="d-none d-sm-inline font-weight-bold">
                  Editar
                </span>
              </Link>
              <button
                onClick={() => this.excluirConta(props.row.id_conta)}
                className="btn btn-danger"
              >
                <i className="fas fa-times" />{' '}
                <span className="d-none d-sm-inline font-weight-bold">
                  Excluir
                </span>
              </button>
            </ButtonGroup>
          </div>
        ),
        filterable: false,
        width: 167
      }
    ]
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card ">
              <div className="card-header ">
                <div className="row align-items-center justify-content-between">
                  <div>
                    <h4 className="card-title">Lista de Documentos</h4>
                    <p className="card-category">Documentos do Sistema</p>
                  </div>
                  <Link
                    className="btn btn-success float-right"
                    to="/admin/documentos/cadastrar"
                  >
                    <i className="fas fa-plus-circle" />{' '}
                    <span className="d-none d-sm-inline">
                      Inclusão de Documentos
                    </span>
                  </Link>
                </div>
              </div>
              <div className="card-body">
                <ReactTable
                  columns={columns}
                  data={this.state.conta}
                  loading={this.state.loading}
                  defaultPageSize={10}
                  filterable
                  defaultFilterMethod={filterCaseInsensitive}
                  className="-striped -highlight"
                  previousText="Anterior"
                  nextText="Próximo"
                  loadingText="Carregando..."
                  noDataText="Nenhum documento foi encontrado"
                  pageText="Pág."
                  ofText="de"
                  rowsText="linhas"
                />
              </div>
              <div className="card-footer ">
                <div className="stats" />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Conta
