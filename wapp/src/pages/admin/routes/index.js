import React, { Component, Fragment } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Sidebar from 'components/Sidebar'
import MainContent from 'components/MainContent'

import HomePage from '../home'
import UsuarioPage from '../usuario'
import UsuarioFormPage from '../usuario/UsuarioForm'

import AssociacaoPage from '../associacao'
import AssociacaoFormPage from '../associacao/AssociacaoForm'

import ContaPage from '../conta'
import ContaFormPage from '../conta/ContaForm'

import CaixaPage from '../caixa'
import CaixaFormPage from '../caixa/CaixaForm'
import ReportFormPage from '../caixa/ReportForm'

import CentroComunitarioPage from '../centroComunitario'
import CentroComunitarioFormPage from '../centroComunitario/CentroComunitarioForm'

import ResponsaveisPage from '../responsaveis'
import ResponsaveisFormPage from '../responsaveis/ResponsavelForm'

import EventosPage from '../eventos'
import EventosFormPage from '../eventos/EventosForm'

import CargosPage from '../cargos'
import CargosFormPage from '../cargos/CargoForm'

import LocacaoPage from '../locacao'
import LocacaoFormPage from '../locacao/LocacaoForm'

import PostagemPage from '../postagem'
import PostagemFormPage from '../postagem/PostagemForm'
import PostagemArquivo from '../postagem/PostagemArquivo'

import DocumentosPage from '../documentos'
import DocumentosFormPage from '../documentos/DocumentosForm'

import GrupoPage from '../gruposDePermissoes'
import GrupoFormPage from '../gruposDePermissoes/GrupoForm'

class AdminRoutes extends Component {
  state = {}
  render() {
    return (
      <Router>
        <Fragment>
          <Sidebar />
          <MainContent>
            <Switch>
              {/* Página Home */}
              <Route exact path="/admin" component={HomePage} />

              {/* Página de Usuários */}
              <Route exact path="/admin/usuarios" component={UsuarioPage} />
              <Route
                path="/admin/usuarios/cadastrar"
                component={UsuarioFormPage}
              />
              <Route
                path="/admin/usuarios/alterar/:id"
                component={UsuarioFormPage}
              />

              {/* Página de Associações */}
              <Route
                exact
                path="/admin/associacoes"
                component={AssociacaoPage}
              />
              <Route
                exact
                path="/admin/associacoes/cadastrar"
                component={AssociacaoFormPage}
              />
              <Route
                path="/admin/associacoes/alterar/:id"
                component={AssociacaoFormPage}
              />

              {/* Página de Conta */}
              <Route exact path="/admin/conta" component={ContaPage} />
              <Route
                exact
                path="/admin/conta/cadastrar"
                component={ContaFormPage}
              />
              <Route
                path="/admin/conta/alterar/:id"
                component={ContaFormPage}
              />

              {/* Página de Fluxo Caixa */}
              <Route exact path="/admin/caixa" component={CaixaPage} />
              <Route
                exact
                path="/admin/caixa/cadastrar"
                component={CaixaFormPage}
              />
              <Route
                path="/admin/caixa/alterar/:id"
                component={CaixaFormPage}
              />
              <Route path="/admin/caixa/report" component={ReportFormPage} />

              {/* Página de Centros Comunitários */}
              <Route
                exact
                path="/admin/centros-comunitarios"
                component={CentroComunitarioPage}
              />
              <Route
                exact
                path="/admin/centros-comunitarios/cadastrar"
                component={CentroComunitarioFormPage}
              />
              <Route
                exact
                path="/admin/centros-comunitarios/alterar/:id"
                component={CentroComunitarioFormPage}
              />

              {/* Página de Responsáveis */}
              <Route
                exact
                path="/admin/responsaveis"
                component={ResponsaveisPage}
              />
              <Route
                exact
                path="/admin/responsaveis/cadastrar"
                component={ResponsaveisFormPage}
              />
              <Route
                exact
                path="/admin/responsaveis/alterar/:id"
                component={ResponsaveisFormPage}
              />

              {/* Página de Eventos */}
              <Route exact path="/admin/eventos" component={EventosPage} />
              <Route
                exact
                path="/admin/eventos/cadastrar"
                component={EventosFormPage}
              />
              <Route
                exact
                path="/admin/eventos/alterar/:id"
                component={EventosFormPage}
              />

              {/* Página de Cargos */}
              <Route exact path="/admin/cargos" component={CargosPage} />
              <Route
                exact
                path="/admin/cargos/cadastrar"
                component={CargosFormPage}
              />
              <Route
                exact
                path="/admin/cargos/alterar/:id"
                component={CargosFormPage}
              />

              {/* Página de Locação */}
              <Route exact path="/admin/locacao" component={LocacaoPage} />
              <Route
                exact
                path="/admin/locacao/cadastrar"
                component={LocacaoFormPage}
              />
              <Route
                exact
                path="/admin/locacao/alterar/:id"
                component={LocacaoFormPage}
              />

              {/* Página de Postagem */}
              <Route exact path="/admin/postagem" component={PostagemPage} />
              <Route
                exact
                path="/admin/postagem/cadastrar"
                component={PostagemFormPage}
              />
              <Route
                exact
                path="/admin/postagem/alterar/:id"
                component={PostagemFormPage}
              />
              <Route
                exact
                path="/admin/postagem/upload/:id"
                component={PostagemArquivo}
              />

              {/* Página de Postagem */}
              <Route
                exact
                path="/admin/documentos"
                component={DocumentosPage}
              />
              <Route
                exact
                path="/admin/documentos/cadastrar"
                component={DocumentosFormPage}
              />
              {/* <Route
                exact
                path="/admin/uploadarq/alterar/:id"
                component={PostagemFormPage}
              /> */}

              {/* Página Grupos de Permissões */}
              <Route
                exact
                path="/admin/grupos-de-permissoes"
                component={GrupoPage}
              />
              <Route
                exact
                path="/admin/grupos-de-permissoes/cadastrar"
                component={GrupoFormPage}
              />
              <Route
                exact
                path="/admin/grupos-de-permissoes/alterar/:id"
                component={GrupoFormPage}
              />
            </Switch>
          </MainContent>
        </Fragment>
      </Router>
    )
  }
}

export default AdminRoutes
