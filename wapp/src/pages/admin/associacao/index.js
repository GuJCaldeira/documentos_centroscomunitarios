import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import ReactTable from 'react-table'
import { ButtonGroup } from 'reactstrap'
import axiosClient from 'helpers/axiosClient'

import filterCaseInsensitive from 'helpers/filterCaseInsensitive'

class AssociacaoLista extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      associacoes: [],
      loading: false
    }

    this.excluirAssociacao = this.excluirAssociacao.bind(this)
  }

  componentDidMount() {
    this.setState({ loading: true })
    axiosClient
      .get('painel/associacao/listar')
      .then(response => {
        if (
          response.data.status &&
          response.data.obj &&
          Array.isArray(response.data.obj.associacoes)
        ) {
          this.setState({
            associacoes: response.data.obj.associacoes.map(associacao => {
              return {
                id_associacao: associacao.id_associacao,
                nome_fantasia: associacao.nome_fantasia,
                razao_social: associacao.razao_social,
                data_criacao: associacao.data_criacao,
                status: associacao.status,
                nome_centro: associacao.nome_centro
              }
            })
          })
        } else {
          console.error(response.data.erro)
        }

        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)

        this.setState({ loading: false })
      })
  }

  componentDidCatch(error, info) {
    console.log(error, info)

    this.setState({
      associacoes: []
    })
  }

  excluirAssociacao(id) {
    // Confirmação de exclusão
    Swal({
      title: 'Você tem certeza?',
      text: 'Você não poderá reverter isso!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, quero excluir a associação!'
    }).then(result => {
      // Se "result.value" for igual a "true" exclúi a associação
      if (result.value) {
        axiosClient
          .post('/painel/associacao/deletar', { id_associacao: id })
          .then(response => {
            // Excluindo um registro da lista
            if (response.data.status) {
              this.setState(prevState => ({
                associacoes: prevState.associacoes.filter(
                  associacao => id !== associacao.id_associacao
                )
              }))
              Swal('Excluído!', response.data.message, 'success')
            } else {
              Swal('Erro!', response.data.erro, 'error')
            }
          })
      }
    })
  }

  render() {
    const { associacoes } = this.state

    const columns = [
      {
        Header: 'Código',
        accessor: 'id_associacao',
        width: 70,
        filterable: false
      },
      {
        Header: 'Nome Fantasia',
        accessor: 'nome_fantasia'
      },
      /*{
        Header: 'Razão Social',
        accessor: 'razao_social'
      },*/
      {
        Header: 'Centro Comunitário',
        accessor: 'nome_centro'
      },
      {
        Header: 'Status',
        Cell: props => (
          <div
            className="d-flex justify-content-center align-items-center "
            style={{ width: 60, height: 30 }}
          >
            <span
              className={`d-flex justify-content-center align-items-center text-center badge badge-${
                props.original.status === 'A' ? 'success' : 'secondary'
              }`}
              style={{ width: 50, height: 30 }}
            >
              {props.original.status === 'A' ? 'Ativo' : 'Inativo'}
            </span>
          </div>
        ),
        filterMethod: (filter, row) => {
          if(filter.value === "todos") return true
          
          return row._original.status === filter.value
        },
        Filter: ({ filter, onChange }) =>
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%" }}
            value={filter ? filter.value : "todos"}
          >
            <option value="todos">Todos</option>
            <option value="A">Ativo</option>
            <option value="I">Inativo</option>  
          </select>,
        width: 100
      },
      {
        Header: 'Ações',
        Cell: props => (
          <div className="d-flex justify-content-center align-items-center">
            <ButtonGroup size="sm">
              <Link
                to={`/admin/associacoes/alterar/${props.row.id_associacao}`}
                className="btn btn-warning text-white"
              >
                <i className="fas fa-pencil-alt" />{' '}
                <span className="d-none d-sm-inline">Editar</span>
              </Link>
              <button
                onClick={() => this.excluirAssociacao(props.row.id_associacao)}
                className="btn btn-danger"
              >
                <i className="fas fa-times" />{' '}
                <span className="d-none d-sm-inline">Excluir</span>
              </button>
            </ButtonGroup>
          </div>
        ),
        minWidth: 95,
        filterable: false
      }
    ]
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card ">
              <div className="card-header ">
                <div className="row align-items-center justify-content-between">
                  <div>
                    <h4 className="card-title">Lista de Associações</h4>
                    <p className="card-category">Associações do sistema</p>
                  </div>
                  <Link
                    className="btn btn-success float-right"
                    to="/admin/associacoes/cadastrar"
                  >
                    <i className="fas fa-plus-circle" />{' '}
                    <span className="d-none d-sm-inline">
                      Cadastrar Associação
                    </span>
                  </Link>
                </div>
              </div>
              <div className="card-body">
                <ReactTable
                  columns={columns}
                  data={associacoes}
                  defaultPageSize={10}
                  filterable
                  defaultFilterMethod={filterCaseInsensitive}
                  className="-striped -highlight table-responsive"
                  previousText="Anterior"
                  nextText="Próximo"
                  loadingText="Carregando..."
                  noDataText="Nenhuma associação foi encontrada"
                  pageText="Pág."
                  ofText="de"
                  rowsText="linhas"
                />
              </div>
              <div className="card-footer ">
                <div className="stats">
                  <i className="fa fa-clock-o" /> Campaign sent 2 days ago
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default AssociacaoLista
