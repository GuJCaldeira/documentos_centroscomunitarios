import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import classNames from 'classnames'
import cepPromise from 'cep-promise'
import MaskedInput from 'react-maskedinput'
import DatePicker from 'react-datepicker'
import Select from 'react-select'
import moment from 'moment'

import FormValidator from 'helpers/FormValidator'

import axiosClient from 'helpers/axiosClient'
import { Redirect } from 'react-router-dom'

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: 'red' }}>*</span>
  </label>
)

class AssociacaoForm extends Component {
  constructor(props) {
    super(props)

    this.validator = new FormValidator([
      {
        field: 'razao_social',
        method: 'isEmpty',
        validWhen: false,
        message: 'A razão social é obrigatória.'
      },
      {
        field: 'nome_fantasia',
        method: 'isEmpty',
        validWhen: false,
        message: 'O nome fantasia é obrigatório'
      },
      {
        field: 'cnpj',
        method: 'isEmpty',
        validWhen: false,
        message: 'O CNPJ é obrigatório'
      },
      {
        field: 'email',
        method: 'isEmail',
        validWhen: true,
        message: 'O email é inválido.'
      },
      {
        field: 'telefone1',
        method: 'isEmpty',
        validWhen: false,
        message: 'O telefone é obrigatório'
      },
      {
        field: 'telefone2',
        method: 'isEmpty',
        validWhen: false,
        message: 'O celular é obrigatório'
      },
      {
        field: 'finalidade_estatutaria',
        method: 'isEmpty',
        validWhen: false,
        message: 'A finalidade estatutaria é obrigatória'
      },
      {
        field: 'dataConstituicao',
        method: 'isEmpty',
        validWhen: false,
        message: 'A data constituição é obrigatória'
      },
      {
        field: 'cep',
        method: 'isEmpty',
        validWhen: false,
        message: 'O CEP é obrigatório'
      },
      {
        field: 'endereco',
        method: 'isEmpty',
        validWhen: false,
        message: 'O endereço é obrigatório'
      },
      {
        field: 'numero',
        method: 'isEmpty',
        validWhen: false,
        message: 'O número é obrigatório'
      },
      {
        field: 'bairro',
        method: 'isEmpty',
        validWhen: false,
        message: 'O bairro é obrigatório'
      },
      {
        field: 'cidade',
        method: 'isEmpty',
        validWhen: false,
        message: 'O cidade é obrigatório'
      },
      {
        field: 'cidade',
        method: 'isEmpty',
        validWhen: false,
        message: 'O cidade é obrigatório'
      },

      {
        field: 'estado',
        method: 'isEmpty',
        validWhen: false,
        message: 'O estado é obrigatório'
      }
    ])

    this.state = {
      id_associacao: '',
      razao_social: '',
      nome_fantasia: '',
      cnpj: '',
      telefone1: '',
      telefone2: '',
      finalidade_estatutaria: '',
      dataConstituicao: moment(),
      email: '',
      cep: '',
      endereco: '',
      numero: '',
      bairro: '',
      cidade: '',
      estado: '',
      centro_comunitario: null,
      status: 'A',
      modoEdit: false,

      centrosOptions: [],
      centrosOptionsLoading: false,

      redirect: false,

      validation: this.validator.valid(),
      submitted: false,
      loadingData: false
    }

    this.submitted = false
  }

  componentDidMount() {
    this.setState({
      loadingData: true
    })
    this.getOptions().then(() => {
      if (this.props.match.params && this.props.match.params.id) {
        const id = this.props.match.params.id
        this.setState({
          modoEdit: true
        })
        this.getUserById(id)
      } else {
        this.setState({
          loadingData: false
        })
      }
    })
  }

  getUserById = async id => {
    try {
      const response = await axiosClient.get('/painel/associacao/listar/' + id)
      if (response.data.status) {
        let associacao = response.data.obj.associacoes[0]
        associacao.data_constituicao = moment(
          associacao.data_constituicao,
          'YYYY-MM-DD HH:mm:ss'
        ).isValid()
          ? moment(associacao.data_constituicao, 'YYYY-MM-DD HH:mm:ss')
          : moment()

        this.setState({
          id_associacao: associacao.id_associacao || '',
          razao_social: associacao.razao_social || '',
          nome_fantasia: associacao.nome_fantasia || '',
          cnpj: associacao.cnpj || '',
          telefone1: associacao.telefone1 || '',
          telefone2: associacao.telefone2 || '',
          finalidade_estatutaria: associacao.finalidade_estatutaria || '',
          dataConstituicao: associacao.data_constituicao,
          email: associacao.email || '',
          cep: associacao.cep || '',
          endereco: associacao.endereco || '',
          numero: associacao.numero || '',
          bairro: associacao.bairro || '',
          cidade: associacao.cidade || '',
          estado: associacao.estado || '',
          status: associacao.status || '',
          centro_comunitario: this.state.centrosOptions.find(
            x => x.value === associacao.id_centro_comunitario
          ),
          loadingData: false
        })
      } else {
        Swal({
          type: 'error',
          title: 'Ocorreu um problema, por favor, tente novamente...',
          html: response.data.erro
        })
      }
    } catch (err) {
      console.error(err)
    }
  }

  handleChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  handleSubmit = e => {
    e.preventDefault()

    const validation = this.validator.validate(this.state)
    this.setState({ validation, submitted: true })
    this.submitted = true

    if (validation.isValid) {
      let {
        id_associacao,
        razao_social,
        nome_fantasia,
        cnpj,
        telefone1,
        telefone2,
        finalidade_estatutaria,
        dataConstituicao,
        email,
        cep,
        endereco,
        numero,
        bairro,
        cidade,
        estado,
        centro_comunitario,
        status
      } = this.state
      dataConstituicao = moment(dataConstituicao).format('YYYY-MM-DD HH:mm:ss')
      cnpj = cnpj.replace(/\D/g, '')

      if (this.props.match.params && this.props.match.params.id) {
        axiosClient
          .post('/painel/associacao/atualizar', {
            id_associacao,
            razao_social,
            nome_fantasia,
            cnpj,
            telefone1: telefone1,
            telefone2: telefone2,
            finalidade_estatutaria,
            data_constituicao: dataConstituicao,
            email,
            cep,
            endereco,
            numero,
            bairro,
            cidade,
            estado,
            id_centro_comunitario: centro_comunitario.value,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.setState({
                redirect: true
              })
              this.submitted = false
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Oops...',
                html: response.data.erro
              })
            }
          })
          .catch(error => {
            console.error(error)
          })
      } else {
        axiosClient
          .post('/painel/associacao/inserir', {
            razao_social,
            nome_fantasia,
            cnpj,
            telefone1: telefone1,
            telefone2: telefone2,
            finalidade_estatutaria,
            data_constituicao: dataConstituicao,
            email,
            id_centro_comunitario: centro_comunitario.value,
            cep,
            endereco,
            numero,
            bairro,
            cidade,
            estado,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.obj.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Oops...',
                html: response.data.erro
              })
            }
          })
          .catch(error => {
            console.error(error)
          })
      }
    } else {
      this.setState({ submitted: false })
    }
  }

  handleChangeCep = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })

    this.pesquisaCep(value)
  }

  pesquisaCep = cep => {
    cepPromise(cep)
      .then(response => {
        console.log(response)
        this.setState({
          estado: response.state,
          cidade: response.city,
          bairro: response.neighborhood,
          endereco: response.street
        })
      })
      .catch(console.error)
  }

  getOptions = () => {
    return new Promise((resolve, reject) => {
      this.setState({
        centrosOptionsLoading: true
      })
      axiosClient
        .get('/painel/comum/listarCentros')
        .then(centrosResponse => {
          const centrosOptions = centrosResponse.data.obj.centro_comunitario
            .map(centro_comunitario => {
              return {
                ...centro_comunitario,
                value: centro_comunitario.id_centro_comunitario,
                label: centro_comunitario.nome_centro
              }
            })
            .filter(centro_comunitario => centro_comunitario.status === 'A')

          this.setState({
            centrosOptions,
            centrosOptionsLoading: false
          })
          resolve()
        })
        .catch(error => {
          console.log(error, "oops.. couldn't get options")
          reject(error)
        })
    })
  }

  render() {
    const { loadingData } = this.state
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation
    const inputClassNames = isInvalid => {
      return classNames('form-control', { 'is-invalid': isInvalid })
    }
    //Redirecionamento para a lista inicial deste formulário
    if (this.state.redirect) return <Redirect to="/admin/associacoes" />

    return (
      <div>
        <Link className="btn btn-secondary mb-2" to="/admin/associacoes/">
          {' '}
          <i className="fas fa-arrow-left" /> Voltar a lista
        </Link>

        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Formulário da Associação</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>
        <fieldset disabled={loadingData}>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-md-10">
                <div className="card">
                  <div
                    className={classNames('card-body', {
                      'is-loading is-loading-lg': loadingData
                    })}
                  >
                    <h3>Informações Cadastrais</h3>
                    <div className="form-group">
                      <LabelRiquered>Razão Social</LabelRiquered>
                      <input
                        type="text"
                        name="razao_social"
                        placeholder="Informe a razão social"
                        className={inputClassNames(
                          validation.razao_social.isInvalid
                        )}
                        value={this.state.razao_social}
                        onChange={this.handleChange}
                      />
                      <div className="invalid-feedback">
                        {validation.razao_social.message}
                      </div>
                    </div>

                    <div className="form-group">
                      <LabelRiquered>Nome Fantasia</LabelRiquered>
                      <input
                        type="text"
                        name="nome_fantasia"
                        placeholder="Informe nome fantasia"
                        className={inputClassNames(
                          validation.nome_fantasia.isInvalid
                        )}
                        value={this.state.nome_fantasia}
                        onChange={this.handleChange}
                      />
                      <div className="invalid-feedback">
                        {validation.nome_fantasia.message}
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="col-md-4">
                        <div className="form-group">
                          <LabelRiquered>CNPJ</LabelRiquered>
                          <MaskedInput
                            type="text"
                            name="cnpj"
                            mask="11.111.111/1111-11"
                            placeholder="Informe o CNPJ"
                            className={inputClassNames(
                              validation.cnpj.isInvalid
                            )}
                            value={this.state.cnpj}
                            onChange={this.handleChange}
                          />
                          <div className="invalid-feedback">
                            {validation.cnpj.message}
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4">
                        <div className="form-group">
                          <LabelRiquered>Telefone</LabelRiquered>
                          <MaskedInput
                            type="text"
                            name="telefone1"
                            mask="(11) 1111-1111"
                            placeholder="(00) 0000-0000"
                            className={inputClassNames(
                              validation.telefone1.isInvalid
                            )}
                            value={this.state.telefone1}
                            onChange={this.handleChange}
                          />
                          <div className="invalid-feedback">
                            {validation.telefone1.message}
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4">
                        <div className="form-group">
                          <LabelRiquered>Celular</LabelRiquered>
                          <MaskedInput
                            type="text"
                            name="telefone2"
                            mask="(11) 1 1111-1111"
                            placeholder="(00) 9 0000-0000"
                            className={inputClassNames(
                              validation.telefone2.isInvalid
                            )}
                            value={this.state.telefone2}
                            onChange={this.handleChange}
                          />
                          <div className="invalid-feedback">
                            {validation.telefone2.message}
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="form-group">
                      <LabelRiquered>Finalidade Estatutária</LabelRiquered>
                      <input
                        type="text"
                        name="finalidade_estatutaria"
                        placeholder="Informe a finalidade estruturaria"
                        className={inputClassNames(
                          validation.finalidade_estatutaria.isInvalid
                        )}
                        value={this.state.finalidade_estatutaria}
                        onChange={this.handleChange}
                      />
                      <div className="invalid-feedback">
                        {validation.finalidade_estatutaria.message}
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <LabelRiquered>Data Constituição</LabelRiquered>
                        <DatePicker
                          selected={this.state.dataConstituicao}
                          className="form-control"
                          maxDate={moment()}
                          onChange={dataConstituicao => {
                            this.setState({ dataConstituicao })
                          }}
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          placeholderText="XX/XX/XXXX"
                        />
                        <div className="invalid-feedback">
                          {/* {validation.dataConstituicao.message} */}
                        </div>
                      </div>
                      <div className="form-group col-md-8">
                        <div className="form-group">
                          <LabelRiquered>Email</LabelRiquered>
                          <input
                            type="text"
                            name="email"
                            placeholder="Informe o e-mail"
                            className={inputClassNames(
                              validation.email.isInvalid
                            )}
                            value={this.state.email}
                            onChange={this.handleChange}
                          />
                          <div className="invalid-feedback">
                            {validation.email.message}
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <label>Centro Comunitário</label>
                        <Select
                          className="basic-single"
                          classNamePrefix="select"
                          isMulti={false}
                          value={this.state.centro_comunitario}
                          isLoading={this.state.centrosOptionsLoading}
                          isSearchable={true}
                          name="centro_comunitario"
                          placeholder="Selecione..."
                          options={this.state.centrosOptions}
                          onChange={selectedOption =>
                            this.setState({
                              centro_comunitario: selectedOption
                            })
                          }
                        />
                      </div>
                    </div>

                    <hr />

                    <h3>Endereço</h3>
                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <LabelRiquered htmlFor="cep">CEP</LabelRiquered>
                        <MaskedInput
                          type="text"
                          name="cep"
                          mask="11111-111"
                          value={this.state.cep}
                          className={inputClassNames(validation.cep.isInvalid)}
                          placeholder="Pesquisar o CEP"
                          onChange={this.handleChangeCep}
                        />
                        <div className="invalid-feedback">
                          {validation.cep.message}
                        </div>
                      </div>
                      <div className="form-group col-md-9">
                        <LabelRiquered htmlFor="endereco">
                          Endereço
                        </LabelRiquered>
                        <input
                          type="text"
                          name="endereco"
                          placeholder="Informe o endereço"
                          value={this.state.endereco}
                          className={inputClassNames(
                            validation.endereco.isInvalid
                          )}
                          id="endereco"
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.endereco.message}
                        </div>
                      </div>
                      <div className="form-group col-md-3">
                        <LabelRiquered htmlFor="numero">Número</LabelRiquered>
                        <input
                          type="text"
                          name="numero"
                          placeholder="000"
                          value={this.state.numero}
                          className={inputClassNames(
                            validation.numero.isInvalid
                          )}
                          id="numero"
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.numero.message}
                        </div>
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="form-group col-md-7">
                        <LabelRiquered htmlFor="bairro">Bairro</LabelRiquered>
                        <input
                          type="text"
                          name="bairro"
                          placeholder="Informe o bairro"
                          value={this.state.bairro}
                          className={inputClassNames(
                            validation.bairro.isInvalid
                          )}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.bairro.message}
                        </div>
                      </div>
                      <div className="form-group col-md-7">
                        <LabelRiquered htmlFor="cidade">Cidade</LabelRiquered>
                        <input
                          type="text"
                          name="cidade"
                          placeholder="Informe a cidade"
                          value={this.state.cidade}
                          className={inputClassNames(
                            validation.cidade.isInvalid
                          )}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.cidade.message}
                        </div>
                      </div>
                      <div className="form-group col-md-5">
                        <LabelRiquered htmlFor="inputState">
                          Estado
                        </LabelRiquered>
                        <input
                          id="inputState"
                          type="text"
                          name="estado"
                          placeholder="Sigla da UF"
                          value={this.state.estado}
                          className={inputClassNames(
                            validation.estado.isInvalid
                          )}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.estado.message}
                        </div>
                      </div>
                    </div>
                    <hr />
                    <div className="form-row">
                      <div className="form-group col-md-3">
                        <label>Status</label>
                        <select
                          name="status"
                          className={inputClassNames()}
                          onChange={this.handleChange}
                          value={this.state.status}
                        >
                          <option value="A">Ativo</option>
                          <option value="I">Inativo</option>
                        </select>
                      </div>
                    </div>
                    <button type="submit" className="btn btn-success">
                      Salvar
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </fieldset>
      </div>
    )
  }
}

export default AssociacaoForm
