import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import classNames from 'classnames'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import Select from 'react-select'

import FormValidator from 'helpers/FormValidator'
import axiosClient from 'helpers/axiosClient'

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: 'red' }}>*</span>
  </label>
)
class EventosForm extends Component {
  constructor(props) {
    super(props)

    this.validator = new FormValidator([
      // {
      //   field: 'nome_evento',
      //   method: 'isEmpty',
      //   validWhen: false,
      //   message: 'O nome do evento é obrigatório.'
      // },
      // {
      //   field: 'descricao',
      //   method: 'isEmpty',
      //   validWhen: false,
      //   message: 'O campo descrição é obrigatório.'
      // },
      {
        field: 'tipo_evento',
        method: 'isEmpty',
        validWhen: false,
        message: 'O tipo do evento é obrigatório.'
      },
      {
        field: 'dataInicio',
        method: 'isEmpty',
        validWhen: false,
        message: 'A data do início do evento é obrigatória.'
      },
      {
        field: 'dataTermino',
        method: 'isEmpty',
        validWhen: false,
        message: 'A data de término do evento é obrigatória.'
      },

      // {
      //   field: 'dados_evento',
      //   method: 'isEmpty',
      //   validWhen: false,
      //   message: 'O campo dados é obrigatório.'
      // },

      {
        field: 'dados_evento',
        method: 'isEmpty',
        validWhen: false,
        message: 'O campo comentario é obrigatório.'
      },

      {
        field: 'valor',
        method: 'isEmpty',
        validWhen: false,
        message: 'Informe um valor'
      }
    ])

    this.state = {
      id_evento: '',
      tipo_evento: 'PV',
      valor: '',
      dataInicio: moment(),
      dataTermino: moment(),
      status: 'A',
      centro_comunitario: '',
      dados_evento: '',
      modoEdit: false,

      responsaveisOptions: [],
      responsaveisOptionsLoading: false,
      centro_comunitarioOptions: [],
      centro_comunitarioOptionsLoading: false,

      validation: this.validator.valid(),
      submitted: false,
      redirect: false,
      loadingData: false
    }

    this.submitted = false
  }
  z

  componentDidMount() {
    this.setState({
      loadingData: true
    })
    this.getOptions().then(() => {
      if (this.props.match.params && this.props.match.params.id) {
        const id = this.props.match.params.id
        this.setState({
          modoEdit: true
        })
        this.getUserById(id)
      } else {
        this.setState({
          loadingData: false
        })
      }
    })
  }

  isValidDate = data => {
    return moment(data).isValid()
  }

  getUserById = async id => {
    try {
      const response = await axiosClient.get('/painel/locacao/listar/' + id)
      if (response.data && response.data.status) {
        const locacao = response.data.obj.locacao[0]
        locacao.data_inicio = moment(
          locacao.data_inicio,
          'YYYY-MM-DD HH:mm:ss'
        ).isValid()
          ? moment(locacao.data_inicio, 'YYYY-MM-DD HH:mm:ss')
          : moment()

        locacao.data_final = moment(
          locacao.data_final,
          'YYYY-MM-DD HH:mm:ss'
        ).isValid()
          ? moment(locacao.data_final, 'YYYY-MM-DD HH:mm:ss')
          : moment()

        this.setState({
          id_evento: locacao.id_evento || '',
          dados_evento: locacao.dados_evento || '',
          dataInicio: locacao.data_inicio || '',
          dataTermino: locacao.data_final || '',
          tipo_evento: locacao.tipo_evento || '',
          valor: locacao.valor || '',
          responsavel: this.state.responsaveisOptions.find(
            x => x.value === locacao.id_responsavel
          ),
          centro_comunitario: this.state.centro_comunitarioOptions.find(
            x => x.value === locacao.id_centro_comunitario
          ),
          status: locacao.status || '',
          loadingData: false
        })
      } else {
        Swal({
          type: 'error',
          title: 'Ocorreu um problema, por favor, tente novamente...',
          html: response.data.erro
        })
      }
    } catch (err) {
      console.error(err)
      Swal({
        type: 'error',
        title: 'Ocorreu um problema, por favor, tente novamente...',
        html: err.message
      })
    }
  }

  handleChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  handleSubmit = e => {
    e.preventDefault()

    const validation = this.validator.validate(this.state)
    this.setState({ validation, submitted: true })
    this.submitted = true

    if (validation.isValid) {
      let {
        dados_evento,
        tipo_evento,
        valor,
        dataInicio,
        dataTermino,
        responsavel,
        centro_comunitario,
        status
      } = this.state
      dataInicio = moment(dataInicio).format('YYYY-MM-DD HH:mm:ss')
      dataTermino = moment(dataTermino).format('YYYY-MM-DD HH:mm:ss')

      if (this.props.match.params && this.props.match.params.id) {
        const id = this.props.match.params.id
        axiosClient
          .post('/painel/locacao/atualizar', {
            id,
            data_inicio: dataInicio,
            data_final: dataTermino,
            dados_evento,
            tipo_evento,
            valor,
            id_centro_comunitario: centro_comunitario.value,
            id_responsavel: responsavel.value,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Ocorreu um problema, por favor, tente novamente...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      } else {
        axiosClient
          .post('/painel/locacao/inserir', {
            dados_evento,
            data_inicio: dataInicio,
            data_final: dataTermino,
            tipo_evento,
            valor,
            id_centro_comunitario: centro_comunitario.value,
            id_responsavel: responsavel.value,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              Swal({
                position: 'top-end',
                type: 'success',
                title: 'Evento inserido com sucesso!',
                showConfirmButton: false,
                timer: 1500
              })
              this.setState({
                redirect: true
              })
            } else {
              Swal({
                type: 'error',
                title: 'Ocorreu um problema, por favor, tente novamente...',
                html: response.data.erro
              })
              this.setState({
                submitted: false
              })
            }
          })
          .catch(err => {
            console.log(err)
            Swal({
              type: 'error',
              title: 'Ocorreu um problema, por favor, tente novamente...',
              html: err.message
            })
          })
      }
    } else {
      this.setState({ submitted: false })
    }
  }

  getOptions = () => {
    return new Promise((resolve, reject) => {
      this.setState({
        responsaveisOptionsLoading: true,
        centro_comunitarioOptionsLoading: true
      })
      Promise.all([
        axiosClient.get('/painel/comum/listarResponsaveis'),
        axiosClient.get('/painel/comum/listarCentros')
      ])
        .then(([responsaveisResponse, centro_comunitarioResponse]) => {
          const centro_comunitarioOptions = centro_comunitarioResponse.data.obj.centro_comunitario
            .map(centro_comunitario => {
              return {
                ...centro_comunitario,
                value: centro_comunitario.id_centro_comunitario,
                label: centro_comunitario.nome_centro
              }
            })
            .filter(centro_comunitario => centro_comunitario.status === 'A')
          const responsaveisOptions = responsaveisResponse.data.obj.responsavel
            .map(responsavel => {
              return {
                ...responsavel,
                value: responsavel.id_responsavel,
                label: responsavel.nome
              }
            })
            .filter(responsavel => responsavel.status === 'A')

          this.setState(
            {
              responsaveisOptions,
              centro_comunitarioOptions,
              responsaveisOptionsLoading: false,
              centro_comunitarioOptionsLoading: false
            },
            resolve()
          )
        })
        .catch(error => {
          console.log(error, "ups... couldn't get options")
          reject(error)
        })
    })
  }

  render() {
    const { loadingData } = this.state
    const validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation
    const inputClassNames = isInvalid => {
      return classNames('form-control', { 'is-invalid': isInvalid })
    }

    if (this.state.redirect) return <Redirect to="/admin/locacao" />

    return (
      <div>
        <Link className="btn btn-secondary mb2" to="/admin/locacao">
          <i className="fas fa-arrow-left" />
          Voltar a lista
        </Link>
        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Formulário de Locação</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-10">
              <div className="card">
                <div
                  className={classNames('card-body', {
                    'is-loading is-loading-lg': loadingData
                  })}
                >
                  <h3>Informações cadastrais da locação</h3>

                  <div className="form-row">
                    <div className="form-group col-md-6">
                      <LabelRiquered>Nome do responsável</LabelRiquered>
                      <Select
                        name="responsavel"
                        value={this.state.responsavel}
                        isMulti={false}
                        options={this.state.responsaveisOptions}
                        isLoading={this.state.responsaveisOptionsLoading}
                        placeholder="Selecione..."
                        onChange={selectedOption =>
                          this.setState({ responsavel: selectedOption })
                        }
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <LabelRiquered>Centro comunitário</LabelRiquered>
                      <Select
                        classNamePrefix="select"
                        name="centro_comunitario"
                        value={this.state.centro_comunitario}
                        isMulti={false}
                        options={this.state.centro_comunitarioOptions}
                        isLoading={this.state.centro_comunitarioOptionsLoading}
                        placeholder="Selecione..."
                        isSearchable={true}
                        onChange={selectedOption =>
                          this.setState({ centro_comunitario: selectedOption })
                        }
                      />
                    </div>
                  </div>

                  <div className="form-row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <LabelRiquered>Data de início da locação</LabelRiquered>
                        <br />
                        <DatePicker
                          selected={this.state.dataInicio}
                          className="form-control"
                          showTimeSelect
                          dateFormat="DD/MM/YYYY HH:mm"
                          timeFormat="HH:mm"
                          minDate={moment()}
                          onChange={dataInicio => {
                            this.setState({ dataInicio })
                          }}
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          placeholderText="XX/XX/XXXX"
                        />

                        <div className="invalid-feedback">
                          {/* {validation.dataEvento.message} */}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <LabelRiquered>
                          Data de término da locação
                        </LabelRiquered>
                        <br />

                        <DatePicker
                          selected={this.state.dataTermino}
                          className="form-control"
                          showTimeSelect
                          dateFormat="DD/MM/YYYY HH:mm"
                          timeFormat="HH:mm"
                          minDate={moment()}
                          onChange={dataTermino => {
                            this.setState({ dataTermino })
                          }}
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                        />

                        <div className="invalid-feedback">
                          {/* {validation.dataEvento.message} */}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="form-row">
                    <div className="col-md-4">
                      <div className="form-group">
                        <LabelRiquered>Tipo do evento</LabelRiquered>
                        <select
                          name="tipo_evento"
                          className={inputClassNames()}
                          onChange={this.handleChange}
                          value={this.state.tipo_evento}
                          autoComplete={'off'}
                        >
                          <option value="PU">Público</option>
                          <option value="PV">Privado</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="form-group">
                        <LabelRiquered>Status</LabelRiquered>
                        <select
                          name="status"
                          className={inputClassNames()}
                          onChange={this.handleChange}
                          value={this.state.status}
                          autoComplete={'off'}
                        >
                          <option value="A">Ativo</option>
                          <option value="I">Inativo</option>
                          <option value="P">Pendente</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="form-group">
                        <label>Valor da locação</label>
                        <input
                          type="integer"
                          name="valor"
                          placeholder="Digite o valor do evento"
                          className="form-control"
                          value={this.state.valor}
                          onChange={this.handleChange}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="form-row">
                    <LabelRiquered>Dados do evento</LabelRiquered>
                    <textarea
                      name="dados_evento"
                      className={inputClassNames(
                        validation.dados_evento.isInvalid
                      )}
                      value={this.state.dados_evento}
                      onChange={this.handleChange}
                      placeholder="Descreva o evento."
                      rows="3"
                    />
                    <div className="invalid-feedback">
                      {validation.dados_evento.message}
                    </div>
                  </div>

                  <hr />

                  <button
                    type="submit"
                    className="btn btn-success"
                    disabled={this.state.submitted}
                  >
                    Salvar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default EventosForm
