import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import ReactTable from 'react-table'
import { ButtonGroup } from 'reactstrap'
import moment from 'moment'
import axiosClient from 'helpers/axiosClient'

import filterCaseInsensitive from 'helpers/filterCaseInsensitive'

class LocacaoLista extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      locacao: []
    }
  }

  componentDidMount() {
    this.setState({ loading: true })
    axiosClient
      .get('painel/locacao/listar')
      .then(response => {
        console.log(response)
        if (
          response.data.status &&
          response.data.obj &&
          Array.isArray(response.data.obj.locacao)
        ) {
          this.setState({
            locacao: response.data.obj.locacao.map(locacao => ({
              id_evento: locacao.id_evento,
              comentario: locacao.comentario,
              data_inicio: moment(
                locacao.data_inicio,
                'YYYY-MM-DD HH:mm:ss'
              ).format('DD/MM/YYYY HH:mm'),
              data_final: moment(
                locacao.data_final,
                'YYYY-MM-DD HH:mm:ss'
              ).format('DD/MM/YYYY HH:mm'),
              tipo_evento: locacao.tipo_evento,
              valor: locacao.valor,
              id_responsavel: locacao.id_responsavel,
              id_centro_comunitario: locacao.id_centro_comunitario,
              status: locacao.status,
              responsavel_nome: locacao.responsavel_nome
            }))
          })
        } else {
          console.error(response.data.erro)
        }

        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)

        this.setState({ loading: false })
      })
  }

  componentDidCatch(error, info) {
    console.log(error, info)

    this.setState({
      locacao: []
    })
  }

  toggleStatus = locacaoAtual => {
    // Confirmação de exclusão
    Swal({
      title: 'Você tem certeza?',
      text: `Tem certeza que quer ${
        locacaoAtual.status === 'A' ? 'DESATIVAR' : 'ATIVAR'
      } a locação "${locacaoAtual.id_evento}"?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Sim, quero ${
        locacaoAtual.status === 'A' ? 'DESATIVAR' : 'ATIVAR'
      } a locação!`
    }).then(result => {
      // Se "result.value" for igual a "true" exclúi o locação
      if (result.value) {
        axiosClient
          .post('/painel/locacao/atualizar', {
            ...locacaoAtual,
            status: locacaoAtual.status === 'A' ? 'I' : 'A'
          })
          .then(response => {
            // Excluindo um registro da lista
            if (response.status)
              this.setState(prevState => ({
                locacao: prevState.locacao.map(
                  locacao =>
                    locacaoAtual.id_evento !== locacao.id_evento
                      ? locacao
                      : {
                          ...locacaoAtual,
                          status: locacaoAtual.status === 'A' ? 'I' : 'A'
                        }
                )
              }))
            Swal(
              'Alterado!',
              'Status da locação alterado com sucesso.',
              'success'
            )
          })
      }
    })
  }

  render() {
    // definindo colunas para a tabela
    const columns = [
      /*{
        Header: 'Evento', // Cabeçalho da coluna
        accessor: 'id_evento', // String-based value accessors!
        width: 70
      },*/
      {
        Header: 'Responsavel',
        accessor: 'responsavel_nome' // String-based value accessors!
      },
      {
        Header: 'Data de Final',
        accessor: 'data_final' // String-based value accessors!
      },
      {
        Header: 'Data de Inicial',
        accessor: 'data_inicio' // String-based value accessors!
      },
      {
        Header: 'Status',
        Cell: props => (
          <div
            className="d-flex justify-content-center align-items-center "
            style={{ width: 60, height: 30 }}
          >
            <span
              className={`d-flex justify-content-center align-items-center text-center badge badge-${
                props.original.status === 'A' ? 'success' : 'secondary'
              }`}
              style={{ width: 50, height: 30 }}
            >
              {props.original.status === 'A' ? 'Ativo' : 'Inativo'}
            </span>
          </div>
        ),

        filterMethod: (filter, row) => {
          if (filter.value === 'todos') return true

          return row._original.status === filter.value
        },
        Filter: ({ filter, onChange }) => (
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: '100%' }}
            value={filter ? filter.value : 'todos'}
          >
            <option value="todos">Todos</option>
            <option value="A">Ativo</option>
            <option value="I">Inativo</option>
          </select>
        ),
        width: 100
      },
      {
        Header: 'Ações',
        filterable: false,
        Cell: props => (
          <div className="d-flex justify-content-center align-items-center">
            <ButtonGroup size="sm">
              <Link
                to={`/admin/locacao/alterar/${props.original.id_evento}`}
                className="btn btn-warning text-white"
              >
                <i className="fas fa-pencil-alt" />{' '}
                <span className="d-none d-sm-inline">Editar</span>
              </Link>
              {props.original.status === 'A' ? (
                <button
                  onClick={() => this.toggleStatus(props.original)}
                  className="btn btn-danger"
                >
                  <i className="fas fa-times" />{' '}
                  <span className="d-none d-sm-inline">Desativar</span>
                </button>
              ) : (
                <button
                  onClick={() => this.toggleStatus(props.original)}
                  className="btn btn-info"
                >
                  <i className="fas fa-times" />{' '}
                  <span className="d-none d-sm-inline">Ativar</span>
                </button>
              )}
            </ButtonGroup>
          </div>
        )
      }
    ]
    console.log(this.state)

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card ">
              <div className="card-header ">
                <div className="row align-items-center justify-content-between">
                  <div>
                    <h4 className="card-title">Lista de Locação</h4>
                    <p className="card-category">
                      Grupos de locação do sistema
                    </p>
                  </div>
                  <Link
                    className="btn btn-success float-right"
                    to="/admin/locacao/cadastrar"
                  >
                    <i className="fas fa-plus-circle" />{' '}
                    <span className="d-none d-sm-inline">
                      Cadastrar Locação
                    </span>
                  </Link>
                </div>
              </div>
              <div className="card-body">
                <ReactTable
                  columns={columns}
                  data={this.state.locacao}
                  loading={this.state.loading}
                  defaultPageSize={10}
                  filterable
                  defaultFilterMethod={filterCaseInsensitive}
                  className="-striped -highlight"
                  previousText="Anterior"
                  nextText="Próximo"
                  loadingText="Carregando..."
                  noDataText="Nenhuma locação foi encontrado"
                  pageText="Pág."
                  ofText="de"
                  rowsText="linhas"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default LocacaoLista
