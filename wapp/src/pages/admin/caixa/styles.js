import styled from 'styled-components'

export const Container = styled.div`
.sumirComDiv {
  display:none;
}
.alinha-direita {
  text-align: right;
}
`

export const CalendarContainer = styled.div`
  height: 700px;
  @media (max-width: 1040px) {
    .rbc-toolbar {
      flex-direction: column;
    }
  }
`
