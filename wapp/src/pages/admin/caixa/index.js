import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ReactTable from 'react-table'
import { ButtonGroup } from 'reactstrap'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import moment from 'moment'
import axiosClient from 'helpers/axiosClient'
import { Container } from './styles'

import filterCaseInsensitive from 'helpers/filterCaseInsensitive'

class Caixa extends Component {
  constructor(props) {
    super(props)

    // definindo o estado inicial das variáveis que serão usadas no componente
    this.state = {
      caixa: [],
      loading: false,
      modal: false,
      exibeSaldo: false,
      saldo: 0.0,
      caixaView: {}
    }

    //    this.excluirCaixa = this.excluirCaixa.bind(this)
  }

  // após a renderização do componente
  componentDidMount() {
    this.setState({ loading: true })
    axiosClient
      .get('painel/caixa/listar')
      .then(response => {
        if (response.data.obj && Array.isArray(response.data.obj.caixa)) {
          this.setState({
            saldo: response.data.obj.saldo,
            exibeSaldo: response.data.obj.exibeSaldo
          })
          //console.log(response.data);
          //console.log(this.state);

          this.setState({
            caixa: response.data.obj.caixa.map(caixa => {
              return {
                id_caixa: caixa.id_caixa,
                descricao: caixa.descricao,
                data_lancamento: moment(
                  caixa.data_lancamento,
                  'YYYY-MM-DD HH:mm:ss'
                ).format('DD/MM/YYYY HH:mm'),
                valor: parseFloat(caixa.valor).toLocaleString('pt-BR', {
                  minimumFractionDigits: 2,
                  style: 'currency',
                  currency: 'BRL'
                }),
                associacao: caixa.id_associacao,
                conta: caixa.id_conta,
                nome_conta: caixa.nome_conta,
                movimento: caixa.movimento
              }
            })
          })
        } else {
          console.error(response.data.erro)
        }

        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)

        this.setState({ loading: false })
      })
  }

  componentDidCatch(error, info) {
    console.log(error, info)

    this.setState({
      caixa: []
    })
  }

  toggle = data => {
    this.setState(prevState => ({
      modal: !prevState.modal,
      caixaView: data
    }))
  }

  /* excluirUsuario(id) {
    // Confirmação de exclusão
    Swal({
      title: 'Você tem certeza?',
      text: 'Você não poderá reverter isso!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, quero excluir o usuário!'
    }).then(result => {
      // Se "result.value" for igual a "true" excluí o usuário
      if (result.value) {
        axiosClient
          .post('/painel/usuario/deletar', { id_usuario: id })
          .then(response => {
            // Excluindo um registro da lista
            if (response.data.status)
              this.setState(prevState => ({
                usuarios: prevState.usuarios.filter(
                  usuario => id !== usuario.id_usuario
                )
              }))
            Swal('Excluído!', 'Usuário excluído com sucesso.', 'success')
          })
      }
    })
  }*/

  render() {
    // definindo colunas para a tabela
    const columns = [
      {
        Header: 'Data lançamento',
        accessor: 'data_lancamento' // String-based value accessors!
      },
      {
        Header: 'Conta',
        accessor: 'nome_conta' // String-based value accessors!
      },
      {
        Header: 'Descrição',
        accessor: 'descricao' // String-based value accessors!
      },
      {
        Header: 'Valor',
        accessor: 'valor', // String-based value accessors!
        Cell: props => <div className="alinha-direita">{props.value}</div>
      },
      {
        Header: 'Movimento',
        Cell: props => (
          <div className="d-flex justify-content-center align-items-center">
            <span
              className={`d-flex justify-content-center align-items-center text-center badge badge-${
                props.original.movimento === 'E' ? 'success' : 'danger'
              }`}
              style={{ width: 50, height: 30 }}
            >
              {props.original.movimento === 'E' ? 'Entrada' : 'Saída'}
            </span>
          </div>
        ),
        filterMethod: (filter, row) => {
          if (filter.value === 'todos') return true

          return row._original.movimento === filter.value
        },
        Filter: ({ filter, onChange }) => (
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: '100%' }}
            value={filter ? filter.value : 'todos'}
          >
            <option value="todos">Todos</option>
            <option value="E">Entrada</option>
            <option value="S">Saida</option>
          </select>
        ),
        width: 100
      },
      {
        Header: 'Ações',
        Cell: props => (
          <div className="d-flex justify-content-center align-items-center">
            <ButtonGroup size="sm">
              <Button
                onClick={() => this.toggle(props.original)}
                className="btn btn-info text-white"
              >
                <i className="fas fa-eye" />{' '}
                <span className="d-none d-sm-inline font-weight-bold">
                  Visualizar
                </span>
              </Button>
            </ButtonGroup>
          </div>
        ),
        filterable: false,
        width: 167
      }
    ]
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="card ">
                <div className="card-header ">
                  <div className="row align-items-center justify-content-between">
                    <div>
                      <h4 className="card-title">Fluxo de Caixa</h4>
                      <p className="card-category">
                        Fluxo de Caixa do Centro Comunitário
                      </p>
                    </div>
                  </div>

                  <div className="col-md-3 col-sm-auto col-xs-auto">
                    <Link
                      className="btn btn-info float-right"
                      to="/admin/caixa/report"
                    >
                      <i className="fas fa-plus-circle" />{' '}
                      <span className="d-none d-sm-inline">
                        Gerar Relatório
                      </span>
                    </Link>
                  </div>
                  <div className="col-md-3 col-sm-auto col-xs-auto">
                    <Link
                      className="btn btn-success float-right"
                      to="/admin/caixa/cadastrar"
                    >
                      <i className="fas fa-plus-circle" />{' '}
                      <span className="d-none d-sm-inline">
                        Realizar movimentação
                      </span>
                    </Link>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-12">
                    <div className="card-header">
                      <div className="col-md-9">
                        <span />
                      </div>
                      <div className="col-md-3">
                        <div className="row align-items-center justify-content-between">
                          <div>
                            <span
                              id="inputGroup-sizing-sm"
                              className={
                                'input-text ' + this.state.exibeSaldo
                                  ? ''
                                  : 'sumirComDiv'
                              }
                            >
                              Saldo Atual: R$ {this.state.saldo}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="card-body">
                  <ReactTable
                    columns={columns}
                    data={this.state.caixa}
                    loading={this.state.loading}
                    defaultPageSize={10}
                    filterable
                    defaultFilterMethod={filterCaseInsensitive}
                    className="-striped -highlight"
                    previousText="Anterior"
                    nextText="Próximo"
                    loadingText="Carregando..."
                    noDataText="Nenhum movimento foi encontrado"
                    pageText="Pág."
                    ofText="de"
                    rowsText="linhas"
                  />
                </div>
                <div className="card-footer ">
                  <div className="stats" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle}>Movimentação</ModalHeader>
          <ModalBody>
            <dl className="row">
              <dt className="col-sm-5">Conta:</dt>
              <dd className="col-sm-7">{this.state.caixaView.nome_conta}</dd>

              <dt className="col-sm-5">Data da Movimentação:</dt>
              <dd className="col-sm-7">
                {this.state.caixaView.data_lancamento}
              </dd>

              <dt className="col-sm-5">Descrição:</dt>
              <dd className="col-sm-7">{this.state.caixaView.descricao}</dd>

              <dt className="col-sm-5">Tipo de Movimento:</dt>
              <dd className="col-sm-7">
                {this.state.caixaView.movimento === 'E' ? 'Entrada' : 'Saída'}
              </dd>

              <dt className="col-sm-5 text-truncate">Valor:</dt>
              <dd className="col-sm-7">{this.state.caixaView.valor}</dd>
            </dl>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggle}>
              Fechar
            </Button>
          </ModalFooter>
        </Modal>
      </Container>
    )
  }
}

export default Caixa
