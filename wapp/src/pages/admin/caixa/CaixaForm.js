import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import classNames from 'classnames'
import 'moment/locale/pt-br'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import Select from 'react-select'
import FormValidator from 'helpers/FormValidator'
import axiosClient from 'helpers/axiosClient'
import IntlCurrencyInput from "react-intl-currency-input"

const currencyConfig = {
  locale: "pt-BR",
  formats: {
    number: {
      BRL: {
        style: "currency",
        currency: "BRL",
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      },
    },
  },
};

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: 'red' }}>*</span>
  </label>
)
class CaixaForm extends Component {
  constructor(props) {
    super(props)

    this.validator = new FormValidator([
      {
        field: 'descricao',
        method: 'isEmpty',
        validWhen: false,
        message: 'A descrição da Conta é obrigatória'
      },
      {
        field: 'valor',
        method: 'isEmpty',
        validWhen: false,
        message: 'O valor da conta é obrigatório'
      },
      // {
      //   field: 'id_associacao',
      //   method: 'isEmpty',
      //   validWhen: false,
      //   message: 'É necessário informar a associação'
      // },
      // {
      //   field: 'id_conta',
      //   method: 'isEmpty',
      //   validWhen: false,
      //   message: 'É necessário o grupo de Conta'
      // },
      {
        field: 'movimento',
        method: 'isEmpty',
        validWhen: false,
        message: 'É necessário informar o movimento'
      }
    ])

    this.state = {
      id_caixa: '',
      descricao: '',
      valor: '',
      data_lancamento: moment(),
      movimento: 'E',
      modoEdit: false,

      associacoesOptions: [],
      associacoesOptionsLoading: false,
      contaOptions: [],
      contaOptionsLoading: false,

      validation: this.validator.valid(),
      submitted: false,
      redirect: false,
      loadingData: false
    }

    this.submitted = false
  }

  componentDidMount() {
    this.setState({
      loadingData: true
    })
    this.getOptions().then(() => {
      if (this.props.match.params && this.props.match.params.id) {
        const id = this.props.match.params.id
        this.setState({
          modoEdit: true
        })
        this.getUserById(id)
      } else {
        this.setState({
          loadingData: false
        })
      }
    })
  }


  isValidDate = data => {
    return moment(data).isValid()
  }

  getUserById = async id => {
    try {
      const response = await axiosClient.get('/painel/caixa/listar/' + id)
      if (response.data.status) {
        const caixa = response.data.obj.caixa[0]
        caixa.data_lancamento = moment(
          caixa.data_lancamento,
          'YYYY-MM-DD HH:mm:ss'
        ).isValid()
          ? moment(caixa.data_lancamento, 'YYYY-MM-DD HH:mm:ss')
          : moment()

        this.setState({
          descricao: caixa.descricao || '',
          valor: caixa.valor || '',
          data_lancamento: caixa.data_lancamento || '',
          movimento: caixa.movimento || '',
          associacao: this.state.associacoesOptions.find(
            x => x.value === caixa.id_associacao
          ),
          conta: this.state.contaOptions.find(
            x => x.value === caixa.id_conta
          ),



          loadingData: false
        })
      } else {
        Swal({
          type: 'error',
          title: 'Ocorreu um problema, por favor, tente novamente...',
          html: response.data.erro
        })
      }
    } catch (err) {
      console.error(err)
    }
  }

  handleChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }
  handleChangeCurrencyInput = (event, value, maskedValue) => {
    event.preventDefault();
    this.setState({
      valor: value
      })
  }

  handleSubmit = e => {
    e.preventDefault()

    const validation = this.validator.validate(this.state)
    this.setState({ validation, submitted: true })
    this.submitted = true

    if (validation.isValid) {
      const { descricao, valor, conta, associacao, movimento } = this.state

      let { data_lancamento } = this.state

      data_lancamento = moment(data_lancamento).format('YYYY-MM-DD HH:mm:ss')

      if (this.props.match.params && this.props.match.params.id) {
        const id = this.props.match.params.id
        axiosClient
          .post('/painel/caixa/atualizar', {
            id_caixa: id,
            descricao,
            valor,
            id_conta: conta.value,
            id_associacao: associacao.value,
            data_lancamento: data_lancamento,
            movimento
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              Swal({
                position: 'top-end',
                type: 'success',
                title: 'Usuário inserido com sucesso!',
                showConfirmButton: false,
                timer: 1500
              })
              this.setState({
                redirect: true
              })
            } else {
              Swal({
                type: 'error',
                title: 'Ocorreu um problema, por favor, tente novamente...',
                html: response.data.erro
              })
              this.setState({
                redirect: true
              })
            }
          })
          .catch(console.error)
      } else {
        axiosClient
          .post('/painel/caixa/inserir', {
            descricao,
            valor,
            id_conta: conta.value,
            id_associacao: associacao.value,
            data_lancamento,
            movimento
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              Swal({
                position: 'top-end',
                type: 'success',
                title: 'Movimentação inserido com sucesso!',
                showConfirmButton: false,
                timer: 1500
              })
              this.setState({
                redirect: true
              })
            } else {
              Swal({
                type: 'error',
                title: 'Ocorreu um problema, por favor, tente novamente...',
                html: response.data.erro
              })
              this.setState({
                submitted: false
              })
            }
          })
          .catch(console.error)
      }
    } else {
      this.setState({ submitted: false })
    }
  }

  getOptions = () => {
    return new Promise((resolve, reject) => {
      this.setState({
        associacoesOptionsLoading: true,
        contaOptionsLoading: true
      })
      Promise.all([
        axiosClient.get('/painel/comum/listarAssociacoes'),
        axiosClient.get('/painel/comum/listarContas')
      ])
        .then(([associacoesResponse, contaResponse]) => {


          const associacoesOptions = associacoesResponse.data.obj.associacoes
            .map(associacao => {
              return {
                ...associacao,
                value: associacao.id_associacao,
                label: associacao.razao_social
              }
            })
            .filter(associacao => associacao.status === 'A')


          const contaOptions = contaResponse.data.obj.conta
            .map(conta => {
              return {
                ...conta,
                value: conta.id_conta,
                label: conta.nome_conta
              }
            })
            .filter(conta => conta.status === 'A')

          this.setState({
            associacoesOptions,
            associacoesOptionsLoading: false,
            contaOptions,
            contaOptionsLoading: false
          })
          resolve()
        })
        .catch(error => {
          console.log(
            error,
            'Ocorreu um problema, por favor, tente novamente...'
          )
          reject(error)
        })
    })
  }

  toggleCheckboxChange = e => {
    this.setState({
      checkboxSenha: e.target.checked
    })
  }

  render() {
    const { loadingData } = this.state
    const validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation
    const inputClassNames = isInvalid => {
      return classNames('form-control', { 'is-invalid': isInvalid })
    }

    if (this.state.redirect) return <Redirect to="/admin/caixa/" />

    return (
      <div>
        <Link className="btn btn-secondary mb-2" to="/admin/caixa/">
          {' '}
          <i className="fas fa-arrow-left" /> Voltar a lista
        </Link>

        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Formulário de Fluxo de caixa</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>

        <fieldset disabled={loadingData}>
          <form onSubmit={this.handleSubmit}>
            <div className="form-row">
              <div className="col-md-10">
                <div className="card">
                  <div
                    className={classNames('card-body', {
                      'is-loading is-loading-lg': loadingData
                    })}
                  >
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <LabelRiquered>Grupo de Conta</LabelRiquered>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              isMulti={false}
                              value={this.state.conta}
                              isLoading={this.state.contaOptionsLoading}
                              isSearchable={true}
                              name="conta"
                              placeholder="Selecione..."
                              options={this.state.contaOptions}
                              onChange={selectedOption =>
                                this.setState({ conta: selectedOption })
                              }
                            />

                        </div>
                        <div className="form-group col-md-6">
                            <LabelRiquered>Associação</LabelRiquered>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              isMulti={false}
                              value={this.state.associacao}
                              isLoading={this.state.associacoesOptionsLoading}
                              isSearchable={true}
                              name="associacao"
                              placeholder="Selecione..."
                              options={this.state.associacoesOptions}
                              onChange={selectedOption =>
                                this.setState({ associacao: selectedOption })
                              }
                            />
                        </div>
                    </div>

                    <div className="form-group">
                      <div className="form-group">
                        <LabelRiquered>Descrição Lançamento</LabelRiquered>
                        <input
                          type="text"
                          name="descricao"
                          placeholder="Descrição lançamento"
                          className={inputClassNames(
                            validation.descricao.isInvalid
                          )}
                          value={this.state.descricao}
                          onChange={this.handleChange}
                          autoComplete={'off'}
                        />
                      </div>
                      <div className="invalid-feedback">
                        {validation.descricao.message}
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <div className="form-group">
                          <LabelRiquered>Data Lançamento</LabelRiquered>
                          <br></br>
                          <DatePicker
                            selected={this.state.data_lancamento}
                            className="form-control"
                            dateFormat="DD/MM/YYYY"
                            onChange={data_lancamento => {
                              this.setState({ data_lancamento })
                            }}
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                          />
                        </div>
                        <div className="invalid-feedback">
                          {/* {validation.data_lancamento.message} */}
                        </div>
                      </div>

                      <div className="form-group col-md-4">
                        <div className="form-group">
                          <LabelRiquered>Tipo Movimento</LabelRiquered>
                          <select
                            name="movimento"
                            className={inputClassNames()}
                            onChange={this.handleChange}
                            value={this.state.movimento}
                            autoComplete={'off'}
                          >
                            <option value="E">Entrada</option>
                            <option value="S">Saida</option>
                          </select>
                        </div>
                      </div>

                      <div className="form-group col-md-4">
                        <div className="form-group">
                          <LabelRiquered>Valor</LabelRiquered>
                          <IntlCurrencyInput
                            currency="BRL" config={currencyConfig}
                            name="valor"
                            placeholder="Valor da movimentação"
                            className={inputClassNames(
                              validation.valor.isInvalid
                            )}
                            value={this.state.valor}
                            onChange={this.handleChangeCurrencyInput}
                            autoComplete={'off'}
                          />
                        </div>
                        <div className="invalid-feedback">
                          {validation.valor.message}
                        </div>
                      </div>
                      <button
                        type="submit"
                        className="btn btn-success"
                        onClick={this.handleSubmit}
                      >
                        Salvar
                      </button>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </fieldset>
      </div>
    )
  }
}

export default CaixaForm
