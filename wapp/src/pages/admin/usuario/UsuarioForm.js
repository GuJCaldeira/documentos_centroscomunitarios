import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import classNames from 'classnames'
import cepPromise from 'cep-promise'
import DatePicker from 'react-datepicker'
import 'moment/locale/pt-br'
import moment from 'moment'
import MaskedInput from 'react-maskedinput'

import Select, { Creatable } from 'react-select'

// import Avatar from 'components/Avatar'
import FormValidator from 'helpers/FormValidator'

// import fotoUsuario from 'images/default-avatar.png'
import axiosClient from 'helpers/axiosClient'
import isEmpty from 'helpers/isEmpty'

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: 'red' }}>*</span>
  </label>
)

class UsuarioForm extends Component {
  isSelectedAssociacao = (_data, state) => isEmpty(state.associacao)

  isSelectedCargo = (_data, state) => isEmpty(state.cargo)

  isSelectedGrupo = (_data, state) => isEmpty(state.grupo)

  chosenSex = (_sex, state) => {
    const sexo = state.sexo.toUpperCase()
    return sexo === 'M' || sexo === 'F' || sexo === 'O'
  }

  constructor(props) {
    super(props)
    const formValidations = [
      {
        field: 'cpf',
        method: 'isEmpty',
        validWhen: false,
        message: 'O CPF é obrigatório.'
      },
      {
        field: 'nome',
        method: 'isEmpty',
        validWhen: false,
        message: 'O nome é obrigatório.'
      },
      {
        field: 'email',
        method: 'isEmpty',
        validWhen: false,
        message: 'O email é obrigatório.'
      },
      {
        field: 'email',
        method: 'isEmail',
        validWhen: true,
        message: 'O email é inválido.'
      },
      {
        field: 'sexo',
        method: this.chosenSex,
        validWhen: true,
        message: 'O sexo é obrigatório'
      },
      {
        field: 'telefone1',
        method: 'isEmpty',
        validWhen: false,
        message: 'O telefone é obrigatório'
      },
      {
        field: 'cep',
        method: 'isEmpty',
        validWhen: false,
        message: 'O CEP é obrigatório'
      },
      {
        field: 'endereco',
        method: 'isEmpty',
        validWhen: false,
        message: 'O endereço é obrigatório'
      },
      {
        field: 'numero',
        method: 'isEmpty',
        validWhen: false,
        message: 'O número é obrigatório'
      },
      {
        field: 'bairro',
        method: 'isEmpty',
        validWhen: false,
        message: 'O bairro é obrigatório'
      },
      {
        field: 'cidade',
        method: 'isEmpty',
        validWhen: false,
        message: 'A cidade é obrigatória'
      },
      {
        field: 'associacao',
        method: this.isSelectedAssociacao,
        validWhen: false,
        message: 'A associação é obrigatório'
      },
      {
        field: 'cargo',
        method: this.isSelectedCargo,
        validWhen: false,
        message: 'O cargo é obrigatório'
      },
      {
        field: 'grupo',
        method: this.isSelectedCargo,
        validWhen: false,
        message: 'O grupo é obrigatório'
      },
      {
        field: 'estado',
        method: 'isEmpty',
        validWhen: false,
        message: 'O estado é obrigatório'
      }
    ]
    if (!props.match.params.id) {
      formValidations.push({
        field: 'senha',
        method: 'isLength',
        args: [{ min: 6 }],
        validWhen: true,
        message: 'A senha deve conter no mínimo 6 digitos'
      })
    }

    this.validator = new FormValidator(formValidations)

    this.state = {
      cpf: '',
      senha: '',
      nome: '',
      email: '',
      dataNascimento: moment(),
      sexo: '',
      telefone1: '',
      telefone2: '',
      cep: '',
      endereco: '',
      numero: '',
      bairro: '',
      cidade: '',
      estado: '',
      associacao: {},
      cargo: {},
      grupo: {},
      status: 'A',
      modoEdit: false,
      checkboxSenha: true,
      associacoesOptions: [],
      associacoesOptionsLoading: false,
      cargosOptions: [],
      cargosOptionsLoading: false,
      gruposOptions: [],
      gruposOptionsLoading: false,
      validation: this.validator.valid(),
      submitted: false,
      redirect: false,
      loadingData: false
    }

    this.submitted = false
  }

  componentDidMount() {
    this.setState({
      loadingData: true
    })
    this.getOptions().then(() => {
      if (this.props.match.params && this.props.match.params.id) {
        const id = this.props.match.params.id
        this.setState({
          modoEdit: true,
          checkboxSenha: false
        })
        this.getUserById(id)
      } else {
        this.setState({
          loadingData: false
        })
      }
    })
  }

  getUserById = async id => {
    try {
      const response = await axiosClient.get('/painel/usuario/listar/' + id)
      if (response.data.status) {
        let usuario = response.data.obj.usuarios[0]
        usuario.data_nasc = moment(
          usuario.data_nasc,
          'YYYY-MM-DD HH:mm:ss'
        ).isValid()
          ? moment(usuario.data_nasc, 'YYYY-MM-DD HH:mm:ss')
          : moment()

        this.setState({
          cpf: usuario.cpf || '',
          nome: usuario.nome || '',
          email: usuario.email || '',
          dataNascimento: usuario.data_nasc,
          sexo: usuario.sexo || '',
          telefone1: usuario.telefone1 || '',
          telefone2: usuario.telefone2 || '',
          cep: usuario.cep || '',
          endereco: usuario.endereco || '',
          numero: usuario.numero || '',
          bairro: usuario.bairro || '',
          cidade: usuario.cidade || '',
          estado: usuario.estado || '',
          associacao: this.state.associacoesOptions.find(
            x => x.value === usuario.id_associacao
          ),
          cargo: this.state.cargosOptions.find(
            x => x.value === usuario.id_cargo
          ),
          grupo: this.state.gruposOptions.find(
            x => x.value === usuario.id_grupo
          ),
          status: usuario.status || '',

          loadingData: false
        })
      } else {
        Swal({
          type: 'error',
          title: 'Ocorreu um problema, por favor, tente novamente...',
          html: response.data.erro
        })
      }
    } catch (err) {
      console.error(err)
    }
  }

  handleChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  handleSubmit = e => {
    e.preventDefault()

    const validation = this.validator.validate(this.state)
    this.setState({ validation, submitted: true })
    this.submitted = true

    if (validation.isValid) {
      let {
        cpf,
        senha,
        nome,
        email,
        dataNascimento,
        sexo,
        telefone1,
        telefone2,
        cep,
        endereco,
        numero,
        bairro,
        cidade,
        estado,
        associacao,
        cargo,
        grupo,
        status
      } = this.state
      dataNascimento = moment(dataNascimento).format('YYYY-MM-DD HH:mm:ss')
      cpf = cpf.replace(/\D/g, '')

      if (this.props.match.params && this.props.match.params.id) {
        const id = this.props.match.params.id
        axiosClient
          .post('/painel/usuario/atualizar', {
            id_usuario: id,
            cpf,
            senha,
            nome,
            data_nasc: dataNascimento,
            sexo,
            email,
            telefone1,
            telefone2,
            id_associacao: associacao.value,
            id_cargo: cargo.value,
            id_grupo: grupo.value,
            cep,
            endereco,
            numero,
            bairro,
            cidade,
            estado,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Ocorreu um problema, por favor, tente novamente...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      } else {
        axiosClient
          .post('/painel/usuario/inserir', {
            cpf,
            senha,
            nome,
            data_nasc: dataNascimento,
            sexo,
            email,
            telefone1,
            telefone2,
            id_associacao: associacao.value,
            id_cargo: cargo.value,
            cep,
            endereco,
            numero,
            bairro,
            cidade,
            estado,
            status,
            id_grupo: grupo.value
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              Swal({
                position: 'top-end',
                type: 'success',
                title: 'Usuário inserido com sucesso!',
                showConfirmButton: false,
                timer: 1500
              })
              this.setState({
                redirect: true
              })
            } else {
              Swal({
                type: 'error',
                title: 'Ocorreu um problema, por favor, tente novamente...',
                html: response.data.erro
              })
              this.setState({
                submitted: false
              })
            }
          })
          .catch(console.error)
      }
    } else {
      this.setState({ submitted: false })
    }
  }

  handleChangeCep = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })

    this.pesquisaCep(value)
  }

  pesquisaCep = cep => {
    if (cep.length >= 8) {
      cepPromise(cep)
        .then(response => {
          this.setState({
            estado: response.state,
            cidade: response.city,
            bairro: response.neighborhood,
            endereco: response.street
          })
        })
        .catch(console.error)
    }
  }

  insertOption = data => {
    axiosClient
      .post('/painel/cargo/inserir', {
        nome_cargo: data.value,
        detalhes: 'porque é obrigatório?'
      })
      .then(response => {
        let cargo = {
          value: response.data.id_cargo,
          label: response.data.obj.nome_cargo
        }
        this.setState({
          cargo,
          cargosOptions: [...this.state.cargosOptions, cargo]
        })
      })
  }

  getOptions = () => {
    return new Promise((resolve, reject) => {
      this.setState({
        cargosOptionsLoading: true,
        associacoesOptionsLoading: true,
        gruposOptionsLoading: true
      })
      Promise.all([
        axiosClient.get('/painel/comum/listarCargos'),
        axiosClient.get('/painel/comum/listarAssociacoes'),
        axiosClient.get('/painel/comum/listarGrupos')
      ])
        .then(([cargosResponse, associacoesResponse, gruposResponse]) => {
          const associacoesOptions = associacoesResponse.data.obj.associacoes
            .map(associacao => {
              return {
                ...associacao,
                value: associacao.id_associacao,
                label: associacao.razao_social
              }
            })
            .filter(associacao => associacao.status === 'A')

          const cargosOptions = cargosResponse.data.obj
            .map(cargo => {
              return {
                ...cargo,
                value: cargo.id_cargo,
                label: cargo.nome_cargo
              }
            })
            .filter(cargo => cargo.status === 'A')

          const gruposOptions = gruposResponse.data.obj.grupos
            .map(grupo => {
              return {
                ...grupo,
                value: grupo.id_grupo,
                label: grupo.nome_grupo
              }
            })
            .filter(grupo => grupo.status === 'A')

          this.setState({
            associacoesOptions,
            cargosOptions,
            gruposOptions,
            cargosOptionsLoading: false,
            associacoesOptionsLoading: false,
            gruposOptionsLoading: false
          })
          resolve()
        })
        .catch(error => {
          console.log(error, "ups.. couldn't get options")
          reject(error)
        })
    })
  }

  toggleCheckboxChange = e => {
    this.setState({
      checkboxSenha: e.target.checked
    })
  }

  render() {
    const { loadingData } = this.state
    const validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation
    const inputClassNames = isInvalid => {
      return classNames('form-control', { 'is-invalid': isInvalid })
    }

    if (this.state.redirect) return <Redirect to="/admin/usuarios" />

    return (
      <div>
        <Link className="btn btn-secondary mb-2" to="/admin/usuarios">
          {' '}
          <i className="fas fa-arrow-left" /> Voltar a lista
        </Link>
        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Formulário do Usuário</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>
        <fieldset disabled={loadingData}>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-md-10">
                <div className="card">
                  <div
                    className={classNames('card-body', {
                      'is-loading is-loading-lg': loadingData
                    })}
                  >
                    <h3>Informações de Acesso</h3>
                    <div className="form-group col-md-12">
                      <LabelRiquered>CPF</LabelRiquered>
                      <MaskedInput
                        type="text"
                        name="cpf"
                        placeholder="000.000.000-00"
                        mask="111.111.111-11"
                        className={inputClassNames(validation.cpf.isInvalid)}
                        value={this.state.cpf}
                        onChange={this.handleChange}
                        readOnly={this.state.modoEdit}
                        autoComplete={'off'}
                      />
                      <div className="invalid-feedback">
                        {validation.cpf.message}
                      </div>
                    </div>
                    <div className="form-group col-md-12">
                      <LabelRiquered>Senha</LabelRiquered>
                      <input
                        type="password"
                        name="senha"
                        placeholder="••••••"
                        className={inputClassNames(validation.nome.isInvalid)}
                        value={this.state.senha}
                        onChange={this.handleChange}
                        validation={validation.senha}
                        autoComplete={'off'}
                        disabled={!this.state.checkboxSenha}
                      />
                      <div className="invalid-feedback">
                        {validation.senha && validation.senha.message}
                      </div>
                    </div>

                    <div className="form-group col-md-12">
                      <div className="form-check">
                        <input
                          type="checkbox"
                          className="form-check-input"
                          id="materialChecked2"
                          onChange={this.toggleCheckboxChange}
                          checked={this.state.checkboxSenha}
                        />
                        <label
                          className="form-check-label"
                          htmlFor="materialChecked2"
                        >
                          Selecione Para Alterar Senha
                        </label>
                      </div>
                    </div>

                    <hr />

                    <h3>Informações Pessoais</h3>

                    {/* <div className="row justify-content-center">
                      <Avatar src={fotoUsuario} />
                    </div> */}

                    <div className="form-group">
                      <LabelRiquered>Nome</LabelRiquered>
                      <input
                        type="text"
                        name="nome"
                        placeholder="Informe o nome do usuário"
                        className={inputClassNames(validation.nome.isInvalid)}
                        value={this.state.nome}
                        onChange={this.handleChange}
                        autoComplete={'off'}
                      />
                      <div className="invalid-feedback">
                        {validation.nome.message}
                      </div>
                    </div>

                    <div className="form-group">
                      <LabelRiquered>Email</LabelRiquered>
                      <input
                        type="text"
                        name="email"
                        placeholder="Informe o email"
                        className={inputClassNames(validation.email.isInvalid)}
                        value={this.state.email}
                        onChange={this.handleChange}
                        autoComplete={'off'}
                      />
                      <div className="invalid-feedback">
                        {validation.email.message}
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <label>Data de Nascimento</label>
                        <br />
                        <DatePicker
                          selected={this.state.dataNascimento}
                          className="form-control"
                          maxDate={moment()}
                          onChange={dataNascimento => {
                            this.setState({ dataNascimento })
                          }}
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          placeholderText="XX/XX/XXXX"
                          autoComplete={'off'}
                        />
                        <div className="invalid-feedback">
                          {/* {validation.dataNascimento.message} */}
                        </div>
                      </div>
                      <div className="form-group col-md-6">
                        <LabelRiquered>Sexo</LabelRiquered>
                        <br />
                        <div className="form-check form-check-inline">
                          <input
                            className={classNames('form-check-input', {
                              'is-invalid': validation.sexo.isInvalid
                            })}
                            type="radio"
                            name="sexo"
                            id="sexoRadio1"
                            value={'M'}
                            onChange={this.handleChange}
                            autoComplete={'off'}
                          />
                          <label
                            className="form-check-label pl-0"
                            htmlFor="sexoRadio1"
                          >
                            {' '}
                            Masculino{' '}
                          </label>
                        </div>
                        <div className="form-check form-check-inline">
                          <input
                            className={classNames('form-check-input', {
                              'is-invalid': validation.sexo.isInvalid
                            })}
                            type="radio"
                            name="sexo"
                            id="sexoRadio2"
                            value={'F'}
                            onChange={this.handleChange}
                            autoComplete={'off'}
                          />
                          <label
                            className="form-check-label pl-0"
                            htmlFor="sexoRadio2"
                          >
                            {' '}
                            Feminino{' '}
                          </label>
                        </div>
                        <div className="form-check form-check-inline">
                          <input
                            className={classNames('form-check-input', {
                              'is-invalid': validation.sexo.isInvalid
                            })}
                            type="radio"
                            name="sexo"
                            id="sexoRadio3"
                            value={'O'}
                            onChange={this.handleChange}
                            autoComplete={'off'}
                          />
                          <label
                            className="form-check-label pl-0"
                            htmlFor="sexoRadio3"
                          >
                            {' '}
                            Outro{' '}
                          </label>
                        </div>

                        <div className="invalid-feedback">
                          {validation.sexo.message}
                        </div>
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="col-md-5">
                        <div className="form-group">
                          <LabelRiquered>Telefone</LabelRiquered>
                          <MaskedInput
                            type="text"
                            name="telefone1"
                            placeholder="(00) 0000-0000"
                            mask="(11) 1111-1111"
                            className={inputClassNames(
                              validation.telefone1.isInvalid
                            )}
                            value={this.state.telefone1}
                            onChange={this.handleChange}
                            autoComplete={'off'}
                          />
                          <div className="invalid-feedback">
                            {validation.telefone1.message}
                          </div>
                        </div>
                      </div>

                      <div className="col-md-5">
                        <div className="form-group">
                          <label>Telefone 2</label>
                          <MaskedInput
                            type="text"
                            name="telefone2"
                            placeholder="(00) 0 0000-0000"
                            mask="(11) 1 1111-1111"
                            className={inputClassNames()}
                            value={this.state.telefone2}
                            onChange={this.handleChange}
                            autoComplete={'off'}
                          />
                        </div>
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <LabelRiquered>Cargo</LabelRiquered>
                        <Creatable
                          name="cargo"
                          value={this.state.cargo}
                          isMulti={false}
                          options={this.state.cargosOptions}
                          isLoading={this.state.cargosOptionsLoading}
                          placeholder="Selecione..."
                          onChange={selectedOption =>
                            this.setState({ cargo: selectedOption })
                          }
                          onNewOptionClick={this.insertOption}
                          promptTextCreator={label => `Criar ${label} `}
                        />
                        <div className="text-danger">
                          {validation.cargo.message}
                        </div>
                      </div>

                      <div className="form-group col-md-4">
                        <LabelRiquered>Associação</LabelRiquered>
                        <Select
                          className="basic-single"
                          classNamePrefix="select"
                          isMulti={false}
                          value={this.state.associacao}
                          isLoading={this.state.associacoesOptionsLoading}
                          isSearchable={true}
                          name="associacao"
                          placeholder="Selecione..."
                          options={this.state.associacoesOptions}
                          onChange={selectedOption =>
                            this.setState({ associacao: selectedOption })
                          }
                        />
                        <div className="text-danger">
                          {validation.associacao.message}
                        </div>
                      </div>

                      <div className="form-group col-md-4">
                        <LabelRiquered>Grupo</LabelRiquered>
                        <Select
                          className="basic-single"
                          classNamePrefix="select"
                          isMulti={false}
                          value={this.state.grupo}
                          isLoading={this.state.gruposOptionsLoading}
                          isSearchable={true}
                          name="grupo"
                          placeholder="Selecione..."
                          options={this.state.gruposOptions}
                          onChange={selectedOption =>
                            this.setState({ grupo: selectedOption })
                          }
                        />
                        <div className="text-danger">
                          {validation.associacao.message}
                        </div>
                      </div>
                    </div>

                    <hr />

                    <h3>Endereço</h3>
                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <LabelRiquered htmlFor="cep">CEP</LabelRiquered>
                        <MaskedInput
                          type="text"
                          name="cep"
                          mask="11111-111"
                          value={this.state.cep}
                          className={inputClassNames(validation.cep.isInvalid)}
                          placeholder="Pesquisar o CEP"
                          onChange={this.handleChangeCep}
                          autoComplete={'off'}
                        />
                        <div className="invalid-feedback">
                          {validation.cep.message}
                        </div>
                      </div>
                      <div className="form-group col-md-9">
                        <LabelRiquered htmlFor="endereco">
                          Endereço
                        </LabelRiquered>
                        <input
                          type="text"
                          name="endereco"
                          placeholder="Informe o nome da rua, avenida, etc."
                          value={this.state.endereco}
                          className={inputClassNames(
                            validation.endereco.isInvalid
                          )}
                          id="endereco"
                          onChange={this.handleChange}
                          autoComplete={'off'}
                        />
                        <div className="invalid-feedback">
                          {validation.endereco.message}
                        </div>
                      </div>
                      <div className="form-group col-md-3">
                        <LabelRiquered htmlFor="numero">Número</LabelRiquered>
                        <input
                          type="text"
                          name="numero"
                          placeholder="000"
                          value={this.state.numero}
                          className={inputClassNames(
                            validation.numero.isInvalid
                          )}
                          id="numero"
                          onChange={this.handleChange}
                          autoComplete={'off'}
                        />
                        <div className="invalid-feedback">
                          {validation.numero.message}
                        </div>
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="form-group col-md-7">
                        <LabelRiquered htmlFor="bairro">Bairro</LabelRiquered>
                        <input
                          type="text"
                          name="bairro"
                          placeholder="Informe o bairro"
                          value={this.state.bairro}
                          className={inputClassNames(
                            validation.bairro.isInvalid
                          )}
                          onChange={this.handleChange}
                          autoComplete={'off'}
                        />
                        <div className="invalid-feedback">
                          {validation.bairro.message}
                        </div>
                      </div>
                      <div className="form-group col-md-7">
                        <LabelRiquered htmlFor="cidade">Cidade</LabelRiquered>
                        <input
                          type="text"
                          name="cidade"
                          placeholder="Informe a cidade"
                          value={this.state.cidade}
                          className={inputClassNames(
                            validation.cidade.isInvalid
                          )}
                          onChange={this.handleChange}
                          autoComplete={'off'}
                        />
                        <div className="invalid-feedback">
                          {validation.cidade.message}
                        </div>
                      </div>
                      <div className="form-group col-md-5">
                        <LabelRiquered htmlFor="inputState">
                          Estado
                        </LabelRiquered>
                        <input
                          id="inputState"
                          type="text"
                          name="estado"
                          placeholder="Sigla da UF"
                          value={this.state.estado}
                          className={inputClassNames(
                            validation.estado.isInvalid
                          )}
                          onChange={this.handleChange}
                          autoComplete={'off'}
                        />
                        <div className="invalid-feedback">
                          {validation.estado.message}
                        </div>
                      </div>
                    </div>
                    <hr />
                    <div className="form-row">
                      <div className="form-group col-md-3">
                        <label>Status</label>
                        <select
                          name="status"
                          className={inputClassNames()}
                          onChange={this.handleChange}
                          value={this.state.status}
                          autoComplete={'off'}
                        >
                          <option value="A">Ativo</option>
                          <option value="I">Inativo</option>
                        </select>
                      </div>
                    </div>
                    <button
                      type="submit"
                      className="btn btn-success"
                      disabled={this.state.submitted}
                    >
                      Salvar
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </fieldset>
      </div>
    )
  }
}

export default UsuarioForm
