import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import classNames from 'classnames'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import Select from 'react-select'
import MaskedInput from 'react-maskedinput'

import FormValidator from 'helpers/FormValidator'
import axiosClient from 'helpers/axiosClient'
import isEmpty from 'helpers/isEmpty'

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: 'red' }}>*</span>
  </label>
)

class EventosForm extends Component {
  isSelectedResponsavel = (_data, state) => isEmpty(state.responsavel)
  constructor(props) {
    super(props)

    this.validator = new FormValidator([
      {
        field: 'nome_evento',
        method: 'isEmpty',
        validWhen: false,
        message: 'O nome do evento é obrigatório.'
      },
      {
        field: 'descricao',
        method: 'isEmpty',
        validWhen: false,
        message: 'O campo descrição é obrigatório.'
      },
      {
        field: 'tipo_evento',
        method: 'isEmpty',
        validWhen: false,
        message: 'O tipo do evento é obrigatório.'
      },

      {
        field: 'dados_evento',
        method: 'isEmpty',
        validWhen: false,
        message: 'O campo dados é obrigatório.'
      },

      {
        field: 'valor',
        method: 'isFloat',
        validWhen: false,
        message: 'Informe um valor'
      }
    ])

    this.state = {
      id_evento: '',
      nome_evento: '',
      descricao: '',
      tipo_evento: 'PV',
      dados_evento: '',
      valor: '',
      data_inicio: moment(),
      data_final: moment(),
      status: 'A',
      centro_comunitario: '',
      modoEdit: false,

      responsaveisOptions: [],
      responsaveisOptionsLoading: false,
      centro_comunitarioOptions: [],
      centro_comunitarioOptionsLoading: false,

      validation: this.validator.valid(),
      submitted: false,
      redirect: false,
      loadingData: false
    }

    this.submitted = false
  }

  componentDidMount() {
    this.setState({
      loadingData: true
    })
    this.getOptions().then(() => {
      if (this.props.match.params && this.props.match.params.id) {
        const id = this.props.match.params.id
        this.setState({
          modoEdit: true
        })
        this.getEventoById(id)
      } else {
        this.setState({
          loadingData: false
        })
      }
    })
  }

  isValidDate = data => {
    return moment(data).isValid()
  }

  getEventoById = async id => {
    try {
      const response = await axiosClient.get('/painel/evento/listar/' + id)
      if (response.data && response.data.status) {
        const evento = response.data.obj.evento[0]
        evento.data_inicio = moment(
          evento.data_inicio,
          'YYYY-MM-DD HH:mm:ss'
        ).isValid()
          ? moment(evento.data_inicio, 'YYYY-MM-DD HH:mm:ss')
          : moment()

        evento.data_final = moment(
          evento.data_final,
          'YYYY-MM-DD HH:mm:ss'
        ).isValid()
          ? moment(evento.data_final, 'YYYY-MM-DD HH:mm:ss')
          : moment()
        this.setState({
          id_evento: evento.id_evento || '',
          nome_evento: evento.nome_evento || '',
          dados_evento: evento.dados_evento || '',
          data_criacao: evento.data_criacao || '',
          data_alteracao: evento.data_alteracao || '',
          detalhes: evento.detalhes || '',
          descricao: evento.descricao || '',
          data_inicio: evento.data_inicio || '',
          data_final: evento.data_final || '',
          tipo_evento: evento.tipo_evento || '',
          valor: evento.valor || '',
          responsavel: this.state.responsaveisOptions.find(
            x => x.value === evento.id_responsavel
          ),
          centro_comunitario: this.state.centro_comunitarioOptions.find(
            x => x.value === evento.id_centro_comunitario
          ),
          status: evento.status || '',
          loadingData: false
        })
      } else {
        Swal({
          type: 'error',
          title: 'Ocorreu um problema, por favor, tente novamente...',
          html: response.data.erro
        })
      }
    } catch (err) {
      console.error(err)
      Swal({
        type: 'error',
        title: 'Ocorreu um problema, por favor, tente novamente...',
        html: err.message
      })
    }
  }

  handleChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  handleSubmit = e => {
    e.preventDefault()
    const validation = this.validator.validate(this.state)
    // this.setState({ validation, submitted: true })

    this.submitted = true
    if (validation.isValid) {
      const {
        nome_evento,
        descricao,
        tipo_evento,
        dados_evento,
        valor,
        responsavel,
        centro_comunitario,
        status
      } = this.state

      let { data_inicio, data_final } = this.state

      data_inicio = moment(data_inicio).format('YYYY-MM-DD HH:mm:ss')
      data_final = moment(data_final).format('YYYY-MM-DD HH:mm:ss')

      if (this.props.match.params && this.props.match.params.id) {
        const id_evento = this.props.match.params.id

        axiosClient
          .post('/painel/evento/atualizar', {
            id_evento,
            data_inicio,
            data_final,
            nome_evento,
            descricao,
            tipo_evento,
            dados_evento,
            valor,
            id_centro_comunitario: centro_comunitario.value,
            id_responsavel: responsavel.value,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Ocorreu um problema, por favor, tente novamente...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      } else {
        axiosClient
          .post('/painel/evento/inserir', {
            nome_evento,
            descricao,
            data_inicio,
            data_final,
            tipo_evento,
            dados_evento,
            valor,
            id_centro_comunitario: centro_comunitario.value,
            id_responsavel: responsavel.value,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              Swal({
                position: 'top-end',
                type: 'success',
                title: 'Evento inserido com sucesso!',
                showConfirmButton: false,
                timer: 1500
              })
              this.setState({
                redirect: true
              })
            } else {
              Swal({
                type: 'error',
                title: 'Ocorreu um problema, por favor, tente novamente...',
                html: response.data.erro
              })
              this.setState({
                submitted: false
              })
            }
          })
          .catch(err => {
            console.log(err)
            Swal({
              type: 'error',
              title: 'Ocorreu um problema, por favor, tente novamente...',
              html: err.message
            })
          })
      }
    } else {
      this.setState({ submitted: false })
    }
  }

  getOptions = () => {
    return new Promise((resolve, reject) => {
      this.setState({
        responsaveisOptionsLoading: true,
        centro_comunitarioOptionsLoading: true
      })
      Promise.all([
        axiosClient.get('/painel/responsavel/listar'),
        axiosClient.get('/painel/centro_comunitario/listar')
      ])
        .then(([responsaveisResponse, centro_comunitarioResponse]) => {
          const centro_comunitarioOptions = centro_comunitarioResponse.data.obj.centro_comunitario
            .map(centro_comunitario => {
              return {
                ...centro_comunitario,
                value: centro_comunitario.id_centro_comunitario,
                label: centro_comunitario.nome_centro
              }
            })
            .filter(centro_comunitario => centro_comunitario.status === 'A')
          const responsaveisOptions = responsaveisResponse.data.obj.responsavel
            .map(responsavel => {
              return {
                ...responsavel,
                value: responsavel.id_responsavel,
                label: responsavel.nome
              }
            })
            .filter(responsavel => responsavel.status === 'A')

          this.setState(
            {
              responsaveisOptions,
              centro_comunitarioOptions,
              responsaveisOptionsLoading: false,
              centro_comunitarioOptionsLoading: false
            },
            resolve()
          )
        })
        .catch(error => {
          console.log(error, "ups... couldn't get options")
          reject(error)
        })
    })
  }

  render() {
    const { loadingData } = this.state
    const validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation
    const inputClassNames = isInvalid => {
      return classNames('form-control', { 'is-invalid': isInvalid })
    }

    if (this.state.redirect) return <Redirect to="/admin/eventos" />

    return (
      <div>
        <Link className="btn btn-secondary mb2" to="/admin/eventos">
          <i className="fas fa-arrow-left mr-2" />
          Voltar a lista
        </Link>
        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Formulário de Eventos</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-10">
              <div className="card">
                <div
                  className={classNames('card-body', {
                    'is-loading is-loading-lg': loadingData
                  })}
                >
                  <h3>Informações Cadastrais do Evento</h3>

                  <div className="form-row">
                    <LabelRiquered>Nome do Evento</LabelRiquered>
                    <input
                      type="text"
                      name="nome_evento"
                      placeholder="Informe um nome para o evento"
                      className={inputClassNames(
                        validation.nome_evento.isInvalid
                      )}
                      value={this.state.nome_evento}
                      onChange={this.handleChange}
                    />
                    <div className="invalid-feedback">
                      {validation.nome_evento.message}
                    </div>
                  </div>

                  <div className="form-row">
                    <LabelRiquered>Descrição do Evento</LabelRiquered>
                    <textarea
                      name="descricao"
                      className={inputClassNames(
                        validation.descricao.isInvalid
                      )}
                      value={this.state.descricao}
                      onChange={this.handleChange}
                      placeholder="Informe uma descrição para o evento"
                      rows="3"
                    />
                    <div className="invalid-feedback">
                      {validation.descricao.message}
                    </div>
                  </div>

                  <div className="form-row form-group">
                    <div className="col-md-6">
                      <div className="form-group">
                        <LabelRiquered>Data Inicial do Evento</LabelRiquered>
                        <br />

                        <DatePicker
                          selected={this.state.data_inicio}
                          className="form-control"
                          showTimeSelect
                          dateFormat="DD/MM/YYYY HH:mm"
                          timeFormat="HH:mm"
                          minDate={moment()}
                          onChange={data_inicio => {
                            this.setState({ data_inicio })
                          }}
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                        />

                        <div className="invalid-feedback">
                          {/* {validation.dataEvento.message} */}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <LabelRiquered>Data de término do Evento</LabelRiquered>
                        <br />

                        <DatePicker
                          selected={this.state.data_final}
                          className="form-control"
                          showTimeSelect
                          dateFormat="DD/MM/YYYY HH:mm"
                          timeFormat="HH:mm"
                          minDate={moment()}
                          onChange={data_final => {
                            this.setState({ data_final })
                          }}
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                        />

                        <div className="invalid-feedback">
                          {/* {validation.dataEvento.message} */}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="form-row">
                    <div className="form-group col-md-6">
                      <LabelRiquered>Responsável</LabelRiquered>
                      <Select
                        name="responsavel"
                        value={this.state.responsavel}
                        isMulti={false}
                        options={this.state.responsaveisOptions}
                        isLoading={this.state.responsaveisOptionsLoading}
                        placeholder="Selecione..."
                        onChange={selectedOption =>
                          this.setState({ responsavel: selectedOption })
                        }
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <LabelRiquered>Centro Comunitário</LabelRiquered>
                      <Select
                        classNamePrefix="select"
                        name="centro_comunitario"
                        value={this.state.centro_comunitario}
                        isMulti={false}
                        options={this.state.centro_comunitarioOptions}
                        isLoading={this.state.centro_comunitarioOptionsLoading}
                        placeholder="Selecione..."
                        isSearchable={true}
                        onChange={selectedOption =>
                          this.setState({ centro_comunitario: selectedOption })
                        }
                      />
                    </div>
                  </div>

                  <div className="form-row">
                    <div className="col-md-4">
                      <div className="form-group">
                        <LabelRiquered>Tipo Evento</LabelRiquered>
                        <select
                          name="tipo_evento"
                          className={inputClassNames()}
                          onChange={this.handleChange}
                          value={this.state.tipo_evento}
                          autoComplete={'off'}
                        >
                          <option value="PU">Público</option>
                          <option value="PV">Privado</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="form-group">
                        <LabelRiquered>Status</LabelRiquered>
                        <select
                          name="status"
                          className={inputClassNames()}
                          onChange={this.handleChange}
                          value={this.state.status}
                          autoComplete={'off'}
                        >
                          <option value="A">Ativo</option>
                          <option value="I">Inativo</option>
                        </select>
                      </div>
                    </div>

                    <div className="col-md-4">
                      <div className="form-group">
                        <label>Valor do Evento (R$)</label>
                        <MaskedInput
                          type="text"
                          name="valor"
                          placeholder="R$ 00,00"
                          mask="111,00"
                          className={inputClassNames(
                            validation.valor.isInvalid
                          )}
                          value={this.state.valor}
                          onChange={this.handleChange}
                          readOnly={this.state.modoEdit}
                          autoComplete={'off'}
                          size="10"
                        />
                      </div>
                      <div className="invalid-feedback">
                        {validation.valor.message}
                      </div>
                    </div>
                  </div>

                  <div className="form-row">
                    <LabelRiquered>Dados do Evento</LabelRiquered>

                    <textarea
                      name="dados_evento"
                      className={inputClassNames(
                        validation.dados_evento.isInvalid
                      )}
                      value={this.state.dados_evento}
                      onChange={this.handleChange}
                      placeholder="Informe uma descrição para o evento"
                      rows="3"
                    />

                    <div className="invalid-feedback">
                      {validation.dados_evento.message}
                    </div>
                  </div>

                  <hr />

                  <button
                    type="submit"
                    className="btn btn-success"
                    disabled={this.state.submitted}
                  >
                    Salvar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default EventosForm
