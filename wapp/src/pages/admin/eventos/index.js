import React, { Component } from 'react'
import BigCalendar from 'react-big-calendar'
import moment from 'moment'
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import { Container } from './styles'
import axiosClient from 'helpers/axiosClient'

const localizer = BigCalendar.momentLocalizer(moment)

class EventosPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false,
      eventos: [],
      eventoView: {}
    }
  }

  componentDidMount() {
    this.setState({ loading: true })
    axiosClient
      .get('painel/evento/listar')
      .then(response => {
        if (
          response.data.status &&
          response.data.obj &&
          Array.isArray(response.data.obj.evento)
        ) {
          this.setState({
            eventos: response.data.obj.evento.map(evento => {
              return {
                id_evento: evento.id_evento,
                data_criacao: evento.data_criacao,
                data_alteracao: evento.data_alteracao,
                nome_evento: evento.nome_evento,
                descricao: evento.descricao,
                data_inicio: moment(
                  evento.data_inicio,
                  'YYYY-MM-DD HH:mm:ss'
                ).toDate(),
                data_final: moment(
                  evento.data_final,
                  'YYYY-MM-DD HH:mm:ss'
                ).toDate(),
                tipo_evento: evento.tipo_evento,
                recorrencia: evento.recorrencia,
                tipo_recorrencia: evento.tipo_recorrencia,
                id_responsavel: evento.id_responsavel,
                dados_evento: evento.dados_evento,
                valor: evento.valor,
                id_centro_comunitario: evento.id_centro_comunitario,
                detalhes: evento.detalhes,
                status: evento.status
              }
            })
          })
        } else {
          console.error(response.data.erro)
        }
        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)

        this.setState({ loading: false })
      })
  }

  componentDidUpdate(_prevProps, prevState) {
    if (
      JSON.stringify(prevState.eventoView) !==
        JSON.stringify(this.state.eventoView) &&
      this.state.eventoView.codigoResponsavel
    ) {
      axiosClient
        .get(
          '/painel/responsavel/listar/' +
            this.state.eventoView.codigoResponsavel
        )
        .then(response => {
          if (response.data.status) {
            this.setState({
              eventoView: {
                ...this.state.eventoView,
                responsavel_nome: response.data.obj.responsavel[0]
                  ? response.data.obj.responsavel[0].nome
                  : ''
              }
            })
          }
        })
    }
  }

  toggle = evento => {
    if (evento.id) {
      this.setState({
        eventoView: evento
      })
    }
    this.setState(prevState => ({
      modal: !prevState.modal
    }))
  }

  excluirEvento = idEvento => {
    Swal({
      title: 'Você tem certeza?',
      text: 'Você não poderá reverter isso!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, quero excluir o evento!'
    }).then(result => {
      // Se "result.value" for igual a "true" excluí o usuário
      if (result.value) {
        axiosClient
          .post('/painel/evento/deletar', { id_evento: idEvento })
          .then(response => {
            // Excluindo um registro da lista
            if (response.data.status) {
              this.setState(prevState => ({
                eventos: prevState.eventos.filter(
                  evento => idEvento !== evento.id_evento
                )
              }))
              Swal('Excluído!', response.data.message, 'success')
            } else {
              Swal('Erro!', response.data.erro, 'error')
            }
            this.toggle({})
          })
      }
    })
  }

  render() {
    const { eventoView } = this.state
    const EventosCalendario = this.state.eventos.map(evento => {
      return {
        id: evento.id_evento,
        title: evento.nome_evento,
        start: evento.data_inicio,
        end: evento.data_final,
        descricao: evento.descricao,
        codigoResponsavel: evento.id_responsavel,
        codigoCentroComunitario: evento.id_centro_comunitario,
        dados_evento: evento.dados_evento
      }
    })
    return (
      <Container>
        <div className="card">
          <div className="card-body">
            <h1 className="card-title text-center">Calendário de eventos</h1>
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <Link
                    className="btn btn-success float-right mb-3"
                    to="/admin/eventos/cadastrar"
                  >
                    <i className="fas fa-plus-circle" />{' '}
                    <span className="d-none d-sm-inline">Cadastrar Evento</span>
                  </Link>
                </div>
              </div>
            </div>
            <div
              style={{
                height: '500px'
              }}
            >
              <Modal
                isOpen={this.state.modal}
                toggle={this.toggle}
                className={this.props.className}
                size="lg"
              >
                <ModalHeader>{`Editar evento`}</ModalHeader>

                <ModalBody>
                  <dl className="row">
                    <dt className="col-sm-3">Nome do evento</dt>
                    <dd className="col-sm-9">{eventoView.title}</dd>

                    <dt className="col-sm-3">Descrição do evento</dt>
                    <dd className="col-sm-9">{eventoView.descricao}</dd>

                    <dt className="col-sm-3 text-truncate">Dados Evento</dt>
                    <dd className="col-sm-9">{eventoView.dados_evento}</dd>

                    <dt className="col-sm-3">Data de início</dt>
                    <dd className="col-sm-9">
                      <p>
                        {moment(eventoView.start).format('DD/MM/YYYY ')} ás{' '}
                        {moment(eventoView.start).format('HH:mm')}
                      </p>
                    </dd>

                    <dt className="col-sm-3">Data de término</dt>
                    <dd className="col-sm-9">
                      <p>
                        {moment(eventoView.end).format('DD/MM/YYYY ')} ás{' '}
                        {moment(eventoView.end).format('HH:mm')}
                      </p>
                    </dd>

                    <dt className="col-sm-3 text-truncate">
                      Responsável evento
                    </dt>
                    <dd className="col-sm-9">{eventoView.responsavel_nome}</dd>
                  </dl>
                </ModalBody>
                <ModalFooter>
                  <Button
                    type="button"
                    color="secondary"
                    onClick={() => this.toggle({})}
                  >
                    Voltar
                  </Button>{' '}
                  <Button
                    type="button"
                    color="danger"
                    onClick={() => this.excluirEvento(eventoView.id)}
                  >
                    Apagar
                  </Button>
                  <Link
                    to={`eventos/alterar/${eventoView.id}`}
                    className="btn btn-warning"
                  >
                    Editar
                  </Link>
                </ModalFooter>
              </Modal>

              <BigCalendar
                messages={{
                  next: 'Próximo',
                  previous: 'Anterior',
                  today: 'Hoje',
                  month: 'Mês',
                  week: 'Semana',
                  day: 'Dia',
                  Agenda: 'Agenda',
                  showMore: total => `+ ${total} Ver todos`
                }}
                popup
                localizer={localizer}
                events={EventosCalendario}
                startAccessor="start"
                endAccessor="end"
                style={{
                  height: '100%'
                }}
                onSelectEvent={evento => this.toggle(evento)}
              />
            </div>
          </div>
        </div>
      </Container>
    )
  }
}
export default EventosPage
