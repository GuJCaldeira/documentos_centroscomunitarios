import React, { Component } from "react";
import {
  FormGroup,
  Label,
  Input,
  Card,
  CardTitle,
  CardHeader,
  CardBody
} from "reactstrap";
import { Redirect, Link } from "react-router-dom";
import Swal from "sweetalert2";
import classNames from "classnames";
import Select from "react-select";

import FormValidator from "helpers/FormValidator";
import axiosClient from "helpers/axiosClient";
import isEmpty from "helpers/isEmpty";

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: "red" }}>*</span>
  </label>
);

class GrupoForm extends Component {
  isSelectedAssociacao = (_data, state) => isEmpty(state.associacao);

  constructor(props) {
    super(props);

    this.validator = new FormValidator([
      {
        field: "nome_grupo",
        method: "isEmpty",
        validWhen: false,
        message: "O nome é obrigatório."
      },
      {
        field: "associacao",
        method: this.isSelectedAssociacao,
        validWhen: false,
        message: "A associação é obrigatória."
      }
    ]);

    this.state = {
      id: Math.random(),
      nome_grupo: "",
      modules: [
        /*{
          id: Math.random(),
          name: 'Associações',
          permissions: { read: true, write: true, update: true, delete: true }
        },
        {
          id: Math.random(),
          name: 'Teste',
          permissions: { read: true, write: true, update: true, delete: true }
        }*/
      ],
      status: "A",
      associacao: {},
      associacoesOptionsLoading: false,
      associacoesOptions: [],

      validation: this.validator.valid(),
      submitted: false,
      redirect: false,
      loadingData: false
    };

    this.submitted = false;
  }

  async componentDidMount() {
    const {
      match: { params }
    } = this.props;

    this.setState({
      loading: true,
      associacoesOptionsLoading: true
    });

    this.getOptions().then(async associacoesOptions => {
      if (params.id) {
        try {
          const response = await axiosClient.get(
            "painel/usuario_grupo/permissao/" + params.id
          );

          if (response.data.status) {
            this.setState({
              nome_grupo: response.data.nome_grupo,
              associacao: associacoesOptions.find(
                elem => elem.value !== response.data.id_associacao
              ),
              modules: response.data.permissoes.map(module => {
                return {
                  id: module.id,
                  name: module.nome,
                  permissions: module.permissoes.map(elem => ({
                    ...elem
                    //checked: true
                  }))
                };
              })
            });
          } else {
            console.error(response.data.erro);
          }
          this.setState({ loading: false });
        } catch (err) {
          console.error(err);
          this.setState({ loading: false });
        }
      } else {
        const response = await axiosClient.get(
          "painel/usuario_grupo/listarPermissoes"
        );
        try {
          if (response.data.status) {
            this.setState({
              modules: response.data.permissoes.map(module => {
                return {
                  id: module.id,
                  name: module.nome,
                  permissions: module.permissoes.map(elem => ({
                    ...elem
                    //checked: true
                  }))
                };
              })
            });
          } else {
            console.error(response.data.erro);
          }
          this.setState({ loading: false });
        } catch (err) {
          console.error(err);
          this.setState({ loading: false });
        }
      }
    });
  }

  handleSubmit = e => {
    e.preventDefault();

    const { status, modules, nome_grupo, associacao } = this.state;
    const {
      match: { params }
    } = this.props;

    const validation = this.validator.validate(this.state);
    this.setState({ validation, submitted: true });
    this.submitted = true;

    if (validation.isValid) {
      if (params.id) {
        axiosClient
          .post("/painel/usuario_grupo/atualizar", {
            id_usuario_grupo: params.id,
            status,
            modules: modules,
            nome_grupo: nome_grupo,
            id_associacao: associacao.value
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false;
              this.setState({
                redirect: true
              });
              Swal({
                position: "top-end",
                type: "success",
                title: "Grupo de Permissões salvo com sucesso!",
                showConfirmButton: false,
                timer: 1500
              });
            } else {
              Swal({
                type: "error",
                title: "Ocorreu um problema, por favor, tente novamente...",
                html: response.data.erro
              });
              this.submitted = false;
              this.setState({
                submitted: false
              });
            }
          })
          .catch(console.error);
      } else {
        axiosClient
          .post("/painel/usuario_grupo/inserir", {
            status,
            modules: modules,
            nome_grupo: nome_grupo,
            id_associacao: associacao.value
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false;
              this.setState({
                redirect: true
              });
              Swal({
                position: "top-end",
                type: "success",
                title: "Grupo de Permissões inserido com sucesso!",
                showConfirmButton: false,
                timer: 1500
              });
            } else {
              Swal({
                type: "error",
                title: "Ocorreu um problema, por favor, tente novamente...",
                html: response.data.erro
              });
              this.submitted = false;
              this.setState({
                submitted: false
              });
            }
          })
          .catch(console.error);
      }
    }
  };

  changePermissionModule = (moduleId, permission) => {
    this.setState({
      modules: this.state.modules.map(modulo =>
        modulo.id === moduleId
          ? {
              ...modulo,
              permissions: modulo.permissions.map(elem =>
                elem.id === permission.id
                  ? { ...elem, checked: !elem.checked }
                  : elem
              )
            }
          : modulo
      )
    });
  };

  handleChange = e => this.setState({ [e.target.name]: e.target.value });

  getOptions = async () => {
    this.setState({
      associacoesOptionsLoading: true
    });
    try {
      const associacoesResponse = await axiosClient.get(
        "/painel/comum/listarAssociacoes"
      );

      const associacoesOptions = associacoesResponse.data.obj.associacoes
        .map(associacao => {
          return {
            ...associacao,
            value: associacao.id_associacao,
            label: associacao.razao_social
          };
        })
        .filter(associacao => associacao.status === "A");

      this.setState({
        associacoesOptions,
        associacoesOptionsLoading: false
      });
      return associacoesOptions;
    } catch (err) {
      console.log(err, "ups.. couldn't get options");
    }
  };

  render() {
    const { loadingData } = this.state;
    const validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;

    const inputClassNames = isInvalid => {
      return classNames("form-control", { "is-invalid": isInvalid });
    };
    if (this.state.redirect)
      return <Redirect to="/admin/grupos-de-permissoes" />;

    return (
      <div className="container-fluid">
        <Link
          className="btn btn-secondary mb2"
          to="/admin/grupos-de-permissoes"
        >
          <i className="fas fa-arrow-left mr-2" />
          Voltar a lista
        </Link>
        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Formulário de Grupo de permissões</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>
        <Card>
          <CardHeader>
            <CardTitle>Grupo</CardTitle>
            <p className="card-category">Permissões do Grupo</p>
          </CardHeader>
          <CardBody>
            <form onSubmit={this.handleSubmit}>
              <div className="row mb-5">
                <div className="col-md-6">
                  <LabelRiquered htmlFor="nome_grupo">
                    Nome do Grupo
                  </LabelRiquered>
                  <input
                    type="text"
                    name="nome_grupo"
                    id="nome_grupo"
                    className={inputClassNames(validation.nome_grupo.isInvalid)}
                    value={this.state.nome_grupo}
                    onChange={this.handleChange}
                    placeholder="Informe o nome do grupo de usuários"
                  />
                  <div className="invalid-feedback">
                    {validation.nome_grupo.message}
                  </div>
                </div>
                <div className="col-md-6">
                  <LabelRiquered>Associação</LabelRiquered>
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    isMulti={false}
                    value={this.state.associacao}
                    isLoading={this.state.associacoesOptionsLoading}
                    isDisabled={this.state.associacoesOptionsLoading}
                    isSearchable={true}
                    name="associacao"
                    placeholder="Selecione..."
                    options={this.state.associacoesOptions}
                    onChange={selectedOption =>
                      this.setState({ associacao: selectedOption })
                    }
                  />
                  <div className="text-danger">
                    {validation && validation.associacao.message}
                  </div>
                </div>
              </div>
              <fieldset disabled={loadingData}>
                <div className="table-responsive">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">Módulo</th>
                        <th scope="col">Permissões</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.modules.map(modulo => (
                        <tr key={modulo.id}>
                          <td>{modulo.name}</td>
                          <td>
                            {modulo.permissions.map(permission => (
                              <FormGroup check inline key={permission.id}>
                                <Label check>
                                  <Input
                                    type="checkbox"
                                    value={permission.checked}
                                    checked={permission.checked}
                                    onChange={e => {
                                      this.changePermissionModule(
                                        modulo.id,
                                        permission
                                      );
                                    }}
                                  />{" "}
                                  {permission.nome}
                                </Label>
                              </FormGroup>
                            ))}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>

                <div className="form-row">
                  <div className="form-group col-md-3">
                    <label>Status</label>
                    <select
                      name="status"
                      className={inputClassNames()}
                      onChange={this.handleChange}
                      value={this.state.status}
                    >
                      <option value="A">Ativo</option>
                      <option value="I">Inativo</option>
                    </select>
                  </div>
                </div>

                <button
                  type="submit"
                  className="btn btn-success"
                  onClick={this.handleSubmit}
                >
                  Salvar
                </button>
              </fieldset>
            </form>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default GrupoForm;
