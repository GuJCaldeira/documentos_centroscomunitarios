import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ReactTable from 'react-table'
import { ButtonGroup } from 'reactstrap'
import Swal from 'sweetalert2'

// import { Container } from './styles'

import axiosClient from 'helpers/axiosClient'

import filterCaseInsensitive from 'helpers/filterCaseInsensitive'

class Grupos extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modulos: [],
      groups: [],
      newGroup: '',
      loading: false
    }

    this.excluirGrupo = this.excluirGrupo.bind(this)
  }

  componentDidMount() {
    this.setState({ loading: true })
    axiosClient
      .get('painel/usuario_grupo/listar')
      .then(response => {
        console.log(response)
        if (
          response.data.status &&
          response.data.obj &&
          Array.isArray(response.data.obj.grupos)
        ) {
          this.setState({
            groups: response.data.obj.grupos.map(group => ({
              id_grupo: group.id_grupo,
              description: group.nome_grupo,
              status: group.status
            }))
          })
        } else {
          console.error(response.data.erro)
        }

        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)

        this.setState({ loading: false })
      })
  }

  excluirGrupo(id_grupo) {
    // Confirmação de exclusão
    Swal({
      title: 'Você tem certeza?',
      text: 'Você não poderá reverter isso!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, quero excluir o grupo de permissões!'
    }).then(result => {
      // Se "result.value" for igual a "true" excluí o usuário
      if (result.value) {
        axiosClient
          .post('/painel/usuario_grupo/deletar', { id_grupo: id_grupo })
          .then(response => {
            // Excluindo um registro da lista
            if (response.data.status) {
              this.setState(prevState => ({
                grupo: prevState.grupo.filter(
                  grupo => id_grupo !== grupo.id_grupo
                )
              }))
              Swal('Excluído!', response.data.message, 'success')
            } else {
              Swal('Erro!', response.data.erro, 'error')
            }
          })
      }
    })
  }

  addGroup = () => {
    this.setState({
      groups: [
        ...this.state.groups,
        {
          id_grupo: Math.random(),
          description: this.state.newGroup,
          status: 'A'
        }
      ],
      newGroup: ''
    })
  }

  render() {
    const columns = [
      {
        Header: 'Código', // Cabeçalho da coluna
        accessor: 'id_grupo', // String-based value accessors!
        width: 70,
        filterable: false
      },
      {
        Header: 'Descrição',
        accessor: 'description' // String-based value accessors!
      },
      {
        Header: 'Status',
        id: "status",
        Cell: props => (
          <div className="d-flex justify-content-center align-items-center">
            <span
              className={`badge badge-${
                props.original.status === 'A' ? 'success' : 'secondary'
              }`}
            >
              {props.original.status === 'A' ? 'Ativo' : 'Inativo'}
            </span>
          </div>
        ),
        filterMethod: (filter, row) => {
          if(filter.value === "todos") return true
          
          return row._original.status === filter.value
        },
        Filter: ({ filter, onChange }) =>
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%" }}
            value={filter ? filter.value : "todos"}
          >
            <option value="todos">Todos</option>
            <option value="A">Ativo</option>
            <option value="I">Inativo</option>  
          </select>,
        width: 100
      },
      {
        Header: 'Ações',
        filterable: false,
        Cell: props => (
          <div className="d-flex justify-content-center align-items-center">
            <ButtonGroup size="sm">
              <Link
                to={`/admin/grupos-de-permissoes/alterar/${props.row.id_grupo}`}
                className="btn btn-warning"
              >
                <i className="fas fa-pencil-alt" />{' '}
                <span className="d-none d-sm-inline">Editar</span>
              </Link>
              <button
                onClick={() => this.excluirGrupo(props.row.id_grupo)}
                className="btn btn-danger"
              >
                <i className="fas fa-times" />{' '}
                <span className="d-none d-sm-inline">Excluir</span>
              </button>
            </ButtonGroup>
          </div>
        )
      }
    ]
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card ">
              <div className="card-header ">
                <div className="d-flex justify-content-between align-items-center">
                  <div>
                    <h4 className="card-title">Grupos</h4>
                    <p className="card-category">
                      Cada grupo contém permissões do sistema
                    </p>
                  </div>
                  <Link
                    className="btn btn-success float-right"
                    to="/admin/grupos-de-permissoes/cadastrar"
                  >
                    <i className="fas fa-plus-circle" />{' '}
                    <span className="d-none d-sm-inline">Novo Grupo</span>
                  </Link>
                </div>
              </div>
              <div className="card-body">
                <ReactTable
                  columns={columns}
                  data={this.state.groups}
                  loading={this.state.loading}
                  defaultPageSize={10}
                  filterable
                  defaultFilterMethod={filterCaseInsensitive}
                  className="-striped -highlight"
                  previousText="Anterior"
                  nextText="Próximo"
                  loadingText="Carregando..."
                  noDataText="Nenhum grupo foi encontrado"
                  pageText="Pág."
                  ofText="de"
                  rowsText="linhas"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Grupos
