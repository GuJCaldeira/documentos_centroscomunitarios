import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import classNames from 'classnames'
import Select from 'react-select'

import FormValidator from 'helpers/FormValidator'
import axiosClient from 'helpers/axiosClient'
import isEmpty from 'helpers/isEmpty'

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: 'red' }}>*</span>
  </label>
)

class CargoForm extends Component {
  isSelectedAssociacao = (_data, state) => isEmpty(state.associacao)

  constructor(props) {
    super(props)

    this.validator = new FormValidator([
      {
        field: 'nome_cargo',
        method: 'isEmpty',
        validWhen: false,
        message: 'O nome do Cargo é obrigatório.'
      },
      {
        field: 'associacao',
        method: this.isSelectedAssociacao,
        validWhen: false,
        message: 'A associação é obrigatória.'
      }
    ])

    this.state = {
      id_cargo: '',
      nome_cargo: '',
      detalhes: '',
      status: 'A',
      associacao: {},
      associacoesOptionsLoading: false,
      associacoesOptions: [],
      modoEdit: false,
      validation: this.validator.valid(),
      submitted: false,
      redirect: false,
      loadingData: false
    }

    this.submitted = false
  }

  componentDidMount() {
    this.setState({
      loadingData: true,
      associacoesOptionsLoading: true
    })
    this.getOptions()
    if (this.props.match.params && this.props.match.params.id) {
      const id = this.props.match.params.id
      this.setState({
        modoEdit: true
      })
      this.getUserById(id)
    } else {
      this.setState({
        loadingData: false
      })
    }
  }

  getUserById = id => {
    axiosClient
      .get('/painel/cargo/listar/' + id)
      .then(response => {
        if (response.data.status) {
          let cargo = response.data.obj[0]
          this.setState({
            id_cargo: cargo.id_cargo || '',
            nome_cargo: cargo.nome_cargo || '',
            status: cargo.status || '',

            loadingData: false
          })
        } else {
          Swal({
            type: 'error',
            title: 'Oops...',
            html: response.data.erro
          })
        }
      })
      .catch(error => {
        console.error(error)
      })
  }

  handleChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  handleSubmit = e => {
    e.preventDefault()

    const validation = this.validator.validate(this.state)
    this.setState({ validation, submitted: true })
    this.submitted = true

    if (validation.isValid) {
      let { id_cargo, nome_cargo, associacao, detalhes, status } = this.state
      if (this.props.match.params && this.props.match.params.id) {
        axiosClient
          .post('/painel/cargo/atualizar', {
            id_cargo,
            nome_cargo,
            id_associacao: associacao.value,
            detalhes,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Oops...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      } else {
        axiosClient
          .post('/painel/cargo/inserir', {
            nome_cargo,
            detalhes,
            id_associacao: associacao.value,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Oops...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      }
    } else {
      this.setState({ submitted: false })
    }
  }

  getOptions = async () => {
    this.setState({
      associacoesOptionsLoading: true
    })
    try {
      const associacoesResponse = await axiosClient.get(
        '/painel/comum/listarAssociacoes'
      )

      const associacoesOptions = associacoesResponse.data.obj.associacoes
        .map(associacao => {
          return {
            ...associacao,
            value: associacao.id_associacao,
            label: associacao.razao_social
          }
        })
        .filter(associacao => associacao.status === 'A')

      this.setState({
        associacoesOptions,
        associacoesOptionsLoading: false
      })
    } catch (err) {
      console.log(err, "ups.. couldn't get options")
    }
  }

  render() {
    const { loadingData } = this.state
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation
    const inputClassNames = isInvalid => {
      return classNames('form-control', { 'is-invalid': isInvalid })
    }

    if (this.state.redirect) return <Redirect to="/admin/cargos" />

    return (
      <div>
        <Link className="btn btn-secondary mb-2" to="/admin/cargos">
          <i className="fas fa-arrow-left" /> Voltar a lista
        </Link>
        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Formulário do Cargo</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>
        <fieldset disabled={loadingData}>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-md-10">
                <div className="card">
                  <div
                    className={classNames('card-body', {
                      'is-loading is-loading-lg': loadingData
                    })}
                  >
                    <h3>Dados</h3>
                    <hr />
                    <div className="row">
                      <div className="form-group col-md-6">
                        <LabelRiquered>Nome do Cargo</LabelRiquered>
                        <input
                          type="text"
                          name="nome_cargo"
                          placeholder="Informe o nome do cargo"
                          className={inputClassNames(
                            validation.nome_cargo.isInvalid
                          )}
                          value={this.state.nome_cargo}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.nome_cargo.message}
                        </div>
                      </div>
                      <div className="form-group col-md-6">
                        <LabelRiquered>Associação</LabelRiquered>
                        <Select
                          className="basic-single"
                          classNamePrefix="select"
                          isMulti={false}
                          value={this.state.associacao}
                          isLoading={this.state.associacoesOptionsLoading}
                          isDisabled={this.state.associacoesOptionsLoading}
                          isSearchable={true}
                          name="associacao"
                          placeholder="Selecione..."
                          options={this.state.associacoesOptions}
                          onChange={selectedOption =>
                            this.setState({ associacao: selectedOption })
                          }
                        />
                        <div className="text-danger">
                          {validation.associacao.message}
                        </div>
                      </div>
                    </div>
                    <hr />
                    <div className="form-row">
                      <div className="form-group col-md-3">
                        <label>Status</label>
                        <select
                          name="status"
                          className={inputClassNames()}
                          onChange={this.handleChange}
                          value={this.state.status}
                        >
                          <option value="A">Ativo</option>
                          <option value="I">Inativo</option>
                        </select>
                      </div>
                    </div>
                    <button type="submit" className="btn btn-success">
                      Salvar
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </fieldset>
      </div>
    )
  }
}

export default CargoForm
