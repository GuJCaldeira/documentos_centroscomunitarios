import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import ReactTable from 'react-table'
import { ButtonGroup } from 'reactstrap'
import moment from 'moment'
import axiosClient from 'helpers/axiosClient'

import filterCaseInsensitive from 'helpers/filterCaseInsensitive'

class CargoLista extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      cargos: []
    }
  }

  componentDidMount() {
    this.setState({ loading: true })
    axiosClient
      .get('painel/cargo/listar')
      .then(response => {
        if (
          response.data.status &&
          response.data.obj &&
          Array.isArray(response.data.obj)
        ) {
          this.setState({
            cargos: response.data.obj.map(cargo => ({
              id_cargo: cargo.id_cargo,
              nome_cargo: cargo.nome_cargo,
              data_criacao: moment(cargo.data_criacao, "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY"),
              status: cargo.status
            }))
          })
        } else {
          console.error(response.data.erro)
        }

        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)

        this.setState({ loading: false })
      })
  }

  componentDidCatch(error, info) {
    console.log(error, info)

    this.setState({
      cargos: []
    })
  }

  toggleStatus = cargoAtual => {
    // Confirmação de exclusão
    Swal({
      title: 'Você tem certeza?',
      text: `Tem certeza que quer ${
        cargoAtual.status === 'A' ? 'DESATIVAR' : 'ATIVAR'
      } o cargo "${cargoAtual.nome_cargo}"?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Sim, quero ${
        cargoAtual.status === 'A' ? 'DESATIVAR' : 'ATIVAR'
      } o cargo!`
    }).then(result => {
      // Se "result.value" for igual a "true" exclúi o cargo
      if (result.value) {
        axiosClient
          .post('/painel/cargo/atualizar', {
            ...cargoAtual,
            status: cargoAtual.status === 'A' ? 'I' : 'A'
          })
          .then(response => {
            // Excluindo um registro da lista
            if (response.status)
              this.setState(prevState => ({
                cargos: prevState.cargos.map(
                  cargo =>
                    cargoAtual.id_cargo !== cargo.id_cargo
                      ? cargo
                      : {
                          ...cargoAtual,
                          status: cargoAtual.status === 'A' ? 'I' : 'A'
                        }
                )
              }))
            Swal(
              'Alterado!',
              'Status do cargo alterado com sucesso.',
              'success'
            )
          })
      }
    })
  }

  render() {
    // definindo colunas para a tabela
    const columns = [
      {
        Header: 'Código', // Cabeçalho da coluna
        accessor: 'id_cargo', // String-based value accessors!
        filterable: false,
        width: 70
      },
      {
        Header: 'Nome',
        accessor: 'nome_cargo' // String-based value accessors!
      },
      {
        Header: 'Data de Inclusão',
        accessor: 'data_criacao' // String-based value accessors!
      },
      {
        Header: 'Status',
        Cell: props => (
          <div
            className="d-flex justify-content-center align-items-center "
            style={{ width: 60, height: 30 }}
          >
            <span
              className={`d-flex justify-content-center align-items-center text-center badge badge-${
                props.original.status === 'A' ? 'success' : 'secondary'
              }`}
              style={{ width: 50, height: 30 }}
            >
              {props.original.status === 'A' ? 'Ativo' : 'Inativo'}
            </span>
          </div>
        ),        
        
        filterMethod: (filter, row) => {
          if(filter.value === "todos") return true
          
          return row._original.status === filter.value
        },
        Filter: ({ filter, onChange }) =>
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%" }}
            value={filter ? filter.value : "todos"}
          >
            <option value="todos">Todos</option>
            <option value="A">Ativo</option>
            <option value="I">Inativo</option>  
          </select>,
        width: 100
      },
      {
        Header: 'Ações',
        filterable: false,
        Cell: props => (
          <div className="d-flex justify-content-center align-items-center">
            <ButtonGroup size="sm">
              <Link
                to={`/admin/cargos/alterar/${props.row.id_cargo}`}
                className="btn btn-warning text-white"
              >
                <i className="fas fa-pencil-alt" />{' '}
                <span className="d-none d-sm-inline">Editar</span>
              </Link>
              {props.original.status === 'A' ? (
                <button
                  onClick={() => this.toggleStatus(props.original)}
                  className="btn btn-danger"
                >
                  <i className="fas fa-times" />{' '}
                  <span className="d-none d-sm-inline">Desativar</span>
                </button>
              ) : (
                <button
                  onClick={() => this.toggleStatus(props.original)}
                  className="btn btn-info"
                >
                  <i className="fas fa-times" />{' '}
                  <span className="d-none d-sm-inline">Ativar</span>
                </button>
              )}
            </ButtonGroup>
          </div>
        )
      }
    ]
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card ">
              <div className="card-header ">
                <div className="row align-items-center justify-content-between">
                  <div>
                    <h4 className="card-title">Lista de Cargos</h4>
                    <p className="card-category">Grupos de cargos do sistema</p>
                  </div>
                  <Link
                    className="btn btn-success float-right"
                    to="/admin/cargos/cadastrar"
                  >
                    <i className="fas fa-plus-circle" />{' '}
                    <span className="d-none d-sm-inline">Cadastrar Cargo</span>
                  </Link>
                </div>
              </div>
              <div className="card-body">
                <ReactTable
                  columns={columns}
                  data={this.state.cargos}
                  loading={this.state.loading}
                  defaultPageSize={10}
                  filterable
                  defaultFilterMethod={filterCaseInsensitive}
                  className="-striped -highlight"
                  previousText="Anterior"
                  nextText="Próximo"
                  loadingText="Carregando..."
                  noDataText="Nenhum cargo foi encontrado"
                  pageText="Pág."
                  ofText="de"
                  rowsText="linhas"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default CargoLista
