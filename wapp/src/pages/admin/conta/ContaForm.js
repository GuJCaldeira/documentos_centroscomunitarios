import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import classNames from 'classnames'
import 'moment/locale/pt-br'

import Select from 'react-select'

import FormValidator from 'helpers/FormValidator'

import axiosClient from 'helpers/axiosClient'

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: 'red' }}>*</span>
  </label>
)

class ContaForm extends Component {
  constructor(props) {
    super(props)

    this.validator = new FormValidator([
      {
        field: 'nome_conta',
        method: 'isEmpty',
        validWhen: false,
        message: 'A descrição do Grupo de conta é obrigatória'
      }
    ])

    this.state = {
      id_conta: '',
      nome_conta: '',
      status: 'A',
      modoEdit: false,
      associacoesOptions: [],
      associacoesOptionsLoading: false,

      validation: this.validator.valid(),
      submitted: false,
      redirect: false,
      loadingData: false
    }

    this.submitted = false
  }

  componentDidMount() {
    this.setState({
      loadingData: true
    })
    this.getOptions().then(() => {
      if (this.props.match.params && this.props.match.params.id) {
        const id = this.props.match.params.id
        this.setState({
          modoEdit: true
        })
        this.getUserById(id)
      } else {
        this.setState({
          loadingData: false
        })
      }
    })
  }

  getUserById = async id => {
    try {
      const response = await axiosClient.get('/painel/conta/listar/' + id)
      if (response.data.status) {
        let conta = response.data.obj.conta[0]

        this.setState({
          nome_conta: conta.nome_conta || '',
          associacao: this.state.associacoesOptions.find(
            x => x.value === conta.id_associacao
          ),
          status: conta.status || '',

          loadingData: false
        })
      } else {
        Swal({
          type: 'error',
          title: 'Ocorreu um problema, por favor, tente novamente...',
          html: response.data.erro
        })
      }
    } catch (err) {
      console.error(err)
    }
  }

  handleChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  handleSubmit = e => {
    e.preventDefault()

    const validation = this.validator.validate(this.state)
    this.setState({ validation, submitted: true })
    this.submitted = true

    if (validation.isValid) {
      let { nome_conta, associacao, status } = this.state

      if (this.props.match.params && this.props.match.params.id) {
        const id = this.props.match.params.id
        axiosClient
          .post('/painel/conta/atualizar', {
            id_conta: id,
            nome_conta,
            id_associacao: associacao.value,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Ocorreu um problema, por favor, tente novamente...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      } else {
        axiosClient
          .post('/painel/conta/inserir', {
            nome_conta,
            id_associacao: associacao.value,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: 'Grupo de conta inserido com sucesso!',
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Ocorreu um problema, por favor, tente novamente...',
                html: response.data.erro
              })
              this.setState({
                submitted: false
              })
            }
          })
          .catch(console.error)
      }
    } else {
      this.setState({ submitted: false })
    }
  }

  getOptions = () => {
    return new Promise((resolve, reject) => {
      this.setState({
        associacoesOptionsLoading: true
      })
      Promise.all([axiosClient.get('/painel/comum/listarAssociacoes')])
        .then(([associacoesResponse]) => {
          const associacoesOptions = associacoesResponse.data.obj.associacoes
            .map(associacao => {
              return {
                ...associacao,
                value: associacao.id_associacao,
                label: associacao.razao_social
              }
            })
            .filter(associacao => associacao.status === 'A')

          this.setState({
            associacoesOptions,
            associacoesOptionsLoading: false
          })
          resolve()
        })
        .catch(error => {
          console.log(
            error,
            'Ocorreu um problema, por favor, tente novamente...'
          )
          reject(error)
        })
    })
  }

  toggleCheckboxChange = e => {
    this.setState({
      checkboxSenha: e.target.checked
    })
  }

  render() {
    const { loadingData } = this.state
    const validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation
    const inputClassNames = isInvalid => {
      return classNames('form-control', { 'is-invalid': isInvalid })
    }

    if (this.state.redirect) return <Redirect to="/admin/conta" />

    return (
      <div>
        <Link className="btn btn-secondary mb-2" to="/admin/conta">
          {' '}
          <i className="fas fa-arrow-left" /> Voltar a lista
        </Link>

        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Formulário Grupo de Contas</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>

        <fieldset disabled={loadingData}>
          <form onSubmit={this.handleSubmit}>
            <div className="form-row">
              <div className="col-md-10">
                <div className="card">
                  <div
                    className={classNames('card-body', {
                      'is-loading is-loading-lg': loadingData
                    })}
                  >
                    <div className="form-group">
                      <LabelRiquered>Nome da Conta</LabelRiquered>
                      <input
                        type="text"
                        name="nome_conta"
                        placeholder="Informe o nome do grupo de conta"
                        className={inputClassNames(
                          validation.nome_conta.isInvalid
                        )}
                        value={this.state.nome_conta}
                        onChange={this.handleChange}
                        autoComplete={'off'}
                      />
                      <div className="invalid-feedback">
                        {validation.nome_conta.message}
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <div className="form-group">
                          <LabelRiquered>Associação</LabelRiquered>
                          <Select
                            className="basic-single"
                            classNamePrefix="select"
                            isMulti={false}
                            value={this.state.associacao}
                            isLoading={this.state.associacoesOptionsLoading}
                            isSearchable={true}
                            name="associacao"
                            placeholder="Selecione..."
                            options={this.state.associacoesOptions}
                            onChange={selectedOption =>
                              this.setState({ associacao: selectedOption })
                            }
                          />
                        </div>
                      </div>
                      <div className="form-group col-md-6">
                        <div className="form-group">
                          <label>Status</label>
                          <select
                            name="status"
                            className={inputClassNames()}
                            onChange={this.handleChange}
                            value={this.state.status}
                            autoComplete={'off'}
                          >
                            <option value="A">Ativo</option>
                            <option value="I">Inativo</option>
                          </select>
                        </div>
                      </div>
                      <button
                        type="submit"
                        className="btn btn-success"
                        disabled={this.state.submitted}
                      >
                        Salvar
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </fieldset>
      </div>
    )
  }
}

export default ContaForm
