import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import classNames from 'classnames'
import cepPromise from 'cep-promise'
import MaskedInput from 'react-maskedinput'

import FormValidator from 'helpers/FormValidator'

import axiosClient from 'helpers/axiosClient'

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: 'red' }}>*</span>
  </label>
)

class CentroComunitarioForm extends Component {
  constructor(props) {
    super(props)

    this.validator = new FormValidator([
      {
        field: 'nome_centro',
        method: 'isEmpty',
        validWhen: false,
        message: 'O Nome do Centro é obrigatório.'
      },
      {
        field: 'telefone1',
        method: 'isEmpty',
        validWhen: false,
        message: 'O telefone é obrigatório'
      },
      {
        field: 'cep',
        method: 'isEmpty',
        validWhen: false,
        message: 'O CEP é obrigatório'
      },
      {
        field: 'endereco',
        method: 'isEmpty',
        validWhen: false,
        message: 'O endereço é obrigatório'
      },
      {
        field: 'numero',
        method: 'isEmpty',
        validWhen: false,
        message: 'O número é obrigatório'
      },
      {
        field: 'bairro',
        method: 'isEmpty',
        validWhen: false,
        message: 'O bairro é obrigatório'
      },
      {
        field: 'cidade',
        method: 'isEmpty',
        validWhen: false,
        message: 'O cidade é obrigatório'
      },
      {
        field: 'estado',
        method: 'isEmpty',
        validWhen: false,
        message: 'O estado é obrigatório'
      }
    ])

    this.state = {
      id_centro: '',
      nome_centro: '',
      telefone1: '',
      telefone2: '',
      cep: '',
      endereco: '',
      numero: '',
      bairro: '',
      complemento: '',
      cidade: '',
      estado: '',
      status: 'A',
      modoEdit: false,
      validation: this.validator.valid(),
      submitted: false,
      redirect: false,
      loadingData: false
    }

    this.submitted = false
  }

  componentDidMount() {
    this.setState({
      loadingData: true
    })
    if (this.props.match.params && this.props.match.params.id) {
      const id = this.props.match.params.id
      this.setState({
        modoEdit: true
      })
      this.getUserById(id)
    } else {
      this.setState({
        loadingData: false
      })
    }
  }

  getUserById = id => {
    axiosClient
      .get('/painel/centro_comunitario/listar/' + id)
      .then(response => {
        if (response.data.status) {
          let centro_comunitario = response.data.obj.centro_comunitario[0]
          this.setState({
            id_centro: centro_comunitario.id_centro_comunitario || '',
            nome_centro: centro_comunitario.nome_centro || '',
            telefone1: centro_comunitario.telefone1 || '',
            telefone2: centro_comunitario.telefone2 || '',
            cep: centro_comunitario.cep || '',
            endereco: centro_comunitario.endereco || '',
            numero: centro_comunitario.numero || '',
            bairro: centro_comunitario.bairro || '',
            complemento: centro_comunitario.complemento || '',
            cidade: centro_comunitario.cidade || '',
            estado: centro_comunitario.estado || '',
            status: centro_comunitario.status || '',

            loadingData: false
          })
        } else {
          Swal({
            type: 'error',
            title: 'Oops...',
            html: response.data.erro
          })
        }
      })
      .catch(error => {
        console.error(error)
      })
  }

  handleChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  handleSubmit = e => {
    e.preventDefault()

    const validation = this.validator.validate(this.state)
    this.setState({ validation, submitted: true })
    this.submitted = true

    if (validation.isValid) {
      let {
        id_centro,
        nome_centro,
        telefone1,
        telefone2,
        cep,
        endereco,
        numero,
        bairro,
        complemento,
        cidade,
        estado,
        status
      } = this.state

      if (this.props.match.params && this.props.match.params.id) {
        axiosClient
          .post('/painel/centro_comunitario/atualizar', {
            id_centro,
            nome_centro,
            telefone1,
            telefone2,
            cep,
            endereco,
            numero,
            bairro,
            complemento,
            cidade,
            estado,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Oops...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      } else {
        axiosClient
          .post('/painel/centro_comunitario/inserir', {
            nome_centro,
            telefone1,
            telefone2,
            cep,
            endereco,
            numero,
            bairro,
            complemento,
            cidade,
            estado,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Oops...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      }
    } else {
      this.setState({ submitted: false })
    }
  }

  handleChangeCep = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })

    this.pesquisaCep(value)
  }

  pesquisaCep = cep => {
    if (cep.length >= 8) {
      cepPromise(cep)
        .then(response => {
          this.setState({
            estado: response.state,
            cidade: response.city,
            bairro: response.neighborhood,
            endereco: response.street
          })
        })
        .catch(console.error)
    }
  }

  render() {
    const { loadingData } = this.state
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation
    const inputClassNames = isInvalid => {
      return classNames('form-control', { 'is-invalid': isInvalid })
    }

    if (this.state.redirect)
      return <Redirect to="/admin/centros-comunitarios" />

    return (
      <div>
        <Link
          className="btn btn-secondary mb-2"
          to="/admin/centros-comunitarios/"
        >
          <i className="fas fa-arrow-left" /> Voltar a lista
        </Link>

        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Formulário do Centro Comunitario</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>

        <fieldset disabled={loadingData}>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-md-10">
                <div className="card">
                  <div
                    className={classNames('card-body', {
                      'is-loading is-loading-lg': loadingData
                    })}
                  >
                    <h3>Informações Cadastrais</h3>
                    <div className="form-group">
                      <LabelRiquered>Nome do Centro</LabelRiquered>
                      <input
                        type="text"
                        name="nome_centro"
                        className={inputClassNames(
                          validation.nome_centro.isInvalid
                        )}
                        value={this.state.nome_centro}
                        placeholder="Digite o nome do centro"
                        onChange={this.handleChange}
                      />
                      <div className="invalid-feedback">
                        {validation.nome_centro.message}
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <LabelRiquered>Telefone</LabelRiquered>
                          <MaskedInput
                            type="text"
                            name="telefone1"
                            mask="(11) 1111-1111"
                            placeholder="(00) 0000-0000"
                            className={inputClassNames(
                              validation.telefone1.isInvalid
                            )}
                            value={this.state.telefone1}
                            onChange={this.handleChange}
                          />
                          <div className="invalid-feedback">
                            {validation.telefone1.message}
                          </div>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="form-group">
                          <label>Celular</label>
                          <MaskedInput
                            type="text"
                            name="telefone2"
                            mask="(11) 1 1111-1111"
                            placeholder="(00) 9 0000-0000"
                            className={inputClassNames()}
                            value={this.state.telefone2}
                            onChange={this.handleChange}
                          />
                        </div>
                      </div>
                    </div>
                    <hr />

                    <h3>Endereço</h3>
                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <LabelRiquered htmlFor="cep">CEP</LabelRiquered>
                        <MaskedInput
                          type="text"
                          name="cep"
                          mask="11111-111"
                          value={this.state.cep}
                          className={inputClassNames(validation.cep.isInvalid)}
                          placeholder="Pesquisar o CEP"
                          onChange={this.handleChangeCep}
                        />
                        <div className="invalid-feedback">
                          {validation.cep.message}
                        </div>
                      </div>
                      <div className="form-group col-md-9">
                        <LabelRiquered htmlFor="endereco">
                          Endereço
                        </LabelRiquered>
                        <input
                          type="text"
                          name="endereco"
                          placeholder="Informe endereço"
                          value={this.state.endereco}
                          className={inputClassNames(
                            validation.endereco.isInvalid
                          )}
                          id="endereco"
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.endereco.message}
                        </div>
                      </div>
                      <div className="form-group col-md-3">
                        <LabelRiquered htmlFor="numero">Número</LabelRiquered>
                        <input
                          type="text"
                          name="numero"
                          placeholder="0000"
                          value={this.state.numero}
                          className={inputClassNames(
                            validation.numero.isInvalid
                          )}
                          id="numero"
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.numero.message}
                        </div>
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <LabelRiquered htmlFor="bairro">Bairro</LabelRiquered>
                        <input
                          type="text"
                          name="bairro"
                          placeholder="Informe bairro"
                          value={this.state.bairro}
                          className={inputClassNames(
                            validation.bairro.isInvalid
                          )}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.bairro.message}
                        </div>
                      </div>
                      <div className="form-group col-md-6">
                        <label htmlFor="complemento">Complemento</label>
                        <input
                          type="text"
                          name="complemento"
                          placeholder="Complemento"
                          value={this.state.complemento}
                          className="form-control"
                          onChange={this.handleChange}
                        />
                      </div>
                      <div className="form-group col-md-7">
                        <LabelRiquered htmlFor="cidade">Cidade</LabelRiquered>
                        <input
                          type="text"
                          name="cidade"
                          placeholder="Informe a cidade"
                          value={this.state.cidade}
                          className={inputClassNames(
                            validation.cidade.isInvalid
                          )}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.cidade.message}
                        </div>
                      </div>
                      <div className="form-group col-md-5">
                        <LabelRiquered htmlFor="inputState">
                          Estado
                        </LabelRiquered>
                        <input
                          id="inputState"
                          type="text"
                          name="estado"
                          placeholder="Sigla da UF"
                          value={this.state.estado}
                          className={inputClassNames(
                            validation.estado.isInvalid
                          )}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.estado.message}
                        </div>
                      </div>
                    </div>

                    <hr />
                    <div className="form-row">
                      <div className="form-group col-md-3">
                        <label>Status</label>
                        <select
                          name="status"
                          className={inputClassNames()}
                          onChange={this.handleChange}
                          value={this.state.status}
                        >
                          <option value="A">Ativo</option>
                          <option value="I">Inativo</option>
                        </select>
                      </div>
                    </div>

                    <button type="submit" className="btn btn-success">
                      Salvar
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </fieldset>
      </div>
    )
  }
}

export default CentroComunitarioForm
