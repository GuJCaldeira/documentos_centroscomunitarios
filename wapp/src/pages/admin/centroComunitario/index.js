import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import ReactTable from 'react-table'
import { ButtonGroup } from 'reactstrap'
import axiosClient from 'helpers/axiosClient'

import filterCaseInsensitive from 'helpers/filterCaseInsensitive'

class CentroComunitarioLista extends Component {
  constructor(props) {
    super(props)

    this.state = {
      centros_comunitarios: [],
      loading: false
    }

    this.excluirCentroComunitario = this.excluirCentroComunitario.bind(this)
  }

  componentDidMount() {
    this.setState({ loading: true })
    axiosClient
      .get('painel/centro_comunitario/listar')
      .then(response => {
        if (
          response.data.status &&
          response.data.obj &&
          Array.isArray(response.data.obj.centro_comunitario)
        ) {
          this.setState({
            centros_comunitarios: response.data.obj.centro_comunitario.map(
              centro_comunitario => {
                return {
                  id_centro_comunitario:
                    centro_comunitario.id_centro_comunitario,
                  nome_centro: centro_comunitario.nome_centro,
                  bairro: centro_comunitario.bairro,
                  status: centro_comunitario.status
                }
              }
            )
          })
        } else {
          console.error(response.data.erro)
        }

        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)

        this.setState({ loading: false })
      })
  }

  componentDidCatch(error, info) {
    console.log(error, info)

    this.setState({
      centro_comunitario: []
    })
  }

  excluirCentroComunitario(id) {
    // Confirmação de exclusão
    Swal({
      title: 'Você tem certeza?',
      text: 'Você não poderá reverter isso!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, quero excluir o centro comunitário!'
    }).then(result => {
      // Se "result.value" for igual a "true" exclúi o centro
      if (result.value) {
        //        console.log(this.state.centros_comunitarios)
        axiosClient
          .post('/painel/centro_comunitario/deletar', {
            id_centro: id
          })
          .then(response => {
            // Excluindo um registro da lista
            if (response.data.status) {
              this.setState(prevState => ({
                centros_comunitarios: prevState.centros_comunitarios.filter(
                  centro_comunitario =>
                    id !== centro_comunitario.id_centro_comunitario
                )
              }))
              Swal('Excluído!', response.data.message, 'success')
            } else {
              Swal('Erro!', response.data.erro, 'error')
            }
          })
      }
    })
  }

  render() {
    //Definindo colunas para a tabela
    const columns = [
      {
        Header: 'Código',
        accessor: 'id_centro_comunitario',
        filterable: false,
        width: 70
      },
      {
        Header: 'Nome',
        accessor: 'nome_centro'
      },
      {
        Header: 'Bairro',
        accessor: 'bairro'
      },
      {
        Header: 'Status',
        Cell: props => (
          <div
            className="d-flex justify-content-center align-items-center "
            style={{ width: 60, height: 30 }}
          >
            <span
              className={`d-flex justify-content-center align-items-center text-center badge badge-${
                props.original.status === 'A' ? 'success' : 'secondary'
              }`}
              style={{ width: 50, height: 30 }}
            >
              {props.original.status === 'A' ? 'Ativo' : 'Inativo'}
            </span>
          </div>
        ),
        
        filterMethod: (filter, row) => {
          if(filter.value === "todos") return true
          
          return row._original.status === filter.value
        },
        Filter: ({ filter, onChange }) =>
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%" }}
            value={filter ? filter.value : "todos"}
          >
            <option value="todos">Todos</option>
            <option value="A">Ativo</option>
            <option value="I">Inativo</option>  
          </select>,
        width: 100
      },
      {
        Header: 'Ações',
        filterable: false,
        Cell: props => (
          <div className="d-flex justify-content-center align-items-center">
            <ButtonGroup size="sm">
              <Link
                to={`/admin/centros-comunitarios/alterar/${
                  props.row.id_centro_comunitario
                }`}
                className="btn btn-warning text-white"
              >
                <i className="fas fa-pencil-alt" />{' '}
                <span className="d-none d-sm-inline">Editar</span>
              </Link>
              <button
                onClick={() =>
                  this.excluirCentroComunitario(props.row.id_centro_comunitario)
                }
                className="btn btn-danger"
              >
                <i className="fas fa-times" />{' '}
                <span className="d-none d-sm-inline">Excluir</span>
              </button>
            </ButtonGroup>
          </div>
        )
      }
    ]
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card ">
              <div className="card-header ">
                <div className="row align-items-center justify-content-between">
                  <div>
                    <h4 className="card-title">Lista de Centro Comunitário</h4>
                    <p className="card-category">
                      Centro Comunitário do sistema
                    </p>
                  </div>
                  <Link
                    className="btn btn-success float-right"
                    to="/admin/centros-comunitarios/cadastrar"
                  >
                    <i className="fas fa-plus-circle" />{' '}
                    <span className="d-none d-sm-inline">
                      Cadastrar Centro Comunitário
                    </span>
                  </Link>
                </div>
              </div>
              <div className="card-body">
                <ReactTable
                  columns={columns}
                  data={this.state.centros_comunitarios}
                  loading={this.state.loading}
                  defaultPageSize={10}
                  filterable
                  defaultFilterMethod={filterCaseInsensitive}
                  className="-striped -highlight"
                  previousText="Anterior"
                  nextText="Próximo"
                  loadingText="Carregando..."
                  noDataText="Nenhum centro comunitário foi encontrado"
                  pageText="Pág."
                  ofText="de"
                  rowsText="linhas"
                />
              </div>
              <div className="card-footer ">
                <div className="stats">
                  <i className="fa fa-clock-o" /> Campaign sent 2 days ago
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default CentroComunitarioLista
