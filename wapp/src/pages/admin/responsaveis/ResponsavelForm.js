import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import classNames from 'classnames'
import cepPromise from 'cep-promise'
import MaskedInput from 'react-maskedinput'
import Select from 'react-select'

import FormValidator from 'helpers/FormValidator'
import axiosClient from 'helpers/axiosClient'
import isEmpty from 'helpers/isEmpty'

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: 'red' }}>*</span>
  </label>
)

class ResponsavelForm extends Component {
  isSelectedAssociacao = (_data, state) => isEmpty(state.associacao)

  constructor(props) {
    super(props)

    this.validator = new FormValidator([
      {
        field: 'nome',
        method: 'isEmpty',
        validWhen: false,
        message: 'O nome é obrigatório.'
      },
      {
        field: 'cpf',
        method: 'isEmpty',
        validWhen: false,
        message: 'O CPF é obrigatório.'
      },
      {
        field: 'email',
        method: 'isEmpty',
        validWhen: false,
        message: 'O email é obrigatório.'
      },
      {
        field: 'email',
        method: 'isEmail',
        validWhen: true,
        message: 'O email é inválido.'
      },
      {
        field: 'telefone1',
        method: 'isEmpty',
        validWhen: false,
        message: 'O telefone 1 é obrigatório.'
      },
      {
        field: 'cep',
        method: 'isEmpty',
        validWhen: false,
        message: 'O CEP é obrigatório.'
      },
      {
        field: 'associacao',
        method: this.isSelectedAssociacao,
        validWhen: false,
        message: 'A associação é obrigatória.'
      },
      {
        field: 'endereco',
        method: 'isEmpty',
        validWhen: false,
        message: 'O endereço é obrigatório'
      },
      {
        field: 'numero',
        method: 'isEmpty',
        validWhen: false,
        message: 'O número é obrigatório'
      },
      {
        field: 'bairro',
        method: 'isEmpty',
        validWhen: false,
        message: 'O bairro é obrigatório'
      },
      {
        field: 'cidade',
        method: 'isEmpty',
        validWhen: false,
        message: 'A cidade é obrigatória'
      },
      {
        field: 'estado',
        method: 'isEmpty',
        validWhen: false,
        message: 'O estado é obrigatório'
      }
    ])

    this.state = {
      id_responsavel: '',
      cpf: '',
      nome: '',
      email: '',
      telefone1: '',
      telefone2: '',
      cep: '',
      endereco: '',
      numero: '',
      bairro: '',
      cidade: '',
      estado: '',
      associacao: {},
      associacoesOptionsLoading: false,
      associacoesOptions: [],
      detalhes: '',
      status: 'A',
      modoEdit: false,
      validation: this.validator.valid(),
      submitted: false,
      redirect: false,
      loadingData: false
    }

    this.submitted = false
  }

  componentDidMount() {
    this.setState({
      loadingData: true,
      associacoesOptionsLoading: true
    })
    this.getOptions()
    if (this.props.match.params && this.props.match.params.id) {
      const id = this.props.match.params.id
      this.setState({
        modoEdit: true
      })
      this.getUserById(id)
    } else {
      this.setState({
        loadingData: false
      })
    }
  }

  getUserById(id) {
    axiosClient
      .get('/painel/responsavel/listar/' + id)
      .then(response => {
        if (response.data.status) {
          let responsavel = response.data.obj.responsavel[0]
          this.setState({
            id_responsavel: responsavel.id_responsavel || '',
            cpf: responsavel.cpf || '',
            nome: responsavel.nome || '',
            email: responsavel.email || '',
            telefone1: responsavel.telefone1 || '',
            telefone2: responsavel.telefone2 || '',
            cep: responsavel.cep || '',
            endereco: responsavel.endereco || '',
            numero: responsavel.numero || '',
            bairro: responsavel.bairro || '',
            cidade: responsavel.cidade || '',
            estado: responsavel.estado || '',
            status: responsavel.status || '',

            loadingData: false
          })
        } else {
          Swal({
            type: 'error',
            title: 'Oops...',
            html: response.data.erro
          })
        }
      })
      .catch(error => {
        console.error(error)
      })
  }

  handleChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  handleSubmit = e => {
    e.preventDefault()

    const validation = this.validator.validate(this.state)
    this.setState({ validation, submitted: true })
    this.submitted = true

    if (validation.isValid) {
      let {
        id_responsavel,
        cpf,
        nome,
        email,
        telefone1,
        telefone2,
        cep,
        endereco,
        numero,
        bairro,
        cidade,
        associacao,
        estado,
        detalhes,
        status
      } = this.state
      cpf = cpf.replace(/\D/g, '')

      if (this.props.match.params && this.props.match.params.id) {
        axiosClient
          .post('/painel/responsavel/atualizar', {
            id_responsavel,
            id_associacao: associacao.value,
            cpf,
            nome,
            email,
            telefone1,
            telefone2,
            cep,
            endereco,
            numero,
            bairro,
            cidade,
            estado,
            detalhes,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Oops...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      } else {
        axiosClient
          .post('/painel/responsavel/inserir', {
            cpf,
            id_associacao: associacao.value,
            nome,
            email,
            telefone1,
            telefone2,
            cep,
            endereco,
            numero,
            bairro,
            cidade,
            estado,
            detalhes,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Oops...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      }
    } else {
      this.setState({ submitted: false })
    }
  }

  handleChangeCep = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })

    this.pesquisaCep(value)
  }

  pesquisaCep = cep => {
    if (cep.length >= 8) {
      cepPromise(cep)
        .then(response => {
          this.setState({
            estado: response.state,
            cidade: response.city,
            bairro: response.neighborhood,
            endereco: response.street
          })
        })
        .catch(console.error)
    }
  }

  getOptions = async () => {
    this.setState({
      associacoesOptionsLoading: true
    })
    try {
      const associacoesResponse = await axiosClient.get(
        '/painel/comum/listarAssociacoes'
      )

      const associacoesOptions = associacoesResponse.data.obj.associacoes
        .map(associacao => {
          return {
            ...associacao,
            value: associacao.id_associacao,
            label: associacao.razao_social
          }
        })
        .filter(associacao => associacao.status === 'A')

      this.setState({
        associacoesOptions,
        associacoesOptionsLoading: false
      })
    } catch (err) {
      console.log(err, "ups.. couldn't get options")
    }
  }

  render() {
    const { loadingData } = this.state
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation
    const inputClassNames = isInvalid => {
      return classNames('form-control', { 'is-invalid': isInvalid })
    }

    if (this.state.redirect) return <Redirect to="/admin/responsaveis" />

    return (
      <div>
        <Link className="btn btn-secondary mb-2" to="/admin/responsaveis">
          <i className="fas fa-arrow-left" />
          Voltar a lista
        </Link>

        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Formulário do Responsável</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>

        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-10">
              <div className="card">
                <div
                  className={classNames('card-body', {
                    'is-loading is-loading-lg': loadingData
                  })}
                >
                  <h3>Dados Pessoais</h3>

                  <hr />

                  <div className="form-group col-md-12 p-0">
                    <LabelRiquered>Nome</LabelRiquered>
                    <input
                      type="text"
                      name="nome"
                      placeholder="Informe o nome do responsável"
                      className={inputClassNames(validation.nome.isInvalid)}
                      value={this.state.nome}
                      onChange={this.handleChange}
                    />
                    <div className="invalid-feedback">
                      {validation.nome.message}
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="form-row">
                      <div className="col-md-5">
                        <LabelRiquered>CPF</LabelRiquered>
                        <MaskedInput
                          type="text"
                          name="cpf"
                          placeholder="000.000.000-00"
                          mask="111.111.111-11"
                          className={inputClassNames(validation.cpf.isInvalid)}
                          value={this.state.cpf}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.cpf.message}
                        </div>
                      </div>
                      <div className="col-md-7">
                        <LabelRiquered>Email</LabelRiquered>
                        <input
                          type="text"
                          name="email"
                          placeholder="Informe o email"
                          className={inputClassNames(
                            validation.email.isInvalid
                          )}
                          value={this.state.email}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.email.message}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="form-row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <LabelRiquered>Telefone</LabelRiquered>
                        <MaskedInput
                          type="text"
                          name="telefone1"
                          placeholder="(00) 0000-0000"
                          mask="(11) 1111-1111"
                          className={inputClassNames(
                            validation.telefone1.isInvalid
                          )}
                          value={this.state.telefone1}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {validation.telefone1.message}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label>Celular</label>
                        <MaskedInput
                          type="text"
                          name="telefone2"
                          mask="(11) 1 1111-1111"
                          placeholder="(00) 9 0000-0000"
                          className={inputClassNames()}
                          value={this.state.telefone2}
                          onChange={this.handleChange}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <LabelRiquered>Associação</LabelRiquered>
                    <Select
                      className="basic-single"
                      classNamePrefix="select"
                      isMulti={false}
                      value={this.state.associacao}
                      isLoading={this.state.associacoesOptionsLoading}
                      isDisabled={this.state.associacoesOptionsLoading}
                      isSearchable={true}
                      name="associacao"
                      placeholder="Selecione..."
                      options={this.state.associacoesOptions}
                      onChange={selectedOption =>
                        this.setState({ associacao: selectedOption })
                      }
                    />
                    <div className="text-danger">
                      {validation.associacao.message}
                    </div>
                  </div>

                  <hr />

                  <h3>Endereço</h3>
                  <div className="form-row">
                    <div className="form-group col-md-3">
                      <LabelRiquered htmlFor="cep">CEP</LabelRiquered>
                      <MaskedInput
                        type="text"
                        name="cep"
                        mask="11111-111"
                        value={this.state.cep}
                        className={inputClassNames(validation.cep.isInvalid)}
                        placeholder="Pesquisar o CEP"
                        onChange={this.handleChangeCep}
                      />
                      <div className="invalid-feedback">
                        {validation.cep.message}
                      </div>
                    </div>
                    <div className="form-group col-md-7">
                      <LabelRiquered htmlFor="endereco">Endereço</LabelRiquered>
                      <input
                        type="text"
                        name="endereco"
                        placeholder="Informe o nome da rua, avenida, etc."
                        value={this.state.endereco}
                        className={inputClassNames(
                          validation.endereco.isInvalid
                        )}
                        id="endereco"
                        onChange={this.handleChange}
                      />
                      <div className="invalid-feedback">
                        {validation.endereco.message}
                      </div>
                    </div>
                    <div className="form-group col-md-2">
                      <LabelRiquered htmlFor="numero">Número</LabelRiquered>
                      <input
                        type="text"
                        name="numero"
                        placeholder="000"
                        value={this.state.numero}
                        className={inputClassNames(validation.numero.isInvalid)}
                        id="numero"
                        onChange={this.handleChange}
                      />
                      <div className="invalid-feedback">
                        {validation.numero.message}
                      </div>
                    </div>
                  </div>

                  <div className="form-row">
                    <div className="form-group col-md-4">
                      <LabelRiquered htmlFor="bairro">Bairro</LabelRiquered>
                      <input
                        type="text"
                        name="bairro"
                        placeholder="Informe o bairro"
                        value={this.state.bairro}
                        className={inputClassNames(validation.bairro.isInvalid)}
                        onChange={this.handleChange}
                      />
                      <div className="invalid-feedback">
                        {validation.bairro.message}
                      </div>
                    </div>
                    <div className="form-group col-md-5">
                      <LabelRiquered htmlFor="cidade">Cidade</LabelRiquered>
                      <input
                        type="text"
                        name="cidade"
                        placeholder="Informe a cidade"
                        value={this.state.cidade}
                        className={inputClassNames(validation.cidade.isInvalid)}
                        onChange={this.handleChange}
                      />
                      <div className="invalid-feedback">
                        {validation.cidade.message}
                      </div>
                    </div>
                    <div className="form-group col-md-3">
                      <LabelRiquered htmlFor="inputState">Estado</LabelRiquered>
                      <input
                        id="inputState"
                        type="text"
                        name="estado"
                        placeholder="Sigla da UF"
                        value={this.state.estado}
                        className={inputClassNames(validation.estado.isInvalid)}
                        onChange={this.handleChange}
                      />
                      <div className="invalid-feedback">
                        {validation.estado.message}
                      </div>
                    </div>
                  </div>
                  <hr />
                  <div className="form-row">
                    <div className="form-group col-md-3">
                      <label>Status</label>
                      <select
                        name="status"
                        className={inputClassNames()}
                        onChange={this.handleChange}
                        value={this.state.status}
                      >
                        <option value="A">Ativo</option>
                        <option value="I">Inativo</option>
                      </select>
                    </div>
                  </div>
                  <button type="submit" className="btn btn-success">
                    Salvar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default ResponsavelForm
