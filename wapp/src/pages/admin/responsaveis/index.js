import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import moment from 'moment'
import ReactTable from 'react-table'
import { ButtonGroup } from 'reactstrap'

import filterCaseInsensitive from 'helpers/filterCaseInsensitive'

import axiosClient from 'helpers/axiosClient'

class ResponsavelPage extends Component {
  constructor(props) {
    super(props)

    // definindo o estado inicial das variáveis que serão usadas no componente
    this.state = {
      responsaveis: [],
      loading: false
    }

    this.excluirResponsavel = this.excluirResponsavel.bind(this)
  }

  // após a renderização do componente
  componentDidMount() {
    this.setState({ loading: true })
    axiosClient
      .get('painel/responsavel/listar')
      .then(response => {
        console.log(response)
        if (
          response.data.status &&
          response.data.obj &&
          Array.isArray(response.data.obj.responsavel)
        ) {
          this.setState({
            responsaveis: response.data.obj.responsavel.map(responsavel => {
              return {
                id_responsavel: responsavel.id_responsavel,
                nome: responsavel.nome,
                data: moment(
                  responsavel.data_criacao,
                  'YYYY-MM-DD HH:mm:ss'
                ).format('DD/MM/YYYY'),
                email: responsavel.email,
                status: responsavel.status
              }
            })
          })
        } else {
          console.error(response.data.erro)
        }

        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)

        this.setState({ loading: false })
      })
  }

  componentDidCatch(error, info) {
    console.log(error, info)

    this.setState({
      responsaveis: []
    })
  }

  excluirResponsavel(id) {
    // Confirmação de exclusão
    Swal({
      title: 'Você tem certeza?',
      text: 'Você não poderá reverter isso!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, quero excluir o Responsável!'
    }).then(result => {
      // Se "result.value" for igual a "true" exclúi o responsável
      if (result.value) {
        axiosClient
          .post('/painel/responsavel/deletar', { id_responsavel: id })
          .then(response => {
            // Excluindo um registro da lista
            if (response.data.status)
              if (response.data.status) {
                this.setState(prevState => ({
                  responsaveis: prevState.responsaveis.filter(
                    responsavel => id !== responsavel.id_responsavel
                  )
                }))
                Swal('Excluído!', response.data.message, 'success')
              } else {
                Swal('Erro!', response.data.erro, 'error')
              }
          })
      }
    })
  }

  render() {
    // definindo colunas para a tabela
    const columns = [
      {
        Header: 'Código', // Cabeçalho da coluna
        accessor: 'id_responsavel', // String-based value accessors!
        filterable: false,
        width: 70
      },
      {
        Header: 'Nome',
        accessor: 'nome' // String-based value accessors!
      },
      {
        Header: 'Email',
        accessor: 'email' // String-based value accessors!
      },
      {
        Header: 'Data de Inclusão',
        accessor: 'data' // String-based value accessors!
      },
      {
        Header: 'Status',

        filterMethod: (filter, row) => {
          if(filter.value === "todos") return true
          
          return row._original.status === filter.value
        },
        Filter: ({ filter, onChange }) =>
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%" }}
            value={filter ? filter.value : "todos"}
          >
            <option value="todos">Todos</option>
            <option value="A">Ativo</option>
            <option value="I">Inativo</option>  
          </select>,
        width: 100,

        Cell: props => (
          <div
            className="d-flex justify-content-center align-items-center "
            style={{ width: 60, height: 30 }}
          >
            <span
              className={`d-flex justify-content-center align-items-center text-center badge badge-${
                props.original.status === 'A' ? 'success' : 'secondary'
              }`}
              style={{ width: 50, height: 30 }}
            >
              {props.original.status === 'A' ? 'Ativo' : 'Inativo'}
            </span>
          </div>
        ),
      },
      {
        Header: 'Ações',
        filterable: false,
        Cell: props => (
          <div className="d-flex justify-content-center align-items-center">
            <ButtonGroup size="sm">
              <Link
                to={`/admin/responsaveis/alterar/${props.row.id_responsavel}`}
                className="btn btn-warning text-white"
              >
                <i className="fas fa-pencil-alt" />
                <span className="d-none d-sm-inline">Editar</span>
              </Link>
              <button
                onClick={() =>
                  this.excluirResponsavel(props.row.id_responsavel)
                }
                className="btn btn-danger"
              >
                <i className="fas fa-times" />
                <span className="d-none d-sm-inline">Excluir</span>
              </button>
            </ButtonGroup>
          </div>
        )
      }
    ]
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card ">
              <div className="card-header ">
                <div className="row align-items-center justify-content-between">
                  <div>
                    <h4 className="card-title">Lista de Responsáveis</h4>
                    <p className="card-category">Responsáveis de Associações</p>
                  </div>
                  <Link
                    className="btn btn-success float-right"
                    to="/admin/responsaveis/cadastrar"
                  >
                    <i className="fas fa-plus-circle" />{' '}
                    <span className="d-none d-sm-inline">
                      Cadastrar Responsável
                    </span>
                  </Link>
                </div>
              </div>
              <div className="card-body">
                <ReactTable
                  columns={columns}
                  data={this.state.responsaveis}
                  loading={this.state.loading}
                  defaultPageSize={10}
                  filterable
                  defaultFilterMethod={filterCaseInsensitive}
                  className="-striped -highlight"
                  previousText="Anterior"
                  nextText="Próximo"
                  loadingText="Carregando..."
                  noDataText="Nenhum responsável foi encontrado"
                  pageText="Pág."
                  ofText="de"
                  rowsText="linhas"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ResponsavelPage
