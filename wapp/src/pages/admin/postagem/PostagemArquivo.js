import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import classNames from 'classnames'

import FormValidator from 'helpers/FormValidator'
import axiosClient from 'helpers/axiosClient'
import axios from 'axios'

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: 'red' }}>*</span>
  </label>
)

class PostagemArquivo extends Component {
  constructor(props) {
    super(props)

    this.validator = new FormValidator([
      {
        field: 'titulo',
        method: 'isEmpty',
        validWhen: false,
        message: 'O título é obrigatório.'
      }
    ])

    this.state = {
      id_postagem: '',
      titulo: '',
      modoEdit: false,
      validation: this.validator.valid(),
      submitted: false,
      redirect: false,
      loadingData: false,
      file: null
    }

    this.submitted = false
  }

  onFormSubmit = e => {
    e.preventDefault() // Stop form submit
    this.fileUpload(this.state.file).then(response => {
      console.log(response.data)
    })
  }

  onChangeFile = e => {
    this.setState({ file: e.target.files[0] })
  }

  fileUpload = file => {
    const url = 'http://example.com/file-upload'
    const formData = new FormData()
    formData.append('file', file)
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    return axios.post(url, formData, config)
  }

  getPostById = id => {
    axiosClient
      .get('/painel/postagem/listar/' + id)
      .then(response => {
        if (response.data.status) {
          let postagem = response.data.obj.postagem[0]
          this.setState({
            id_postagem: postagem.id_postagem || '',
            titulo: postagem.titulo,
            loadingData: false
          })
        } else {
          Swal({
            type: 'error',
            title: 'Oops...',
            html: response.data.erro
          })
        }
      })
      .catch(error => {
        console.error(error)
      })
  }

  handleChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  render() {
    const { loadingData } = this.state
    // let validation = this.submitted
    //   ? this.validator.validate(this.state)
    //   : this.state.validation
    // const inputClassNames = isInvalid => {
    //   return classNames('form-control', { 'is-invalid': isInvalid })
    // }

    if (this.state.redirect) return <Redirect to="/admin/postagem" />

    return (
      <div>
        <Link className="btn btn-secondary mb-2" to="/admin/postagem">
          <i className="fas fa-arrow-left" />
          Voltar a lista
        </Link>

        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Envíar arquivos</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>

        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-10">
              <div className="card">
                <div
                  className={classNames('card-body', {
                    'is-loading is-loading-lg': loadingData
                  })}
                >
                  <div className="form-row">
                    <div className="form-group col-md-12 ">
                      <div className="form-group">
                        <LabelRiquered>Título</LabelRiquered>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Titulo postagem"
                        />
                      </div>
                    </div>
                    <div className="form-group col-lg-6">
                      <div className="form-group">
                        <LabelRiquered>Selecionar arquivo</LabelRiquered>
                        <div className="custom-file">
                          <input
                            type="file"
                            className="custom-file-input"
                            id="customFile"
                            onChange={this.onChangeFile}
                          />
                          <label className="custom-file-label" htmlFor="customFile">
                            Escolha o arquivo{' '}
                          </label>
                        </div>
                        <p className="p-1">
                          Formatos suportados: JPG, PNG e PDF.
                        </p>
                      </div>
                    </div>
                  </div>

                  <button type="submit" className="btn btn-primary">
                    Envíar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default PostagemArquivo
