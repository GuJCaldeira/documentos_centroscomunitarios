import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import classNames from 'classnames'
// import MaskedInput from 'react-maskedinput'
import Select from 'react-select'
import CreatableSelect from 'react-select/lib/Creatable'
import { FormGroup, Label, Input } from 'reactstrap'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import { EditorState, convertToRaw, ContentState } from 'draft-js'
import htmlToDraft from 'html-to-draftjs'

import FormValidator from 'helpers/FormValidator'
import axiosClient from 'helpers/axiosClient'
import isEmpty from 'helpers/isEmpty'

const LabelRiquered = props => (
  <label {...props}>
    {props.children} <span style={{ color: 'red' }}>*</span>
  </label>
)
const components = {
  DropdownIndicator: null
}

const createOption = label => ({
  label,
  value: label
})

class PostagemForm extends Component {
  isSelectedCentroComunitario = (_data, state) =>
    isEmpty(state.centroComunitario)

  constructor(props) {
    super(props)

    this.validator = new FormValidator([
      {
        field: 'titulo',
        method: 'isEmpty',
        validWhen: false,
        message: 'O título é obrigatório.'
      },
      {
        field: 'conteudo',
        method: 'isEmpty',
        validWhen: false,
        message: 'O conteúdo é obrigatório.'
      },
      {
        field: 'centroComunitario',
        method: this.isSelectedCentroComunitario,
        validWhen: false,
        message: 'O centro comunitário é obrigatória.'
      }
    ])

    this.state = {
      id_postagem: '',
      titulo: '',
      destaque: false,
      menu: false,
      conteudo: EditorState.createEmpty(),
      centroComunitario: {},
      centrosComunitariosOptionsLoading: false,
      centrosComunitariosOptions: [],
      categorias: [],
      status: 'A',

      inputValue: '',
      modoEdit: false,
      validation: this.validator.valid(),
      submitted: false,
      redirect: false,
      loadingData: false
    }

    this.submitted = false
  }

  componentDidMount() {
    this.setState({
      loadingData: true,
      centroComunitarioOptionsLoading: true
    })
    this.getOptions()
    if (this.props.match.params && this.props.match.params.id) {
      const id = this.props.match.params.id
      this.setState({
        modoEdit: true
      })
      this.getPostById(id)
    } else {
      this.setState({
        loadingData: false
      })
    }
  }

  getPostById = id => {
    axiosClient
      .get('/painel/postagem/listar/' + id)
      .then(response => {
        if (response.data.status) {
          let postagem = response.data.obj.postagem[0]
          const contentBlock = htmlToDraft(postagem.conteudo)
          if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(
              contentBlock.contentBlocks
            )
            const editorState = EditorState.createWithContent(contentState)
            this.setState({
              conteudo: editorState
            })
          }

          this.setState({
            id_postagem: postagem.id_postagem || '',
            titulo: postagem.titulo,
            destaque: postagem.destaque,
            menu: postagem.menu,
            centroComunitario: postagem.centroComunitario,
            centrosComunitariosOptionsLoading: false,
            centrosComunitariosOptions: [],
            categorias: postagem.categorias,
            status: postagem.status,

            loadingData: false
          })
        } else {
          Swal({
            type: 'error',
            title: 'Oops...',
            html: response.data.erro
          })
        }
      })
      .catch(error => {
        console.error(error)
      })
  }

  handleChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  handleSubmit = e => {
    e.preventDefault()

    const validation = this.validator.validate(this.state)
    this.setState({ validation, submitted: true })
    this.submitted = true
    console.log(this.state)

    if (validation.isValid) {
      let {
        id_postagem,
        titulo,
        destaque,
        menu,
        conteudo,
        centroComunitario,
        categorias,
        status
      } = this.state

      if (this.props.match.params && this.props.match.params.id) {
        axiosClient
          .post('/painel/postagem/atualizar', {
            id_postagem,
            titulo,
            destaque,
            menu,
            conteudo: draftToHtml(convertToRaw(conteudo.getCurrentContent())),
            id_centro_comunitario: centroComunitario.value,
            categorias,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Oops...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      } else {
        axiosClient
          .post('/painel/postagem/inserir', {
            id_postagem,
            titulo,
            destaque,
            menu,
            conteudo: draftToHtml(convertToRaw(conteudo.getCurrentContent())),
            id_centro_comunitario: centroComunitario.value,
            categorias,
            status
          })
          .then(response => {
            if (response.data.status) {
              this.submitted = false
              this.setState({
                redirect: true
              })
              Swal({
                position: 'top-end',
                type: 'success',
                title: response.data.message,
                showConfirmButton: false,
                timer: 1500
              })
            } else {
              Swal({
                type: 'error',
                title: 'Oops...',
                html: response.data.erro
              })
            }
          })
          .catch(console.error)
      }
    } else {
      this.setState({ submitted: false })
    }
  }

  getOptions = async () => {
    this.setState({
      centrosComunitariosOptionsLoading: true
    })
    try {
      const centrosComunitariosResponse = await axiosClient.get(
        '/painel/centro_comunitario/listar'
      )

      const centrosComunitariosOptions = centrosComunitariosResponse.data.obj.centro_comunitario
        .map(centro_comunitario => {
          return {
            ...centro_comunitario,
            value: centro_comunitario.id_centro_comunitario,
            label: centro_comunitario.nome_centro
          }
        })
        .filter(associacao => associacao.status === 'A')

      this.setState({
        centrosComunitariosOptions,
        centrosComunitariosOptionsLoading: false
      })
    } catch (err) {
      console.log(err, "ups.. couldn't get options")
    }
  }

  handleChangeCreatable = (value, actionMeta) => {
    this.setState({ categorias: value })
  }

  handleInputChange = inputValue => {
    this.setState({ inputValue })
  }

  handleKeyDown = event => {
    const { inputValue } = this.state

    if (!inputValue) return
    switch (event.key) {
      case 'Enter':
      case 'Tab':
        this.setState(prevState => ({
          inputValue: '',
          categorias: [...prevState.categorias, createOption(inputValue)]
        }))
        event.preventDefault()
        break
      default:
    }
  }

  render() {
    const { loadingData, inputValue } = this.state
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation
    const inputClassNames = isInvalid => {
      return classNames('form-control', { 'is-invalid': isInvalid })
    }

    if (this.state.redirect) return <Redirect to="/admin/postagem" />

    return (
      <div>
        <Link className="btn btn-secondary mb-2" to="/admin/postagem">
          <i className="fas fa-arrow-left" />
          Voltar a lista
        </Link>

        <div className="d-flex justify-content-start align-items-center my-3">
          <h1 className="mb-0 mr-3">Formulário de Postagem</h1>
          {loadingData && <div className="text-muted">Carregando...</div>}
        </div>

        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-10">
              <div className="card">
                <div
                  className={classNames('card-body', {
                    'is-loading is-loading-lg': loadingData
                  })}
                >
                  <div className="form-group col-md-12 p-0">
                    <LabelRiquered>Título</LabelRiquered>
                    <input
                      type="text"
                      name="titulo"
                      placeholder="Informe o titulo"
                      className={inputClassNames(validation.titulo.isInvalid)}
                      value={this.state.titulo}
                      onChange={this.handleChange}
                    />
                    <div className="invalid-feedback">
                      {validation.titulo.message}
                    </div>
                  </div>
                  <div className="row col-12 form-group">
                    <FormGroup check inline>
                      <Label check>
                        <Input type="checkbox" /> Destaque
                      </Label>
                    </FormGroup>
                    <FormGroup check inline>
                      <Label check>
                        <Input type="checkbox" /> Menu
                      </Label>
                    </FormGroup>
                  </div>
                  <div className="form-group">
                    <LabelRiquered>Conteúdo</LabelRiquered>
                    <Editor
                      editorState={this.state.conteudo}
                      wrapperClassName="demo-wrapper"
                      editorClassName="form-control"
                      onEditorStateChange={conteudo =>
                        this.setState({ conteudo })
                      }
                      editorStyle={{ minHeight: '200px' }}
                    />
                  </div>

                  <hr />

                  <div className="form-group">
                    <LabelRiquered>Centro Comunitário</LabelRiquered>
                    <Select
                      className="basic-single"
                      classNamePrefix="select"
                      isMulti={false}
                      value={this.state.centroComunitario}
                      isLoading={this.state.centrosComunitariosOptionsLoading}
                      isDisabled={this.state.centrosComunitariosOptionsLoading}
                      isSearchable={true}
                      name="centroComunitario"
                      placeholder="Selecione..."
                      options={this.state.centrosComunitariosOptions}
                      onChange={selectedOption =>
                        this.setState({ centroComunitario: selectedOption })
                      }
                    />
                    <div className="text-danger">
                      {validation.centroComunitario.message}
                    </div>
                  </div>

                  <div className="form-group">
                    <Label>Categorias</Label>
                    <CreatableSelect
                      components={components}
                      inputValue={inputValue}
                      isClearable
                      isMulti
                      menuIsOpen={false}
                      onChange={this.handleChangeCreatable}
                      onInputChange={this.handleInputChange}
                      onKeyDown={this.handleKeyDown}
                      placeholder="Digite a categoria e pressione enter..."
                      value={this.state.categorias}
                    />
                  </div>

                  <div className="form-row">
                    <div className="form-group col-md-3">
                      <label>Status</label>
                      <select
                        name="status"
                        className={inputClassNames()}
                        onChange={this.handleChange}
                        value={this.state.status}
                      >
                        <option value="A">Ativo</option>
                        <option value="I">Inativo</option>
                      </select>
                    </div>
                  </div>
                  <button type="submit" className="btn btn-success">
                    Salvar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default PostagemForm
