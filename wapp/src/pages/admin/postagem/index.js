import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import moment from 'moment'
import ReactTable from 'react-table'
import { ButtonGroup } from 'reactstrap'

import axiosClient from 'helpers/axiosClient'

import filterCaseInsensitive from 'helpers/filterCaseInsensitive'

class PostagemPage extends Component {
  constructor(props) {
    super(props)

    // definindo o estado inicial das variáveis que serão usadas no componente
    this.state = {
      postagens: [],
      loading: false
    }
  }

  // após a renderização do componente
  componentDidMount() {
    this.setState({ loading: true })
    axiosClient
      .get('painel/postagem/listar')
      .then(response => {
        console.log(response)
        if (
          response.data.status &&
          response.data.obj &&
          Array.isArray(response.data.obj.postagem)
        ) {
          this.setState({
            postagens: response.data.obj.postagem.map(postagem => {
              return {
                id_postagem: postagem.id_postagem,
                descricao: postagem.descricao,
                titulo: postagem.titulo,
                imagem: postagem.imagem,
                id_centro_comunitario: postagem.id_centro_comunitario,
                detalhes: postagem.detalhes,
                data_criacao: moment(
                  postagem.data_criacao,
                  'YYYY-MM-DD HH:mm:ss'
                ).format('DD/MM/YYYY HH:mm'),
                email: postagem.email,
                status: postagem.status
              }
            })
          })
        } else {
          console.error(response.data.erro)
        }

        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)

        this.setState({ loading: false })
      })
  }

  componentDidCatch(error, info) {
    console.log(error, info)

    this.setState({
      postagens: []
    })
  }

  excluirPostagem = id => {
    // Confirmação de exclusão
    Swal({
      title: 'Você tem certeza?',
      text: 'Você não poderá reverter isso!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, quero excluir a Postagem!'
    }).then(result => {
      // Se "result.value" for igual a "true" exclúi a Postagem
      if (result.value) {
        axiosClient
          .post('/painel/postagem/deletar', { id_postagem: id })
          .then(response => {
            // Excluindo um registro da lista
            if (response.data.status) {
              this.setState(prevState => ({
                postagens: prevState.postagens.filter(
                  postagem => id !== postagem.id_postagem
                )
              }))
              Swal('Excluído!', response.data.message, 'success')
            } else {
              Swal('Erro!', response.data.erro, 'error')
            }
          })
      }
    })
  }

  render() {
    // definindo colunas para a tabela
    const columns = [
      {
        Header: 'Código', // Cabeçalho da coluna
        accessor: 'id_postagem', // String-based value accessors!
        filterable: false,
        width: 70
      },
      {
        Header: 'Título',
        accessor: 'titulo' // String-based value accessors!
      },
      // {
      //   Header: 'Descrição',
      //   accessor: 'descricao' // String-based value accessors!
      // },
      {
        Header: 'Data de Inclusão',
        accessor: 'data_criacao' // String-based value accessors!
      },
      {
        Header: 'Status',
        Cell: props => (
          <div
            className="d-flex justify-content-center align-items-center "
            style={{ width: 60, height: 30 }}
          >
            <span
              className={`d-flex justify-content-center align-items-center text-center badge badge-${
                props.original.status === 'A' ? 'success' : 'secondary'
              }`}
              style={{ width: 50, height: 30 }}
            >
              {props.original.status === 'A' ? 'Ativo' : 'Inativo'}
            </span>
          </div>
        ),
        
        filterMethod: (filter, row) => {
          if(filter.value === "todos") return true
          
          return row._original.status === filter.value
        },
        Filter: ({ filter, onChange }) =>
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%" }}
            value={filter ? filter.value : "todos"}
          >
            <option value="todos">Todos</option>
            <option value="A">Ativo</option>
            <option value="I">Inativo</option>  
          </select>,
        width: 100
        
      },
      {
        Header: 'Ações',
        filterable: false,
        Cell: props => (
          <div className="d-flex justify-content-center align-items-center">
            <ButtonGroup size="sm">
              <Link
                to={`/admin/postagem/alterar/${props.row.id_postagem}`}
                className="btn btn-warning text-white"
              >
                <i className="fas fa-pencil-alt mr-2" />
                <span className="d-none d-sm-inline">Editar</span>
              </Link>
              <button
                onClick={() => this.excluirPostagem(props.row.id_postagem)}
                className="btn btn-danger"
              >
                <i className="fas fa-times mr-2" />
                <span className="d-none d-sm-inline">Excluir</span>
              </button>
              <Link
                to={`/admin/postagem/upload/${props.row.id_postagem}`}
                className="btn btn-primary text-white"
              >
                <i className="fas fa-pencil-alt mr-2" />
                <span className="d-none d-sm-inline">Arquivo</span>
              </Link>
            </ButtonGroup>
          </div>
        )
      }
    ]
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card ">
              <div className="card-header ">
                <div className="row align-items-center justify-content-between">
                  <div>
                    <h4 className="card-title">Lista de Postagens</h4>
                    <p className="card-category">Postagens do portal</p>
                  </div>
                  <Link
                    className="btn btn-success float-right"
                    to="/admin/postagem/cadastrar"
                  >
                    <i className="fas fa-plus-circle" />{' '}
                    <span className="d-none d-sm-inline">
                      Cadastrar Postagem
                    </span>
                  </Link>
                </div>
              </div>
              <div className="card-body">
                <ReactTable
                  columns={columns}
                  data={this.state.postagens}
                  loading={this.state.loading}
                  defaultPageSize={10}
                  filterable
                  defaultFilterMethod={filterCaseInsensitive}
                  className="-striped -highlight"
                  previousText="Anterior"
                  nextText="Próximo"
                  loadingText="Carregando..."
                  noDataText="Nenhuma Postagem foi encontrada"
                  pageText="Pág."
                  ofText="de"
                  rowsText="linhas"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default PostagemPage
