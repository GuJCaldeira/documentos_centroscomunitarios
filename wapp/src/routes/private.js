import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import moment from 'moment'

const PrivateRoute = ({ component: Component, auth, ...props }) => (
  <Route
    {...props}
    render={renderProps =>
      !!auth.token && moment(auth.expire).isAfter(moment()) ? (
        <Component {...renderProps} />
      ) : (
        <Redirect to={{ pathname: '/login' }} />
      )
    }
  />
)

const mapStateToProps = state => ({
  auth: state.auth
})

export default connect(mapStateToProps)(PrivateRoute)
