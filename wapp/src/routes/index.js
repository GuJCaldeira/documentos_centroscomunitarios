import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import PrivateRoute from './private'
import PortalRoutes from '../pages/portal/routes'
import AdminRoutes from '../pages/admin/routes'
import LoginPage from '../pages/login'
import LocacaoPage from '../pages/portal/locacao'
import FaleconoscoPage from '../pages/portal/faleconosco'
import EventosPage from '../pages/portal/eventos'

class Routes extends Component {
  state = {}
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={PortalRoutes} />
          <PrivateRoute path="/admin" component={AdminRoutes} />
          <Route path="/login" component={LoginPage} />
          <Route path="/locacao" component={LocacaoPage} />
          <Route path="/faleconosco" component={FaleconoscoPage} />
          <Route path="/eventos" component={EventosPage} />
        </Switch>
      </Router>
    )
  }
}

export default Routes
