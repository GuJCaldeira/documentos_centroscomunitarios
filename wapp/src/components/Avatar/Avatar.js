import React, { Component } from 'react'
import classNames from 'classnames'
import CircularProgressbar from 'react-circular-progressbar'

import avatar from 'images/default-avatar.png'
import axiosClient from 'helpers/axiosClient'

import { Container, ButtonSelectFile, ImageAvatar, PieChart } from './styles'

export default class componentName extends Component {
  constructor(props) {
    super(props)

    this.state = {
      update: false,
      modal: false,
      progressUpload: null,
      visibleProgressUpload: false,
      userAvatar: null
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      userAvatar: nextProps.src
    }
  }

  handlerMouseEnter = e => {
    this.setState({
      update: true
    })
  }

  handlerMouseLeave = e => {
    this.setState({
      update: false
    })
  }

  updateProgress = percent => {
    this.setState({
      progressUpload: percent
    })
  }

  reloadAvatar = src => {
    this.setState({
      userAvatar: src
    })
  }

  fileSelectedHandler = event => {
    if (event.target.files[0]) {
      this.fileUploadHandler(event.target.files[0])
    }
  }

  fileUploadHandler = selectedFile => {
    const fd = new FormData()
    fd.append('avatar', selectedFile)

    let id = 1

    this.setState({
      visibleProgressUpload: true
    })

    axiosClient
      .put('v1/account/user/' + id, fd, {
        onUploadProgress: progressEvent => {
          let percent = Math.round(
            (progressEvent.loaded / progressEvent.total) * 100
          )

          this.updateProgress(percent)
        },
        headers: { 'Content-Type': 'multipart/form-data' }
      })
      .then(response => {
        let id = 1
        axiosClient
          .get('/v1/account/user/' + id)
          .then(response => {
            this.reloadAvatar(response.data.avatar)
            // profile.avatar = response.data.avatar
            // this.setState({
            //   visibleProgressUpload: false
            // })
          })
          .then(() => {
            this.setState({
              visibleProgressUpload: false
            })
          })
          .catch(error => {
            this.setState({
              visibleProgressUpload: false
            })
          })
      })
      .catch(error => {
        this.setState({
          visibleProgressUpload: false
        })
      })
  }

  render() {
    return (
      <div>
        <Container
          className={classNames('img-avatar', this.props.containerClassName)}
        >
          <PieChart>
            <CircularProgressbar
              className={classNames('progressbar-green', {
                'd-none': !this.state.visibleProgressUpload
              })}
              strokeWidth={4}
              textForPercentage={() => ''}
              percentage={this.state.progressUpload}
            />
          </PieChart>
          <ImageAvatar
            className={classNames(
              'img-avatar rounded-circle',
              this.props.className
            )}
            src={this.state.userAvatar || avatar}
            alt={this.props.name}
            onMouseEnter={this.handlerMouseEnter}
            onMouseLeave={this.handlerMouseLeave}
          />

          <input
            style={{ display: 'none' }}
            type="file"
            onChange={this.fileSelectedHandler}
            ref={fileInput => (this.fileInput = fileInput)}
          />

          <ButtonSelectFile
            type="button"
            className={classNames(
              'btn btn-update-avatar btn-primary rounded-circle animated',
              {
                'd-none': !this.state.update
              }
            )}
            onMouseEnter={this.handlerMouseEnter}
            onMouseLeave={this.handlerMouseLeave}
            onClick={() => this.fileInput.click()}
          >
            <i className="fas fa-pencil-alt" />
          </ButtonSelectFile>
        </Container>
      </div>
    )
  }
}
