import styled from 'styled-components'

export const Container = styled.div`
  position: relative;
  &:hover img {
    filter: blur(2px);
    button {
      display: block;
    }
  }
  &:active img {
    filter: blur(2px);
    button {
      display: block;
    }
  }
  .progressbar-green .CircularProgressbar-path {
    stroke: #27ae60;
  }
`

export const ButtonSelectFile = styled.button`
  position: absolute;
  bottom: -10px;
  right: 0px;
  /* padding: 5px 10px;
  font-size: 8px; */
`

export const ImageAvatar = styled.img`
  -webkit-transition: -webkit-filter 500ms linear;
  transition: filter 500ms linear;
  background: #f1f1f1;
`

export const PieChart = styled.div`
  position: absolute;
  top: -2px;
  width: 105%;
  height: 105%;
  left: -2px;
`
