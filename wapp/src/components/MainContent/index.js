import React, { Fragment } from 'react'
import { Store } from 'store'
import { Types } from 'store/ducks/auth'

const MainContent = ({ children }) => {
  const logout = () =>
    Store.dispatch({
      type: Types.LOGOUT
    })
  return (
    <Fragment>
      <div className="main-content">
        <nav className="navbar navbar-expand-md navbar-light d-none d-md-flex">
          <div className="container-fluid">
            {/* Brand */}
            {/*<a className="navbar-brand mr-auto" href="/admin">
              Painel de Controle
  </a>*/}
            {/* Form */}
            <form className="form-inline mr-4 d-none d-md-flex">
              <div
                className="input-group input-group-rounded input-group-merge"
                data-toggle="lists"
                data-lists-values="[&quot;name&quot;]"
              >
                {/* Input */}
                {/* <input
                  type="search"
                  className="form-control form-control-prepended  dropdown-toggle search"
                  data-toggle="dropdown"
                  placeholder="Search"
                  aria-label="Search"
                />
                <div className="input-group-prepend">
                  <div className="input-group-text">
                    <i className="fe fe-search" />
                  </div>
                </div> */}
                {/* Menu */}
                <div className="dropdown-menu dropdown-menu-card">
                  <div className="card-body">
                    {/* List group */}
                    <div className="list-group list-group-flush list my--3">
                      <a
                        href="team-overview.html"
                        className="list-group-item px-0"
                      >
                        <div className="row align-items-center">
                          <div className="col-auto">
                            {/* Avatar */}
                            <div className="avatar">
                              <img
                                src="assets/img/avatars/teams/team-logo-1.jpg"
                                alt="..."
                                className="avatar-img rounded"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            {/* Title */}
                            <h4 className="text-body mb-1 name">Airbnb</h4>
                            {/* Time */}
                            <p className="small text-muted mb-0">
                              <span className="fe fe-clock" />{' '}
                              <time dateTime="2018-05-24">Updated 2hr ago</time>
                            </p>
                          </div>
                        </div>{' '}
                        {/* / .row */}
                      </a>
                      <a
                        href="team-overview.html"
                        className="list-group-item px-0"
                      >
                        <div className="row align-items-center">
                          <div className="col-auto">
                            {/* Avatar */}
                            <div className="avatar">
                              <img
                                src="assets/img/avatars/teams/team-logo-2.jpg"
                                alt="..."
                                className="avatar-img rounded"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            {/* Title */}
                            <h4 className="text-body mb-1 name">
                              Medium Corporation
                            </h4>
                            {/* Time */}
                            <p className="small text-muted mb-0">
                              <span className="fe fe-clock" />{' '}
                              <time dateTime="2018-05-24">Updated 2hr ago</time>
                            </p>
                          </div>
                        </div>{' '}
                        {/* / .row */}
                      </a>
                      <a
                        href="project-overview.html"
                        className="list-group-item px-0"
                      >
                        <div className="row align-items-center">
                          <div className="col-auto">
                            {/* Avatar */}
                            <div className="avatar avatar-4by3">
                              <img
                                src="assets/img/avatars/projects/project-1.jpg"
                                alt="..."
                                className="avatar-img rounded"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            {/* Title */}
                            <h4 className="text-body mb-1 name">
                              Homepage Redesign
                            </h4>
                            {/* Time */}
                            <p className="small text-muted mb-0">
                              <span className="fe fe-clock" />{' '}
                              <time dateTime="2018-05-24">Updated 4hr ago</time>
                            </p>
                          </div>
                        </div>{' '}
                        {/* / .row */}
                      </a>
                      <a
                        href="project-overview.html"
                        className="list-group-item px-0"
                      >
                        <div className="row align-items-center">
                          <div className="col-auto">
                            {/* Avatar */}
                            <div className="avatar avatar-4by3">
                              <img
                                src="assets/img/avatars/projects/project-2.jpg"
                                alt="..."
                                className="avatar-img rounded"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            {/* Title */}
                            <h4 className="text-body mb-1 name">
                              Travels &amp; Time
                            </h4>
                            {/* Time */}
                            <p className="small text-muted mb-0">
                              <span className="fe fe-clock" />{' '}
                              <time dateTime="2018-05-24">Updated 4hr ago</time>
                            </p>
                          </div>
                        </div>{' '}
                        {/* / .row */}
                      </a>
                      <a
                        href="project-overview.html"
                        className="list-group-item px-0"
                      >
                        <div className="row align-items-center">
                          <div className="col-auto">
                            {/* Avatar */}
                            <div className="avatar avatar-4by3">
                              <img
                                src="assets/img/avatars/projects/project-3.jpg"
                                alt="..."
                                className="avatar-img rounded"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            {/* Title */}
                            <h4 className="text-body mb-1 name">
                              Safari Exploration
                            </h4>
                            {/* Time */}
                            <p className="small text-muted mb-0">
                              <span className="fe fe-clock" />{' '}
                              <time dateTime="2018-05-24">Updated 4hr ago</time>
                            </p>
                          </div>
                        </div>{' '}
                        {/* / .row */}
                      </a>
                      <a
                        href="profile-posts.html"
                        className="list-group-item px-0"
                      >
                        <div className="row align-items-center">
                          <div className="col-auto">
                            {/* Avatar */}
                            <div className="avatar">
                              <img
                                src="assets/img/avatars/profiles/avatar-1.jpg"
                                alt="..."
                                className="avatar-img rounded-circle"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            {/* Title */}
                            <h4 className="text-body mb-1 name">
                              Dianna Smiley
                            </h4>
                            {/* Status */}
                            <p className="text-body small mb-0">
                              <span className="text-success">●</span> Online
                            </p>
                          </div>
                        </div>{' '}
                        {/* / .row */}
                      </a>
                      <a
                        href="profile-posts.html"
                        className="list-group-item px-0"
                      >
                        <div className="row align-items-center">
                          <div className="col-auto">
                            {/* Avatar */}
                            <div className="avatar">
                              <img
                                src="assets/img/avatars/profiles/avatar-2.jpg"
                                alt="..."
                                className="avatar-img rounded-circle"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            {/* Title */}
                            <h4 className="text-body mb-1 name">Ab Hadley</h4>
                            {/* Status */}
                            <p className="text-body small mb-0">
                              <span className="text-danger">●</span> Offline
                            </p>
                          </div>
                        </div>{' '}
                        {/* / .row */}
                      </a>
                    </div>
                  </div>
                </div>{' '}
                {/* / .dropdown-menu */}
              </div>
            </form>
            {/* User */}
            <div className="navbar-user">
              {/* Dropdown */}
              {/* <div className="dropdown mr-4 d-none d-md-flex">
                <a
                  href="/admin"
                  className="text-muted"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <span className="icon active">
                    <i className="fas fa-bell" />
                  </span>
                </a>
                <div className="dropdown-menu dropdown-menu-right dropdown-menu-card">
                  <div className="card-header">
                    <div className="row align-items-center">
                      <div className="col">
                        <h5 className="card-header-title">Notifications</h5>
                      </div>
                      <div className="col-auto">
                        <a href="#!" className="small">
                          View all
                        </a>
                      </div>
                    </div>{' '}
                  </div>{' '}
                  <div className="card-body">

                    <div className="list-group list-group-flush my--3">
                      <a className="list-group-item px-0" href="#!">
                        <div className="row">
                          <div className="col-auto">

                            <div className="avatar avatar-sm">
                              <img
                                src="assets/img/avatars/profiles/avatar-1.jpg"
                                alt="..."
                                className="avatar-img rounded-circle"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            <div className="small text-muted">
                              <strong className="text-body">
                                Dianna Smiley
                              </strong>{' '}
                              shared your post with{' '}
                              <strong className="text-body">Ab Hadley</strong>,{' '}
                              <strong className="text-body">Adolfo Hess</strong>
                              , and{' '}
                              <strong className="text-body">3 others</strong>.
                            </div>
                          </div>
                          <div className="col-auto">
                            <small className="text-muted">2m</small>
                          </div>
                        </div>{' '}
                      </a>
                      <a className="list-group-item px-0" href="#!">
                        <div className="row">
                          <div className="col-auto">
                            <div className="avatar avatar-sm">
                              <img
                                src="assets/img/avatars/profiles/avatar-2.jpg"
                                alt="..."
                                className="avatar-img rounded-circle"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            <div className="small text-muted">
                              <strong className="text-body">Ab Hadley</strong>{' '}
                              reacted to your post with a 😍
                            </div>
                          </div>
                          <div className="col-auto">
                            <small className="text-muted">2m</small>
                          </div>
                        </div>{' '}
                      </a>
                      <a className="list-group-item px-0" href="#!">
                        <div className="row">
                          <div className="col-auto">
                            <div className="avatar avatar-sm">
                              <img
                                src="assets/img/avatars/profiles/avatar-3.jpg"
                                alt="..."
                                className="avatar-img rounded-circle"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            <div className="small text-muted">
                              <strong className="text-body">Adolfo Hess</strong>{' '}
                              commented{' '}
                              <blockquote className="text-body">
                                “I don’t think this really makes sense to do
                                without approval from Johnathan since he’s the
                                one...”{' '}
                              </blockquote>
                            </div>
                          </div>
                          <div className="col-auto">
                            <small className="text-muted">2m</small>
                          </div>
                        </div>{' '}
                      </a>
                      <a className="list-group-item px-0" href="#!">
                        <div className="row">
                          <div className="col-auto">
                            <div className="avatar avatar-sm">
                              <img
                                src="assets/img/avatars/profiles/avatar-4.jpg"
                                alt="..."
                                className="avatar-img rounded-circle"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            <div className="small text-muted">
                              <strong className="text-body">
                                Daniela Dewitt
                              </strong>{' '}
                              subscribed to you.
                            </div>
                          </div>
                          <div className="col-auto">
                            <small className="text-muted">2m</small>
                          </div>
                        </div>{' '}
                      </a>
                      <a className="list-group-item px-0" href="#!">
                        <div className="row">
                          <div className="col-auto">
                            <div className="avatar avatar-sm">
                              <img
                                src="assets/img/avatars/profiles/avatar-5.jpg"
                                alt="..."
                                className="avatar-img rounded-circle"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            <div className="small text-muted">
                              <strong className="text-body">Miyah Myles</strong>{' '}
                              shared your post with{' '}
                              <strong className="text-body">Ryu Duke</strong>,{' '}
                              <strong className="text-body">Glen Rouse</strong>,
                              and{' '}
                              <strong className="text-body">3 others</strong>.
                            </div>
                          </div>
                          <div className="col-auto">
                            <small className="text-muted">2m</small>
                          </div>
                        </div>{' '}
                      </a>
                      <a className="list-group-item px-0" href="#!">
                        <div className="row">
                          <div className="col-auto">
                            <div className="avatar avatar-sm">
                              <img
                                src="assets/img/avatars/profiles/avatar-6.jpg"
                                alt="..."
                                className="avatar-img rounded-circle"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            <div className="small text-muted">
                              <strong className="text-body">Ryu Duke</strong>{' '}
                              reacted to your post with a 😍
                            </div>
                          </div>
                          <div className="col-auto">
                            <small className="text-muted">2m</small>
                          </div>
                        </div>{' '}
                      </a>
                      <a className="list-group-item px-0" href="#!">
                        <div className="row">
                          <div className="col-auto">
                            <div className="avatar avatar-sm">
                              <img
                                src="assets/img/avatars/profiles/avatar-7.jpg"
                                alt="..."
                                className="avatar-img rounded-circle"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            <div className="small text-muted">
                              <strong className="text-body">Glen Rouse</strong>{' '}
                              commented{' '}
                              <blockquote className="text-body">
                                “I don’t think this really makes sense to do
                                without approval from Johnathan since he’s the
                                one...”{' '}
                              </blockquote>
                            </div>
                          </div>
                          <div className="col-auto">
                            <small className="text-muted">2m</small>
                          </div>
                        </div>{' '}
                      </a>
                      <a className="list-group-item px-0" href="#!">
                        <div className="row">
                          <div className="col-auto">
                            <div className="avatar avatar-sm">
                              <img
                                src="assets/img/avatars/profiles/avatar-8.jpg"
                                alt="..."
                                className="avatar-img rounded-circle"
                              />
                            </div>
                          </div>
                          <div className="col ml--2">
                            <div className="small text-muted">
                              <strong className="text-body">Grace Gross</strong>{' '}
                              subscribed to you.
                            </div>
                          </div>
                          <div className="col-auto">
                            <small className="text-muted">2m</small>
                          </div>
                        </div>{' '}
                      </a>
                    </div>
                  </div>
              </div> */}
              {/* Dropdown */}
              <div className="dropdown">
                {/* Toggle */}
                {/* <img
                    src="assets/img/avatars/profiles/avatar-1.jpg"
                    alt="..."
                    className="avatar-img rounded-circle"
                  /> */}
                {/* Menu */}
                <div className="menu-right">
                  {/* <a href="/admin" className="dropdown-item">
                    Profile
                  </a>
                  <a href="/admin" className="dropdown-item">
                    Settings
                  </a> */}
                  {/* <hr className="dropdown-divider" /> */}
                  <button
                    type="button"
                    onClick={() => logout()}
                    className="btn btn-light"
                  >
                    Sair
                  </button>
                </div>
              </div>
            </div>
          </div>{' '}
          {/* / .container-fluid */}
        </nav>
        <div className="container-fluid mt-5">{children}</div>
      </div>
    </Fragment>
  )
}
export default MainContent
