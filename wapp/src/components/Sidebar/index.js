import React from 'react'
import { NavLink } from 'react-router-dom'
import Logo from 'images/brasao_de_piracicaba.svg'

import { Store } from 'store'
import { Types } from 'store/ducks/auth'

const Sidebar = () => {
  const logout = () =>
    Store.dispatch({
      type: Types.LOGOUT
    })

  return (
    <nav className="navbar navbar-vertical fixed-left navbar-expand-md navbar-light">
      <div className="container-fluid">
        {/* <!-- Toggler --> */}
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#sidebarCollapse"
          aria-controls="sidebarCollapse"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>

        {/* <!-- Brand --> */}
        <a className="navbar-brand" href="/admin">
          <div className="d-flex flex-column">
            <img
              src={Logo}
              className="navbar-brand-img mx-auto mb-1"
              alt="Logo Piracicaba"
            />
            Prefeitura de Piracicaba
          </div>
        </a>

        {/* <!-- User (xs) --> */}
        <div className="navbar-user d-md-none">
          {/* <!-- Dropdown --> */}
          <div className="dropdown">
            {/* <!-- Toggle --> */}
            <a
              href="#!"
              id="sidebarIcon"
              className="btn btn-light dropdown-toggle"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              {/* <div className="avatar avatar-sm avatar-online">
                <img
                  src="assets/img/avatars/profiles/avatar-1.jpg"
                  className="avatar-img rounded-circle"
                  alt="..."
                />
              </div> */}
              <i className="fas fa-angle-down" />
            </a>

            {/* <!-- Menu --> */}
            <div
              className="dropdown-menu dropdown-menu-right"
              aria-labelledby="sidebarIcon"
            >
              {/* <a href="/admin" className="dropdown-item">
                Profile
              </a>
              <a href="/admin" className="dropdown-item">
                Settings
              </a> */}
              {/* <hr className="dropdown-divider" /> */}
              <button
                type="button"
                onClick={() => logout()}
                className="btn dropdown-item"
              >
                Sair
              </button>
            </div>
          </div>
        </div>

        {/* <!-- Collapse --> */}
        <div className="collapse navbar-collapse" id="sidebarCollapse">
          {/* <!-- Form --> */}
          {/* <form className="mt-4 mb-3 d-md-none">
            <div className="input-group input-group-rounded input-group-merge">
              <input
                type="search"
                className="form-control form-control-rounded form-control-prepended"
                placeholder="Search"
                aria-label="Search"
              />
              <div className="input-group-prepend">
                <div className="input-group-text">
                  <span className="fe fe-search" />
                </div>
              </div>
            </div>
          </form> */}

          {/* <!-- Navigation --> */}
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                exact
                to="/admin"
              >
                <i className="fas fa-home mr-2" /> Início
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/usuarios"
              >
                <i className="fas fa-user mr-2" /> Usuários
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/associacoes"
              >
                <i className="fas fa-users mr-2" /> Associações
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/centros-comunitarios"
              >
                <i className="fas fa-building mr-2" /> Centros Comunitários
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/conta"
              >
                <i className="fas fa-briefcase mr-2" /> Grupos de Contas
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/caixa"
              >
                <i className="fas fa-file-invoice-dollar mr-2" /> Fluxo de Caixa
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/responsaveis"
              >
                <i className="fas fa-user-tie mr-2" /> Responsáveis
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/eventos"
              >
                <i className="fas fa-calendar-check mr-2" /> Eventos
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/cargos"
              >
                <i className="fas fa-id-card mr-2" /> Cargos
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/locacao"
              >
                <i className="fas fa-handshake mr-2" /> Locação
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/postagem"
              >
                <i className="fas fa-newspaper mr-2" /> Postagem
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/documentos"
              >
                <i className="fas fa-folder mr-2" /> Documentos
              </NavLink>
            </li>
          </ul>

          {/* <!-- Divider --> */}
          <hr className="my-3" />

          {/* <!-- Heading --> */}
          <h6 className="navbar-heading text-muted">Configuração</h6>

          {/* <!-- Navigation --> */}
          <ul className="navbar-nav mb-md-3">
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin/grupos-de-permissoes"
              >
                <i className="fas fa-lock mr-2" /> Grupos de Permissões
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Sidebar
