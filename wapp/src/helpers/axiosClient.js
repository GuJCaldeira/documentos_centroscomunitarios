import React from 'react'
import axios from 'axios'
import qs from 'qs'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const SwalRC = withReactContent(Swal)

// Instância usada para aplicar as configurações padrões
const axiosClient = axios.create({
  //baseURL: 'http://centroscomunitarios.tst.pmp.sp.gov.br/ws/index.php/', // url_default
  baseURL: 'http://localhost:8090/unimep-varejao/ws/index.php/', // url_default
  headers: { 'content-type': 'application/x-www-form-urlencoded' }, // content-type accept for CodeIgniter
  transformRequest: [
    function(data, headers) {
      // transform to QueryString before request
      return qs.stringify(data)
    }
  ]
})

// Middleware usado para capturar erro dentro do response
// quando header.status for OK
axiosClient.interceptors.response.use(
  response => {
    const statusCode = response.data.status
    // Verificando código se houver error
    if (!statusCode) {
      console.log(response)
      switch (response.data.code) {
        case 401:
          SwalRC.fire({
            type: 'error',
            title: response.data.erro,
            html: 'É necessário fazer o login novamente.',
            footer: (
              <a className="btn btn-primary btn-sm" href="/login">
                Fazer Login
              </a>
            ),
            allowOutsideClick: false,
            showConfirmButton: false
          })
          break
        case 403:
          SwalRC.fire({
            type: 'error',
            title: response.data.erro,
            html: 'Você não tem permissão para acessar este módulo!',
            allowOutsideClick: true
          })
          break

        default:
          break
      }
    }
    return response
  },
  error => {
    return error
  }
)

// Instância usada para aplicar as configurações padrões
export const axiosSite = axios.create({
  //baseURL: 'http://centroscomunitarios.tst.pmp.sp.gov.br/ws/index.php/site/', // url_default
  baseURL: 'http://localhost:8090/unimep-varejao/ws/index.php/', // url_default
  headers: { 'content-type': 'application/x-www-form-urlencoded' }, // content-type accept for CodeIgniter
  transformRequest: [
    function(data, headers) {
      // transform to QueryString before request
      return qs.stringify(data)
    }
  ]
})

export default axiosClient
