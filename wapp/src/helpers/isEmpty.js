function isEmpty(obj) {
  return Object.keys(obj).every(k => !Object.keys(obj[k]).length)
}

export default isEmpty
