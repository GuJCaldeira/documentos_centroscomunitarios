<?php

class Authsession extends CI_Model {
    public function __construct() {
		parent::__construct();
		
		if (!function_exists('getallheaders')) {
			function getallheaders() {
				$headers = [];
				foreach ($_SERVER as $name => $value) {
					if (substr($name, 0, 5) == 'HTTP_') {
						$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
					}
				}
				return $headers;
			}
		}
		
        $header = getallheaders();
        $auth = $header['authorization'];
        if(strlen($auth)<12)
            $auth = $header['Authorization'];
        
        $this->token = str_replace(array("Bearer", " "), "", $auth);
    }

    public function valida($modulo, $permissao){
        $id_usuario = $this->get_item('id_usuario');
        if($id_usuario==false){
            $rps = array(
                'status' => false,
				'erro' => 'Sessão inválida.',
				'code' => 401
            );
            die(json_encode($rps));
        } else {
            $permissoes = $this->permissoes($id_usuario);

            if(is_object($permissoes))
                $permissoes = json_decode(json_encode($permissoes), true);

            if($modulo=='comum') return true;

            $acesso = $permissoes[$modulo][$permissao];
            if($acesso=='false' && !is_bool($acesso)) $acesso = false;
            if($acesso=='true' && !is_bool($acesso)) $acesso = true;

            //Se ID usuário < 0 = permissões total.
            if($acesso==false && $id_usuario>0){
                $rps = array(
                    'status' => false,
					'erro' => 'Usuário não tem esta permissão neste módulo.',
					'code' => 403
                );
                die(json_encode($rps));
            } else {
                return true;
            }
        }
    }

    //Manipulando dados da sessão.
    public function get_item($item){
        $sessao = $this->get_session();

        if($sessao!=false){
            $data = json_decode($sessao->data);
            return $data->$item;
        } else {
            return false;
        }
    }
    public function set_item($item){

    }

    //Manipulando as sessões
    public function destroy(){
        $sessoes = $this->sessao($this->token);
        if(count($sessoes)==1){
            $sessao = $sessoes[0];
            $this->deletar($sessao->id);
            return $sessao;
        } else {
            return false;
        }


    }
    public function create($data=array()){
        $parar = false;

        while($parar==false && $count<15){
            $id = $this->utils->geraSenha(15, false, true, false, true);
            $sessao = $this->sessao($id);
            if(count($sessao)==0){
                $parar = true;
            }
            $count++;
        }

        if($count>=15)
            die("Erro fatal, não foi possível gerar um ID de sessão válido");

        $novaSessao = array(
            'id' => $id,
            'ip_address' => $this->utils->getIp(),
            'expiration_timestamp' => time()+(60*60*48), //duração 48h
            'data' => json_encode($data)
        );
        $idNovaSessao = $this->inserir($novaSessao);
        return array('hash' => $id, 'expiration_timestamp' => $novaSessao['expiration_timestamp']);
    }
    public function get_session(){
        $sessoes = $this->sessao($this->token);
        if(count($sessoes)==1){
            return $sessoes[0];
        } else {
            return false;
        }
    }

    //Módulos disponíveis
    public function permissoesDisponiveis(){
        $modulos = array(
            array("id" => "associacao", "nome" => "Associações", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar"))),
            array("id" => "cargo", "nome" => "Cargos", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar"))),
            array("id" => "centro_comunitario", "nome" => "Centros comunitários", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar"))),
            array("id" => "denuncia", "nome" => "Denúncias", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar"))),
            array("id" => "evento", "nome" => "Eventos", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar"))),
            //array("id" => "gestao", "nome" => "Associações"),
            array("id" => "postagem", "nome" => "Postagens", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar"))),
            array("id" => "responsavel", "nome" => "Responsáveis", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar"))),
            array("id" => "usuario", "nome" => "Usuários", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar"))),
            array("id" => "usuario_grupo", "nome" => "Grupos de usuário", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar"))),
            array("id" => "caixa", "nome" => "Caixa", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar"))),
            array("id" => "locacao", "nome" => "Locação", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar"))),
            array("id" => "conta", "nome" => "Contas", "permissoes" => array(array("id" => "read", "nome" => "Leitura"), array("id" => "write", "nome" => "Salvar"), array("id" => "update", "nome" => "Atualizar"), array("id" => "delete", "nome" => "Deletar")))
        );

        return $modulos;
    }

    //Manipular permissões do usuário
    public function permissoes_old($id_usuario){
        $array = array();
        $perm = $this->permissoesDisponiveis();
        foreach ($perm as $p) {
            //$array[$p['id']] = array("read"=> true, "write"=>true, "update"=>true, "delete"=>true);
            $array[$p['id']] = array("read"=> true, "write"=>true, "update"=>true, "delete"=>true);
        }
        return $array;
    }    

    public function permissoes($id_usuario){
        $this->load->model("grupomodel");
        $this->load->model("usuariomodel");

        $usuario = $this->usuariomodel->buscar(array('id_usuario' => $id_usuario))[0];

        $id_grupo = $usuario->id_grupo;
        $grupo = $this->grupomodel->listar(null, array('id_grupo' => $id_grupo))[0];
        $array = json_decode($grupo->json);

        return $array;
    }


    /* Banco de dados das sessões */
    private function inserir($data){
        $this->db->set('timestamp', time());
        $this->db->insert('auth_sessions', $data); 
        $id = $this->db->insert_id();
        return $id;
    }

    private function sessao($id){
        $this->db->from('auth_sessions');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    private function atualizar($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('auth_sessions', $data);
    }

    private function deletar($id){
        $this->db->where('id', $id);
        $this->db->delete('auth_sessions');
    }

}
