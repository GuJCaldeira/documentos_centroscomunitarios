<?php

class Associacaomodel extends CI_Model {
    public function __construct() {
        parent::__construct();

    }

    public function inserir($data){
        $this->db->set('data_criacao', 'NOW()', FALSE);
        $this->db->insert('associacao', $data); 
        $id = $this->db->insert_id();
        return $id;
    }

    public function buscar($array=array()){
        $this->db->from('associacao');
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function listar($id=null, $array=array()){
        $this->db->from('associacao');
        $this->db->select("associacao.*, centro_comunitario.nome_centro");
        if(!is_null($id)) $this->db->where('id_associacao', $id);
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where('associacao.'.$item, $val);
                }
            } else {
                $this->db->where('associacao.'.$item, $value);
            }
        }
        $this->db->join('centro_comunitario', 'centro_comunitario.id_centro_comunitario = associacao.id_centro_comunitario', 'left');
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function listarcentro($id=null, $array=array()){
        $this->db->from('associacao');
        if(!is_null($id)) $this->db->where('id_centro_comunitario',$id);
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }


    public function atualizar($id, $data) {
        $this->db->set('data_alteracao', 'NOW()', FALSE);
        $this->db->where('id_associacao', $id);
        $this->db->update('associacao', $data);
    }

    public function deletar($id){
        $this->db->where('id_associacao', $id);
        $this->db->delete('associacao');
    }

}