<?php

class Usuariomodel extends CI_Model {
    public function __construct() {
        parent::__construct();

    }

    public function inserir($data){
        $this->db->set('data_criacao', 'NOW()', FALSE);
        $this->db->insert('usuario', $data); 
        $id = $this->db->insert_id();
        return $id;
    }

    public function buscar($array=array()){
        $this->db->from('usuario');
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }


public function listar($id=null, $array=array()){
        $this->db->from('usuario');
        $this->db->select("usuario.*, associacao.nome_fantasia");
        if(!is_null($id)) $this->db->where('id_usuario',$id);
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where('usuario.'.$item, $val);
                }
            } else {
                $this->db->where('usuario.'.$item, $value);
            }
        }
        $this->db->join('associacao', 'associacao.id_associacao = usuario.id_associacao', 'left');
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }


    public function listarassociao($id=null, $array=array()){
        $this->db->from('associacao');
        if(!is_null($id)) $this->db->where('id_associacao',$id);
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function atualizar($id, $data) {
        $this->db->set('data_alteracao', 'NOW()', FALSE);
        $this->db->where('id_usuario', $id);
        $this->db->update('usuario', $data);
    }

    public function deletar($id){
        $this->db->where('id_usuario', $id);
        $this->db->delete('usuario');
    }

}