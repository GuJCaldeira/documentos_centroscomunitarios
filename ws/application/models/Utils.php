<?php

class Utils extends CI_Model {
  public function __construct() {
      parent::__construct();
  }

  public function post($campo=null, $tratar=true){
    $postdata = file_get_contents("php://input");
      if (isset($postdata)) {
        $post = (object) json_decode($postdata);
      } else {
        $post = (object) $this->input->post();
      }


      if($campo==null and $tratar==true){
        foreach ($post as $chave => $valor) {
          $novoPost[$chave] = $this->security->xss_clean($valor);
        }
        return (object) $novoPost;
      }

      if($campo==null and $tratar==false){
        return (object) $post;
      }
      

      if($tratar) {
      return $this->security->xss_clean($post->$campo);
    } else {
      return $post->$campo;
    }
  }
  


  public function getIp() {
      $ipaddress = '';
      if (isset($_SERVER['HTTP_CLIENT_IP']))
          $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
      else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
          $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
      else if(isset($_SERVER['HTTP_X_FORWARDED']))
          $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
      else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
          $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
      else if(isset($_SERVER['HTTP_FORWARDED']))
          $ipaddress = $_SERVER['HTTP_FORWARDED'];
      else if(isset($_SERVER['REMOTE_ADDR']))
          $ipaddress = $_SERVER['REMOTE_ADDR'];
      else
          $ipaddress = 'UNKNOWN';

      return $ipaddress;
  }
  public function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false, $minusculas=true) {
      $lmin = 'abcdefghijklmnopqrstuvwxyz';
      $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $num = '1234567890';
      $simb = '!@#$%*-';
      //$simb = '';
      $retorno = '';
      $caracteres = '';
      if ($minusculas) $caracteres .= $lmin;
      if ($maiusculas) $caracteres .= $lmai;
      if ($numeros) $caracteres .= $num;
      if ($simbolos) $caracteres .= $simb;
      $len = strlen($caracteres);
      for ($n = 1; $n <= $tamanho; $n++) {
      $rand = mt_rand(1, $len);
      $retorno .= $caracteres[$rand-1];
      }
      return $retorno;
  }

}