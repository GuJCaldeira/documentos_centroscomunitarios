<?php
//Este é o MODEL do Fluxo de Caixa
class Caixamodel extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function inserir($data){
		$this->db->set('data_criacao','NOW()',FALSE);
		$this->db->insert('caixa',$data);
		$id = $this->db->insert_id();
		return $id;
	}


	public function listar($id=null, $array=array()){
		$this->db->from('caixa');
        $this->db->select("caixa.*, conta.nome_conta");
		if(!is_null($id)) $this->db->where('id_caixa',$id);
		foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where('caixa'.$item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
		$this->db->join('conta', 'conta.id_conta = caixa.id_conta', 'left');
		$this->db->order_by("caixa.data_lancamento", "asc");
		$query = $this->db->get();
		$res = $query->result();
		//echo $this->db->last_query();
		return $res;
	}
	public function listarconta($id=null, $array=array()){
		$this->db->from('caixa');
		if(!is_null($id)) $this->db->where('id_conta',$id);
		foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
		$query = $this->db->get();
		$res = $query->result();
		return $res;
	}

	//Replicar modelo abaixo em todos os models, inclusive este:
	/*
	public function listar($id=null, $array=array()){
		$this->db->from('caixa');
		if(!is_null($id)) $this->db->where('id_caixa',$id);
		foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
		$query = $this->db->get();
		$res = $query->result();
		return $res;
	}
	*/

	public function atualizar($id,$data){
		$this->db->set('data_alteracao','NOW()',FALSE);
		$this->db->where('id_caixa',$id);
		$this->db->update('caixa',$data);
	}

	public function deletar($id){
		$this->db->where('id_caixa', $id);
		$this->db->delete('caixa');
	}
}



?>