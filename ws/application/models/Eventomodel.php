<?php

class Eventomodel extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function inserir($data){
        $this->db->set('data_criacao', 'NOW()', FALSE);
        $this->db->insert('evento', $data); 
        $id = $this->db->insert_id();
        return $id;
    }

    public function buscar($array=array()){
        $this->db->from('evento');
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    
    public function listar($id=null, $array=array()){
        $this->db->from('evento');
        $this->db->select("evento.*, centro_comunitario.nome_centro");
        if(!is_null($id)) $this->db->where('id_evento', $id);
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where('evento.'.$item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
        $this->db->join('centro_comunitario', 'centro_comunitario.id_centro_comunitario = evento.id_centro_comunitario', 'left');
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function atualizar($id, $data) {
        $this->db->set('data_alteracao', 'NOW()', FALSE);
        $this->db->where('id_evento', $id);
        $this->db->update('evento', $data);
    }

    public function deletar($id){
        $this->db->where('id_evento', $id);
        $this->db->delete('evento');
    }

}