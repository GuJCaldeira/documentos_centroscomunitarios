<?php

class Cargomodel extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function inserir($data){
		$this->db->set('data_criacao','NOW()',FALSE);
		$this->db->insert('cargo',$data);
		$id = $this->db->insert_id();
		return $id;
	}


	public function listar($id=null, $array=array()){
		$this->db->from('cargo');
		if(!is_null($id)) $this->db->where('id_cargo',$id);
		foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
		$query = $this->db->get();
		$res = $query->result();
		return $res;
	}

	public function atualizar($id,$data){
		$this->db->set('data_alteracao','NOW()',FALSE);
		$this->db->where('id_cargo',$id);
		$this->db->update('cargo',$data);
	}

	public function deletar($id){
		$this->db->where('id_cargo', $id);
		$this->db->delete('cargo');
	}
}



?>