<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailmodel extends CI_Model {
    public function __construct() {
        parent::__construct();

    }   

    
    public function envia($data){
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.piracicaba.sp.gov.br',
            'smtp_port' => 587,
            'smtp_crypto' => 'tls',
            'smtp_user' => 'centroscomunitarios@piracicaba.sp.gov.br',
            'smtp_pass' => 'SENHA#0!.1',
            'mailtype'  => 'html', 
            'charset'   => 'utf-8'
        );


        $this->load->library('email');

        $this->email->clear(TRUE);

        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        $this->email->from($config['smtp_user'],'Sistema - CentrosComunitários');
        if(is_array($data['destinatario'])){
            $juntor = "";
            foreach ($data['destinatario'] as $chave) {
                $dest .= $juntor.$chave;
                $juntor = ",";
            }
            $this->email->to($dest);
            //echo $dest;
        } else {
            $this->email->to($data['destinatario']);
        }
        $this->email->subject($data['assunto']);
        $this->email->message($data['corpo']);
        if($data['anexo'])
            $this->email->attach($data['anexo']);
        if($data['anexoA']) {
            foreach($data['anexoA'] as $anexo) {
                $this->email->attach($anexo);
            }
        }
        $retorno =  $this->email->send();
        
        //$lucas = $this->email->print_debugger();
        //var_dump($lucas);

        return $retorno;
    }

}