<?php

class Centro_comunitariomodel extends CI_Model {
    public function __construct() {
        parent::__construct();

    }

    public function inserir($data){
        $this->db->set('data_criacao', 'NOW()', FALSE);
        $this->db->insert('centro_comunitario', $data); 
        $id = $this->db->insert_id();
        return $id;
    }

    public function buscar($array=array()){
        $this->db->from('centro_comunitario');
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    
    public function listar($id=null, $array=array()){
        $this->db->from('centro_comunitario');
        if(!is_null($id)) $this->db->where('id_centro_comunitario', $id);
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function atualizar($id, $data) {
        $this->db->set('data_alteracao', 'NOW()', FALSE);
        $this->db->where('id_centro_comunitario', $id);
        $this->db->update('centro_comunitario', $data);
    }

    public function deletar($id){
        $this->db->where('id_centro_comunitario', $id);
        $this->db->delete('centro_comunitario');
    }

}