<?php
class Gestaomodel extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function inserir($data){
        $this->db->set('data_criacao', 'NOW()', FALSE);
        $this->db->insert('gestao', $data); 
        $id_gestao = $this->db->insert_id();
        return $id_gestao;
    } //ok

    public function buscar($array=array()){
        $this->db->from('gestao');
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    } //ok

    public function listar($id_gestao=null, $array=array()){
        $this->db->from('gestao');
        if(!is_null($id_gestao)) $this->db->where('id_gestao', $id_gestao);
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    } //ok

    public function atualizar($id_gestao, $data) {
        $this->db->set('data_alteracao', 'NOW()', FALSE);
        $this->db->where('id_gestao', $id_gestao);
        $this->db->update('gestao', $data);
    } //ok

    public function deletar($id_gestao){
        $this->db->where('id_gestao', $id_gestao);
        $this->db->delete('gestao');
    } //ok

}