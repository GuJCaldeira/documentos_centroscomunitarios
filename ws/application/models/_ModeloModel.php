<?php

class ModeloModel extends CI_Model {
    public function __construct() {
        parent::__construct();

    }

    public function inserir($data){
        $this->db->set('data_criacao', 'NOW()', FALSE);
        $this->db->insert('usuario', $data); 
        $id = $this->db->insert_id();
        return $id;
    }

    public function buscar($array=array()){
        $this->db->from('usuario');
        foreach($array as $item => $value) {
            if(is_array($value)) {
                foreach($value as $val){
                    $this->db->where($item, $val);
                }
            } else {
                $this->db->where($item, $value);
            }
        }
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function listar($id=null){
        $this->db->from('usuario');
        if(!is_null($id)) $this->db->where('id', $id);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function atualizar($id, $data) {
        $this->db->set('data_alteracao', 'NOW()', FALSE);
        $this->db->where('id', $id);
        $this->db->update('usuario', $data);
    }

    public function deletar($id){
        $this->db->where('id', $id);
        $this->db->delete('usuario');
    }

}