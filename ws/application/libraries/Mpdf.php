<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class mpdf {
    
    public function __construct() {
        $CI = & get_instance();
    }

    public function load($param=[]) {
        require  APPPATH.'/third_party/mpdf/vendor/autoload.php';

        /*if ($param == null) {
            //$param = '"en-GB-x","A4","","",10,10,10,10,6,3';
            return new mPDF($param);       
        } else {
            $topD = 9;
            $esqD = 8;
            return new mPDF('','', 0, '', $esqD, 0, $topD, 0, 0, 0, 'L');
        }*/
        $mpdf = new \Mpdf\Mpdf($param);
        $mpdf->SetDisplayMode('fullpage');
        return $mpdf;
        //return $pusher;     
    }
}