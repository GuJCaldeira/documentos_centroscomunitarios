<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VALE <?php echo $vale->hash_vale; ?> - SGECDE</title>
<style type="text/css">
<!--
.style3 {font-size: 12px}
body {
  font-size: 11px;
}
.tableStri table { 
color: #333;
font-family: Helvetica, Arial, sans-serif;
width: 640px; 
border-collapse: 
collapse; border-spacing: 0; 
}

.tableStri td, th { 
border: 1px solid transparent; /* No more visible border */
height: 22px; 
transition: all 0.3s;  /* Simple transition for hover effect */
}

.tableStri th {
background: #DFDFDF;  /* Darken header a bit */
font-weight: bold;
}

.tableStri td {
background: #FAFAFA;
/*text-align: center;*/
}

.teste {
  text-align: center;
}
.teste2 {
  text-align: right;
}

/* Cells in even rows (2,4,6...) are one color */ 
.tableStri tr:nth-child(even) td { background: #F1F1F1; }   

/* Cells in odd rows (1,3,5...) are another (excludes header cells)  */ 
.tableStri tr:nth-child(odd) td { background: #FEFEFE; }  

.tableStri tr td:hover { background: #666; color: #FFF; } /* Hover cell effect! */
-->

.header {
  display: table;
}
.logo {
  float: left;
  width: 20%;
}
.cabecalho {
  text-align: center;
  margin-left: 21%;
}
/*@page :first {    
    header: html_firstpageheader;
}*/

.pontilhado {width:100%; border-bottom: #000 1px dashed; margin: 10px 0 10px 0;}
</style>
</head>

<body>

<div class="header">
  <div class="logo"><img src="<?php echo base_url(); ?>/logo.png" width="150" heigth="110"></div>
  <div class="cabecalho">
    <strong>CENTROS COMUNITÁRIOS - PIRACICABA</strong>
    <br><!--Rua asdasdasd -->
    <br><!--CNPJ: 0000000000000 / IE: 00000000000000000000-->
    <br><!--aaaa / aaaa-->
    <br><!--(19) 3414-2004-->
  </div>
</div>
<br>

<table width="100%" border="0" class="tableSemNada">
  <thead>
  <tr>
    <td width="50%">Piracicaba, <?php echo date("d", time()); ?> de 
      <?php
      $mes_extenso = array(
        'Jan' => 'janeiro',
        'Feb' => 'fevereiro',
        'Mar' => 'marco',
        'Apr' => 'abril',
        'May' => 'maio',
        'Jun' => 'junho',
        'Jul' => 'julho',
        'Aug' => 'agosto',
        'Nov' => 'novembro',
        'Sep' => 'setembro',
        'Oct' => 'outubro',
        'Dec' => 'dezembro'
    ); ?>
    <?php echo $mes_extenso[date("M", time())]; ?>  de <?php echo date("Y", time()); ?></td>
    <td class="teste2"></td>
  </tr>
</thead>
</table>
<br>
<?php

echo "<b>Associação: </b>";
if(!is_null($busca['caixa.id_associacao'])){
    //echo $conta->nome_conta;
} else {
    echo "TODOS";
}
echo "<br>";

echo "<b>Grupo de conta: </b>";
if(!is_null($busca['caixa.id_conta'])){
    echo $conta->nome_conta;
} else {
    echo "TODOS";
}
echo "<br>";

echo "<b>Tipo do movimento: </b>";
if(!is_null($busca['caixa.movimento'])){
    if($busca['caixa.movimento']=='S')
        echo "SAÍDA";
    if($busca['caixa.movimento']=='E')
        echo "ENTRADA";
    if($busca['caixa.movimento']=='T')
        echo "TODOS";
} else {
    echo "TODOS";
}
echo "<br>";

if(!is_null($busca['caixa.data_lancamento>='])){
    echo "<b>Data de: </b>";
    echo date("d/m/Y", strtotime($busca['caixa.data_lancamento>=']));
    echo "<br>";
} else {
    echo "<b>Data de:</b> desde o início";
    echo "<br>";
}

if(!is_null($busca['caixa.data_lancamento<='])){
    echo "<b>Data até: </b>";
    echo date("d/m/Y", strtotime($busca['caixa.data_lancamento<=']));
    echo "<br>";
} else {
    echo "<b>Data até:</b> o fim";
    echo "<br>";
}

if(!is_null($busca['caixa.valor>='])){
    echo "<b>Valor mínimo: </b>R$ ";
    echo number_format($busca['caixa.valor>='], 2, ",", ".");
    echo "<br>";
} else {
    echo "<b>Valor mínimo:</b> Nenhum";
    echo "<br>";
}

if(!is_null($busca['caixa.valor<='])){
    echo "<b>Valor máximo: </b>R$ ";
    echo number_format($busca['caixa.valor<='], 2, ",", ".");
    echo "<br>";
} else {
    echo "<b>Valor máximo:</b> Nenhum";
    echo "<br>";   
}



?>

<br>

<?php if(count($movimentacoes)>0){ ?>
<p>
<table class="tableStri" width="100%">
    <thead>
        <tr>
            <th>#</th>
            <th>Data do lançamento</th>
            <th>Conta</th>
            <th>Descrição</th>
            <th>Valor</th>
            <th>Movimento</th>
            <th>Saldo</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0; $saldo = 0; foreach ($movimentacoes as $movi) { ?>
        <tr>
            <td><?php echo $i = $i + 1;?></td>
            <td><?php echo date("d/m/Y", strtotime($movi->data_lancamento));?></td>
            <td><?php echo $movi->nome_conta;?></td>
            <td><?php echo $movi->descricao;?></td>
            <td><?php echo "R$ ".number_format($movi->valor, 2, ",", ".");;?></td>
            <td>
                <center>
                <?php
                    if($movi->movimento=='S'){
                        echo "Saída";
                    } else {
                        echo "Entrada";
                    }
                ?>
                </center>
            </td>
            <td style="text-align: right">
                <?php
                    if($movi->movimento=='S'){
                        $saldo -= ($movi->valor * 1);
                    } else{
                        $saldo += ($movi->valor * 1);
                    }
                    echo "R$ ".number_format($saldo, 2, ",", ".");
                ?>
            </td>
        </tr>
        <?php } ?>


        <tr>
            <td colspan="6" style="text-align: right"><div align="right"><strong>TOTAL:</strong></div></td>
            <td style="text-align: right"><strong>R$ <?php echo number_format($saldo, 2, ",", "."); ?></strong></td>
        </tr>
    </tbody>
</table>

</p>
<?php } else { ?>
<center>Nenhum registro foi encontrado com estes parâmetros!</center>
<?php } ?>


<p></p>

</body>
</html>