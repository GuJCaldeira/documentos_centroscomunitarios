<?php
class Evento extends CI_Controller {

	public function listar($id_evento=null){
		$this->load->model("eventomodel");

		$eventos = $this->eventomodel->listar($id_evento);
		$aEventos = array();

		foreach($eventos as $evento){

			if($evento->locacao_evento=='E') {
				$nEventos = array(
					'id_centro_comunitario' => $evento->id_centro_comunitario,
					'nome_centro' => $evento->nome_centro,
					'titulo' => $evento->nome_evento,
					'descricao' =>  $evento->descricao,
					'data_inicio' =>  $evento->data_inicio,
					'data_final' => $evento->data_final,
					'tipo_evento' => 'E'
				);
			} else {
				$nEventos = array(
					'id_centro_comunitario' => $evento->id_centro_comunitario,
					'nome_centro' => $evento->nome_centro,
					'titulo' => $evento->nome_evento,
					'descricao' =>  $evento->nome_evento,
					'data_inicio' =>  $evento->data_inicio,
					'data_final' => $evento->data_final,
					'tipo_evento' => 'L'
				);
			}


			$aEventos[] = $nEventos;
		}


		$rps = array(
			'status' => true,
			'obj' => array(
				'evento' => $aEventos
			)
		);
		echo json_encode($rps);
	}
}