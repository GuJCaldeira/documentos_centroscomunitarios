<?php
class Centrocomunitario extends CI_Controller {

	public function listar($id_centro=null){
		$this->load->model("centro_comunitariomodel");

		$centro_comunitario = $this->centro_comunitariomodel->listar($id_centro);
		$aCentros = array();

		foreach($centro_comunitario as $cc){
            $eCC = array(
                'id_centro_comunitario' => $cc->id_centro_comunitario,
                'nome_centro' => $cc->nome_centro." (".$cc->bairro.")",
            );

			$aCentros[] = $eCC;
		}


		$rps = array(
			'status' => true,
			'obj' => array(
				'centros' => $aCentros
			)
		);
		echo json_encode($rps);
	}
}