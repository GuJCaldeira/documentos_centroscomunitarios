<?php
class ModeloController extends CI_Controller {
	public function inserir() {
		$this->authsession->valida('usuarios', 'write');

		$this->load->model("usuariomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome', 'nome do usuário', 'trim|required');
		$this->form_validation->set_rules('data_nasc', 'data de nascimento', 'trim|required');
		$this->form_validation->set_rules('email', 'email do usuário', 'trim|valid_email|required');
		$this->form_validation->set_rules('telefone1', 'telefone1 do usuário', 'trim|required');
		$this->form_validation->set_rules('telefone2', 'telefone2 do usuário', 'trim');
		$this->form_validation->set_rules('foto', 'foto', 'trim');
		$this->form_validation->set_rules('id_associacao', 'id da associação', 'trim');
		$this->form_validation->set_rules('id_cargo', 'id do cargo', 'trim');
		$this->form_validation->set_rules('data_admissao', 'data de admissão', 'trim');
		$this->form_validation->set_rules('data_demissao', 'data de demissão', 'trim');
		$this->form_validation->set_rules('id_endereco', 'id do endereço', 'trim');
		$this->form_validation->set_rules('senha', 'senha do usuário', 'trim|required');
		$this->form_validation->set_rules('cpf', 'cpf do usuário', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$senha = password_hash($this->input->post("senha"), PASSWORD_DEFAULT);

			$arrayUsuario = array(
				'nome' => $this->input->post("nome"),
				'data_nasc' => $this->input->post("data_nasc"),
				'email' => $this->input->post("email"),
				'telefone1' => $this->input->post("telefone1"),
				'telefone2' => $this->input->post("telefone2"),
				'foto' => $this->input->post("foto"),
				'id_associacao' => $this->input->post("id_associacao"),
				'id_cargo' => $this->input->post("id_cargo"),
				'data_admissao' => $this->input->post("data_admissao"),
				'data_demissao' => $this->input->post("data_demissao"),
				'id_endereco' => $this->input->post("id_endereco"),
				'senha' => $senha,
				'cpf' => $this->input->post("cpf"),
			);

			$id_usuario = $this->usuariomodel->inserir($arrayUsuario);
			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Inserido com sucesso!',
					'usuario' => $arrayUsuario,
					'id_usuario' => $id_usuario
				)
			);
			echo json_encode($rps);
		}
	}

	public function listar($id_usuario=null){
		$this->authsession->valida('usuarios', 'read');
		$this->load->model("usuariomodel");

		$usuarios = $this->usuariomodel->listar($id_usuario);

		$rps = array(
			'status' => true,
			'obj' => array(
				'usuarios' => $usuarios
			)
		);
		echo json_encode($rps);
	}

	public function atualizar(){
		$this->authsession->valida('usuarios', 'update');
		$this->load->model("usuariomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome', 'nome do usuário', 'trim|required');
		$this->form_validation->set_rules('data_nasc', 'data de nascimento', 'trim|required');
		$this->form_validation->set_rules('email', 'email do usuário', 'trim|valid_email|required');
		$this->form_validation->set_rules('telefone1', 'telefone1 do usuário', 'trim|required');
		$this->form_validation->set_rules('telefone2', 'telefone2 do usuário', 'trim');
		$this->form_validation->set_rules('foto', 'foto', 'trim');
		$this->form_validation->set_rules('id_associacao', 'id da associação', 'trim');
		$this->form_validation->set_rules('id_cargo', 'id do cargo', 'trim');
		$this->form_validation->set_rules('data_admissao', 'data de admissão', 'trim');
		$this->form_validation->set_rules('data_demissao', 'data de demissão', 'trim');
		$this->form_validation->set_rules('id_endereco', 'id do endereço', 'trim');
		$this->form_validation->set_rules('senha', 'senha do usuário', 'trim');
		$this->form_validation->set_rules('cpf', 'cpf do usuário', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_usuario = $this->input->post("id_usuario");
			$usuario = $this->usuariomodel->listar($id_usuario);

			if(count($usuario)>0){

				$arrayAtualiza = array(
					'nome' => $this->input->post("nome"),
					'data_nasc' => $this->input->post("data_nasc"),
					'email' => $this->input->post("email"),
					'telefone1' => $this->input->post("telefone1"),
					'telefone2' => $this->input->post("telefone2"),
					'foto' => $this->input->post("foto"),
					'id_associacao' => $this->input->post("id_associacao"),
					'id_cargo' => $this->input->post("id_cargo"),
					'data_admissao' => $this->input->post("data_admissao"),
					'data_demissao' => $this->input->post("data_demissao"),
					'id_endereco' => $this->input->post("id_endereco"),
					'cpf' => $this->input->post("cpf"),
				);

				$senha = $this->input->post("senha");
				if($senha!=''){
					$arrayAtualiza['senha'] = password_hash($senha, PASSWORD_DEFAULT);
				}

				$this->usuariomodel->atualizar($id_usuario, $arrayAtualiza);

				$rps = array(
					'status' => true,
					'message' => 'Atualizado com sucesso!',
					'obj' => $arrayAtualiza,
					'id_usuario' => $id_usuario
				);
				echo json_encode($rps);

			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Usuário não existe!'
				);
				echo json_encode($rps);
			}
		}
	}

	public function deletar(){
		$this->authsession->valida('usuarios', 'delete');
		$this->load->model("usuariomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_usuario', 'id do usuário', 'trim|required');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_usuario = $this->input->post("id_usuario");
			$usuario = $this->usuariomodel->listar($id_usuario);

			if(count($usuario)>0){
				//echo 'usuário existe';
				$this->usuariomodel->deletar($id_usuario);

				$rps = array(
					'status' => true,
					'message' => 'Deletado com sucesso!',
					'id_usuario' => $id_usuario
				);
				echo json_encode($rps);

			} else {
				//echo 'usuário não existe';
				$rps = array(
					'status' => false,
					'erro' => 'Usuário não existe!'
				);
				echo json_encode($rps);
			}


		}
	}





}