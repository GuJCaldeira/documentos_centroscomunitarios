<?php
class Gestao extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$rps = array(
			'status' => false,
			'erro' => 'Endpoint desativado 02/10/2018, LM'
		);
		die(json_encode($rps));
	}
	public function inserir() {
		$this->authsession->valida('gestao', 'write');

		$this->load->model("gestaomodel");
		$this->load->library("form_validation");
		$this->form_validation->set_rules('status', 'status da gestão', 'trim|required');
		$this->form_validation->set_rules('id_associacao', 'id da associação', 'trim|required');
		$this->form_validation->set_rules('id_centro_comunitario', 'id do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('data_inicio', 'data de início do evento', 'trim|required');
		$this->form_validation->set_rules('data_final', 'data de encerramento do evento', 'trim|required');
		
		if($this->form_validation->run()==FALSE){
			$rps = array('status' => false,'erro' => validation_errors());
			echo json_encode($rps);
		} else {
			
			$arrayGestao = array(
				'status' => $this->input->post("status"),
				'id_associacao' => $this->input->post("id_associacao"),
				'id_centro_comunitario' => $this->input->post("id_centro_comunitario"),
				'status' => $this->input->post("status"),
				'data_inicio' => $this->input->post("data_inicio"),
				'data_final' => $this->input->post("data_final"),
			);

			$id_gestao = $this->gestaomodel->inserir($arrayGestao);
			$rps = array(
				'status' => true,
				'obj' => array('message' => 'Inserido com sucesso!','usuario' => $arrayGestao,
					'id_gestao' => $id_gestao)
			);
			echo json_encode($rps);
		}
	} //ok

	public function listar($id_gestao=null){
		$this->authsession->valida('gestao', 'read');
		$this->load->model("gestaomodel");

		$gestao = $this->gestaomodel->listar($id_gestao);

		$rps = array('status' => true,'obj' => array('gestao' => $gestao));
		echo json_encode($rps);
	} //ok

	public function atualizar(){
		$this->authsession->valida('gestao', 'update');
		$this->load->model("gestaomodel");
		$this->load->library("form_validation");
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('id_associacao', 'id da associação', 'trim|required');
		$this->form_validation->set_rules('id_centro_comunitario', 'id do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('status', 'status da gestão', 'trim|required');
		$this->form_validation->set_rules('data_inicio', 'data de início do evento', 'trim|required');
		$this->form_validation->set_rules('data_final', 'data de encerramento do evento', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$rps = array('status' => false,	'erro' => validation_errors());
			echo json_encode($rps);
		} else {
			$id_gestao = $this->input->post("id_gestao");
			$gestao = $this->gestaomodel->listar($id_gestao);

			if(count($gestao)>0){

				$arrayAtualiza = array(
					'status' => $this->input->post("status"),
					'id_associacao' => $this->input->post("id_associacao"),
					'id_centro_comunitario' => $this->input->post("id_centro_comunitario"),
					'status' => $this->input->post("status"),
					'data_inicio' => $this->input->post("data_inicio"),
				'data_final' => $this->input->post("data_final"),
				);

				$this->gestaomodel->atualizar($id_gestao, $arrayAtualiza);

				$rps = array(
					'status' => true,
					'message' => 'Atualizado com sucesso!',
					'obj' => $arrayAtualiza,
					'id_gestao' => $id_gestao
				);
				echo json_encode($rps);

			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Denúncia não existe!'
				);
				echo json_encode($rps);
			}
		}
	} //ok

	public function deletar(){
		$this->authsession->valida('gestao', 'delete');
		$this->load->model("gestaomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_gestao', 'Código da Gestão', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$rps = array('status' => false,	'erro' => validation_errors());
			echo json_encode($rps);
		} else {
			$id_gestao = $this->input->post("id_gestao");
			$usuario = $this->gestaomodel->listar($id_gestao);

			if(count($usuario)>0){
				$this->gestaomodel->deletar($id_gestao);
				$rps = array(
					'status' => true,
					'message' => 'Deletada com sucesso!',
					'id_gestao' => $id_gestao
				);
				echo json_encode($rps);

			} else {
				$rps = array('status' => false,'erro' => 'Gestão não existe!');
				echo json_encode($rps);
			}
		}
	} //ok
}