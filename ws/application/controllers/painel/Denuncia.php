<?php
class Denuncia extends CI_Controller {
	public function inserir() {
		$this->authsession->valida('denuncia', 'write');

		$this->load->model("denunciamodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome', 'nome do usuário', 'trim');
		$this->form_validation->set_rules('status', 'status da denuncia', 'trim|required');
		$this->form_validation->set_rules('destino', 'destino', 'trim|required');
		$this->form_validation->set_rules('email', 'e-mail do usuário', 'trim|valid_email');
		$this->form_validation->set_rules('descricao', 'descrição', 'trim|required');
		$this->form_validation->set_rules('detalhes', 'detalhes', 'trim');
		$this->form_validation->set_rules('anexo', 'anexo', 'trim');
		
		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$senha = password_hash($this->input->post("senha"), PASSWORD_DEFAULT);

			$arrayUsuario = array(
				'nome' => $this->input->post("nome"),
				'status' => $this->input->post("status"),
				'email' => $this->input->post("email"),
				'destino' => $this->input->post("destino"),
				'descricao' => $this->input->post("descricao"),
				'detalhes' => $this->input->post("detalhes"),
				'anexo' => $this->input->post("anexo"),
			);

			$id_denuncia = $this->denunciamodel->inserir($arrayUsuario);
			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Inserido com sucesso!',
					'usuario' => $arrayUsuario,
					'id_denuncia' => $id_denuncia
				)
			);
			echo json_encode($rps);
		}
	}

	public function listar($id_denuncia=null){
		$this->authsession->valida('denuncia', 'read');
		$this->load->model("denunciamodel");

		$denuncias = $this->denunciamodel->listar($id_denuncia);

		$rps = array(
			'status' => true,
			'obj' => array(
				'denuncias' => $denuncias
			)
		);
		echo json_encode($rps);
	}

	public function atualizar(){
		$this->authsession->valida('denuncia', 'update');
		$this->load->model("denunciamodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome', 'nome do usuário', 'trim');
		$this->form_validation->set_rules('status', 'status da denuncia', 'trim|required');
		$this->form_validation->set_rules('destino', 'destino', 'trim|required');
		$this->form_validation->set_rules('email', 'e-mail do usuário', 'trim|valid_email');
		$this->form_validation->set_rules('descricao', 'descrição', 'trim|required');
		$this->form_validation->set_rules('detalhes', 'detalhes', 'trim');
		$this->form_validation->set_rules('anexo', 'anexo', 'trim');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_denuncia = $this->input->post("id_denuncia");
			$denuncia = $this->denunciamodel->listar($id_denuncia);

			if(count($denuncia)>0){

				$arrayAtualiza = array(
					'nome' => $this->input->post("nome"),
					'status' => $this->input->post("status"),
					'destino' => $this->input->post("destino"),
					'email' => $this->input->post("email"),
					'descricao' => $this->input->post("descricao"),
					'detalhes' => $this->input->post("detalhes"),
					'anexo' => $this->input->post("anexo"),
				);

				$this->denunciamodel->atualizar($id_denuncia, $arrayAtualiza);

				$rps = array(
					'status' => true,
					'message' => 'Atualizado com sucesso!',
					'obj' => $arrayAtualiza,
					'id_denuncia' => $id_denuncia
				);
				echo json_encode($rps);

			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Denúncia não existe!'
				);
				echo json_encode($rps);
			}
		}
	}
}