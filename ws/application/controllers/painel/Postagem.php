<?php
class Postagem extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}

	public function testeEmail(){
		$this->load->model("emailmodel");
		$a['destinatario'] = 'lucasm@cdepiracicaba.com.br';
		$a['assunto'] = 'TEstando email';
		$a['corpo'] = 'teiuhash iuashdiahsd iuahsdiahsdiuhasd';
		$this->emailmodel->envia($a);
	}

	public function inserir() {
		$this->authsession->valida('postagem', 'write');

		$this->load->model("postagemmodel");
		$this->load->library("form_validation");
		$this->form_validation->set_rules('id_centro_comunitario', 'centro comunitário', 'trim|required');
		$this->form_validation->set_rules('titulo', 'título postagem', 'trim|required');
		$this->form_validation->set_rules('destaque', 'destaque desta postagem', 'trim|required');
		$this->form_validation->set_rules('menu', 'é menu desta postagem', 'trim|required');
		$this->form_validation->set_rules('conteudo', 'conteudo desta postagem', 'trim|required');
		$this->form_validation->set_rules('status','status da postagem','trim|required');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$arrayPostagem = array(
				'id_centro_comunitario' => $this->input->post("id_centro_comunitario"),
				'titulo' => $this->input->post("titulo"),
				'destaque' => $this->input->post("destaque"),
				'menu' => $this->input->post("menu"),
				'conteudo' => $this->input->post("conteudo"),
				'status' => $this->input->post("status"),
				'id_usuario' => $this->id_usuario
			);

			$id_postagem = $this->postagemmodel->inserir($arrayPostagem);
			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Inserido com sucesso!',
					'postagem' => $arrayPostagem,
					'id_postagem' => $id_postagem
				)
			);
			echo json_encode($rps);
		}
	}

	public function listar($id_postagem=null){
		$this->authsession->valida('postagem', 'read');
		$this->load->model("postagemmodel");

		$busca = array();
		if(!$this->id_adm){
			$this->load->model("associacaomodel");
			$associacao = $this->associacaomodel->listar($this->id_associacao)[0];

			$busca['evento.id_centro_comunitario'] = $associacao->id_centro_comunitario;
		}

		$postagem = $this->postagemmodel->listar($id_postagem, $busca);

		$rps = array(
			'status' => true,
			'obj' => array(
				'postagem' => $postagem
			)
		);
		echo json_encode($rps);
	}

	public function atualizar(){
		$this->authsession->valida('postagem', 'update');
		$this->load->model("postagemmodel");
		$this->load->library("form_validation");
		$this->form_validation->set_rules('id_centro_comunitario', 'centro comunitário', 'trim|required');
		$this->form_validation->set_rules('titulo', 'título postagem', 'trim|required');
		$this->form_validation->set_rules('destaque', 'destaque desta postagem', 'trim|required');
		$this->form_validation->set_rules('menu', 'é menu desta postagem', 'trim|required');
		$this->form_validation->set_rules('conteudo', 'conteudo desta postagem', 'trim|required');
		$this->form_validation->set_rules('status','status da postagem','trim|required');


		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_postagem = $this->input->post("id_postagem");
			$postagem = $this->postagemmodel->listar($id_postagem);

			if(count($postagem)>0){

				$arrayAtualiza = array(
					'id_centro_comunitario' => $this->input->post("id_centro_comunitario"),
					'titulo' => $this->input->post("titulo"),
					'destaque' => $this->input->post("destaque"),
					'menu' => $this->input->post("menu"),
					'conteudo' => $this->input->post("conteudo"),
					'status' => $this->input->post("status")
				);



				$this->postagemmodel->atualizar($id_postagem, $arrayAtualiza);

				$rps = array(
					'status' => true,
					'message' => 'Atualizado com sucesso!',
					'obj' => $arrayAtualiza,
					'id_postagem' => $id_postagem
				);
				echo json_encode($rps);

			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Postagem não existe!'
				);
				echo json_encode($rps);
			}
		}
	}

	public function deletar(){
		$this->authsession->valida('postagem', 'delete');
		$this->load->model("postagemmodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_postagem', 'id da postagem', 'trim|required');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_postagem = $this->input->post("id_postagem");
			$postagem = $this->postagemmodel->listar($id_postagem);

			if(count($postagem)>0){
				//echo 'usuário existe';
				$this->postagemmodel->deletar($id_postagem);

				$rps = array(
					'status' => true,
					'message' => 'Deletado com sucesso!',
					'id_postagem' => $id_postagem
				);
				echo json_encode($rps);

			} else {
				//echo 'usuário não existe';
				$rps = array(
					'status' => false,
					'erro' => 'Postagem não existe!'
				);
				echo json_encode($rps);
			}


		}
	}

}