<?php
class Usuario extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}

	public function inserir() {
		$this->authsession->valida('usuario', 'write');

		$this->load->model("usuariomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome', 'nome do usuário', 'trim|required');
		$this->form_validation->set_rules('data_nasc', 'data de nascimento', 'trim|required');
		$this->form_validation->set_rules('email', 'e-mail do usuário', 'trim|valid_email|required');
		$this->form_validation->set_rules('telefone1', 'telefone do usuário', 'trim|required');
		$this->form_validation->set_rules('telefone2', 'celular do usuário', 'trim');
		$this->form_validation->set_rules('foto', 'foto do usuário', 'trim');
		$this->form_validation->set_rules('id_associacao', 'id da associação', 'trim|required');
		$this->form_validation->set_rules('id_grupo', 'id do grupo', 'trim|required');
		$this->form_validation->set_rules('id_cargo', 'id do cargo', 'trim|required');
		$this->form_validation->set_rules('data_admissao', 'data de admissão', 'trim');
		$this->form_validation->set_rules('data_demissao', 'data de demissão', 'trim');
		$this->form_validation->set_rules('senha', 'senha do usuário', 'trim|required');
		$this->form_validation->set_rules('cpf', 'CPF do usuário', 'trim|required');
		$this->form_validation->set_rules('cep', 'CEP do endereço', 'trim|required');
		$this->form_validation->set_rules('endereco', 'endereço do endereço', 'trim|required');
		$this->form_validation->set_rules('numero', 'número do endereço', 'trim|required');
		$this->form_validation->set_rules('bairro', 'bairro do endereço', 'trim|required');
		$this->form_validation->set_rules('complemento', 'complemento do endereço', 'trim');
		$this->form_validation->set_rules('cidade', 'cidade do endereço', 'trim|required');
		$this->form_validation->set_rules('estado', 'Estado do endereço', 'trim|required');
		$this->form_validation->set_rules('detalhes', 'detalhes do endereço', 'trim');
		$this->form_validation->set_rules('status','status do usuário','trim');
		$this->form_validation->set_rules('sexo','sexo do usuário','trim');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$senha = password_hash($this->input->post("senha"), PASSWORD_DEFAULT);

			$arrayUsuario = array(
				'nome' => $this->input->post("nome"),
				'data_nasc' => $this->input->post("data_nasc"),
				'email' => $this->input->post("email"),
				'telefone1' => $this->input->post("telefone1"),
				'telefone2' => $this->input->post("telefone2"),
				'foto' => $this->input->post("foto"),
				'id_associacao' => $this->input->post("id_associacao"),
				'id_grupo' => $this->input->post("id_grupo"),
				'id_cargo' => $this->input->post("id_cargo"),
				'data_admissao' => $this->input->post("data_admissao"),
				'data_demissao' => $this->input->post("data_demissao"),
				'senha' => $senha,
				'cpf' => $this->input->post("cpf"),
				'cep' => $this->input->post("cep"),
				'endereco' => $this->input->post("endereco"),
				'numero' => $this->input->post("numero"),
				'bairro' => $this->input->post("bairro"),
				'complemento' => $this->input->post("complemento"),
				'cidade' => $this->input->post("cidade"),
				'estado' => $this->input->post("estado"),
				'detalhes' => $this->input->post("detalhes"),
				'status' => $this->input->post("status"),
				'sexo' => $this->input->post("sexo")
			);

			$id_usuario = $this->usuariomodel->inserir($arrayUsuario);
			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Inserido com sucesso!',
					'usuario' => $arrayUsuario,
					'id_usuario' => $id_usuario
				)
			);
			echo json_encode($rps);
		}
	}

	public function listar($id_usuario=null){
		$this->authsession->valida('usuario', 'read');
		$this->load->model("usuariomodel");

		$busca = array();
		if(!$this->id_adm){
			$busca['id_associacao'] = $this->id_associacao;
			$busca['id_usuario>']   = 0;
		}

		$usuarios = $this->usuariomodel->listar($id_usuario, $busca);

		$rps = array(
			'status' => true,
			'obj' => array(
				'usuarios' => $usuarios
			)
		);
		echo json_encode($rps);
	}

	public function atualizar(){
		$this->authsession->valida('usuario', 'update');
		$this->load->model("usuariomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome', 'nome do usuário', 'trim|required');
		$this->form_validation->set_rules('data_nasc', 'data de nascimento', 'trim|required');
		$this->form_validation->set_rules('email', 'e-mail do usuário', 'trim|valid_email|required');
		$this->form_validation->set_rules('telefone1', 'telefone do usuário', 'trim|required');
		$this->form_validation->set_rules('telefone2', 'celular do usuário', 'trim');
		$this->form_validation->set_rules('foto', 'foto do usuário', 'trim');
		$this->form_validation->set_rules('id_associacao', 'id da associação', 'trim|required');
		$this->form_validation->set_rules('id_grupo', 'id do grupo', 'trim|required');
		$this->form_validation->set_rules('id_cargo', 'id do cargo', 'trim|required');
		$this->form_validation->set_rules('data_admissao', 'data de admissão', 'trim');
		$this->form_validation->set_rules('data_demissao', 'data de demissão', 'trim');
		$this->form_validation->set_rules('senha', 'senha do usuário', 'trim');
		$this->form_validation->set_rules('cpf', 'CPF do usuário', 'trim|required');
		$this->form_validation->set_rules('cep', 'CEP do endereço', 'trim|required');
		$this->form_validation->set_rules('endereco', 'endereço do endereço', 'trim|required');
		$this->form_validation->set_rules('numero', 'número do endereço', 'trim|required');
		$this->form_validation->set_rules('bairro', 'bairro do endereço', 'trim|required');
		$this->form_validation->set_rules('complemento', 'complemento do endereço', 'trim');
		$this->form_validation->set_rules('cidade', 'cidade do endereço', 'trim|required');
		$this->form_validation->set_rules('estado', 'estado do endereço', 'trim|required');
		$this->form_validation->set_rules('status','status do usuário','trim');
		$this->form_validation->set_rules('sexo','sexo do usuário','trim');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_usuario = $this->input->post("id_usuario");
			$usuario = $this->usuariomodel->listar($id_usuario);

			if(count($usuario)>0){

				$arrayAtualiza = array(
					'nome' => $this->input->post("nome"),
					'data_nasc' => $this->input->post("data_nasc"),
					'email' => $this->input->post("email"),
					'telefone1' => $this->input->post("telefone1"),
					'telefone2' => $this->input->post("telefone2"),
					'foto' => $this->input->post("foto"),
					'id_associacao' => $this->input->post("id_associacao"),
					'id_grupo' => $this->input->post("id_grupo"),
					'id_cargo' => $this->input->post("id_cargo"),
					'data_admissao' => $this->input->post("data_admissao"),
					'data_demissao' => $this->input->post("data_demissao"),
					'cpf' => $this->input->post("cpf"),
					'cep' => $this->input->post("cep"),
					'endereco' => $this->input->post("endereco"),
					'numero' => $this->input->post("numero"),
					'bairro' => $this->input->post("bairro"),
					'complemento' => $this->input->post("complemento"),
					'cidade' => $this->input->post("cidade"),
					'estado' => $this->input->post("estado"),
					'status' => $this->input->post("status"),
					'sexo' => $this->input->post("sexo")
				);

				$senha = $this->input->post("senha");
				if($senha!=''){
					$arrayAtualiza['senha'] = password_hash($senha, PASSWORD_DEFAULT);
				}

				$this->usuariomodel->atualizar($id_usuario, $arrayAtualiza);

				$rps = array(
					'status' => true,
					'message' => 'Atualizado com sucesso!',
					'obj' => $arrayAtualiza,
					'id_usuario' => $id_usuario
				);
				echo json_encode($rps);

			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Usuário não existe!'
				);
				echo json_encode($rps);
			}
		}
	}

	public function deletar(){
		$this->authsession->valida('usuario', 'delete');
		$this->load->model("usuariomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_usuario', 'id do usuário', 'trim|required');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_usuario = $this->input->post("id_usuario");
			$usuario = $this->usuariomodel->listar($id_usuario);

			if(count($usuario)>0){
				//echo 'usuário existe';
				$this->usuariomodel->deletar($id_usuario);

				$rps = array(
					'status' => true,
					'message' => 'Deletado com sucesso!',
					'id_usuario' => $id_usuario
				);
				echo json_encode($rps);

			}else {
				//echo 'usuário não existe';
				$rps = array(
					'status' => false,
					'erro' => 'Usuário não existe!'
				);
				echo json_encode($rps);
			}


		}
	}

}
