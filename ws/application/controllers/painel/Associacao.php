<?php
class Associacao extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}

	public function inserir() {
		$this->authsession->valida('associacao', 'write');

		$this->load->model("associacaomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome_fantasia', 'nome fantasia da associação', 'trim|required');
		$this->form_validation->set_rules('razao_social', 'razão social da associação', 'trim|required');
		$this->form_validation->set_rules('cnpj', 'CNPJ da associação', 'trim|required');
		$this->form_validation->set_rules('telefone1', 'telefone da associação', 'trim|required');
		$this->form_validation->set_rules('telefone2', 'telefone celular da associação', 'trim');
		$this->form_validation->set_rules('data_constituicao', 'data da constituição', 'trim');
		$this->form_validation->set_rules('finalidade_estatutaria', 'finalidade estatutaria', 'trim');
		$this->form_validation->set_rules('email', 'e-mail da associação', 'trim|valid_email|required');
		$this->form_validation->set_rules('detalhes', 'detalhes da associação', 'trim');
		$this->form_validation->set_rules('cep', 'CEP da associação', 'trim|required');
		$this->form_validation->set_rules('endereco', 'endereço da Associação', 'trim|required');
		$this->form_validation->set_rules('numero', 'número da associação', 'trim|required');
		$this->form_validation->set_rules('bairro', 'bairro da associação', 'trim|required');
		$this->form_validation->set_rules('complemento', 'complemento do endereço da associação', 'trim');
		$this->form_validation->set_rules('cidade', 'cidade da associação', 'trim|required');
		$this->form_validation->set_rules('estado', 'estado da associação', 'trim|required');
		$this->form_validation->set_rules('status', 'status da Associação', 'trim|required');
		$this->form_validation->set_rules('id_centro_comunitario', 'Centro Comunitario', 'trim');
	

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$senha = password_hash($this->input->post("senha"), PASSWORD_DEFAULT);

			$arrayAssociacao = array(
				'nome_fantasia' => $this->input->post("nome_fantasia"),
				'razao_social' => $this->input->post("razao_social"),
				'cnpj' => $this->input->post("cnpj"),
				'telefone1' => $this->input->post("telefone1"),
				'telefone2' => $this->input->post("telefone2"),
				'data_constituicao' => $this->input->post("data_constituicao"),
				'finalidade_estatutaria' => $this->input->post("finalidade_estatutaria"),
				'email' => $this->input->post("email"),
				'detalhes' => $this->input->post("detalhes"),
				'cep' => $this->input->post("cep"),
				'endereco' => $this->input->post("endereco"),
				'numero' => $this->input->post("numero"),
				'bairro' => $this->input->post("bairro"),
				'complemento' => $this->input->post("complemento"),
				'cidade' => $this->input->post("cidade"),
				'estado' => $this->input->post("estado"),
				'status' => $this->input->post("status"),
				'id_centro_comunitario'=> (int)$this->input->post("id_centro_comunitario"),
			);

			$id_associacao = $this->associacaomodel->inserir($arrayAssociacao);
			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Inserido com sucesso!',
					'associacao' => $arrayAssociacao,
					'id_associacao' => $id_associacao
				)
			);
			echo json_encode($rps);
		}
	}

	public function listar($id_associacao=null){
		$this->authsession->valida('associacao', 'read');
		$this->load->model("associacaomodel");

		$busca = array();
		if(!$this->id_adm)
			$busca['id_associacao'] = $this->id_associacao;

		$associacoes = $this->associacaomodel->listar($id_associacao, $busca);

		$rps = array(
			'status' => true,
			'obj' => array(
				'associacoes' => $associacoes
			)
		);
		echo json_encode($rps);
	}

	public function atualizar(){
		$this->authsession->valida('associacao', 'update');
		$this->load->model("associacaomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome_fantasia', 'nome fantasia da associação', 'trim|required');
		$this->form_validation->set_rules('razao_social', 'razão social da associação', 'trim|required');
		$this->form_validation->set_rules('cnpj', 'CNPJ da associação', 'trim|required');
		$this->form_validation->set_rules('telefone1', 'telefone da associação', 'trim|required');
		$this->form_validation->set_rules('telefone2', 'telefone celular da associação', 'trim');
		$this->form_validation->set_rules('data_constituicao', 'data da constituição', 'trim');
		$this->form_validation->set_rules('finalidade_estatutaria', 'finalidade estatutaria', 'trim');
		$this->form_validation->set_rules('email', 'e-mail da associação', 'trim|valid_email|required');
		$this->form_validation->set_rules('detalhes', 'detalhes da associação', 'trim');
		$this->form_validation->set_rules('cep', 'CEP da associação', 'trim|required');
		$this->form_validation->set_rules('endereco', 'endereço da Associação', 'trim|required');
		$this->form_validation->set_rules('numero', 'número da Associação', 'trim|required');
		$this->form_validation->set_rules('bairro', 'bairro da Associação', 'trim|required');
		$this->form_validation->set_rules('complemento', 'complemento do endereço da associação', 'trim');
		$this->form_validation->set_rules('cidade', 'cidade da Associação', 'trim|required');
		$this->form_validation->set_rules('estado', 'estado da Associação', 'trim|required');
		$this->form_validation->set_rules('status', 'status da Associação', 'trim|required');
		$this->form_validation->set_rules('id_centro_comunitario', 'Centro Comunitario', 'trim');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_associacao = $this->input->post("id_associacao");
			$associacao = $this->associacaomodel->listar($id_associacao);

			if(count($associacao)>0){

				$arrayAtualiza = array(
					'nome_fantasia' => $this->input->post("nome_fantasia"),
					'razao_social' => $this->input->post("razao_social"),
					'cnpj' => $this->input->post("cnpj"),
					'telefone1' => $this->input->post("telefone1"),
					'telefone2' => $this->input->post("telefone2"),
					'data_constituicao' => $this->input->post("data_constituicao"),
					'finalidade_estatutaria' => $this->input->post("finalidade_estatutaria"),
					'email' => $this->input->post("email"),
					'detalhes' => $this->input->post("detalhes"),
					'cep' => $this->input->post("cep"),
					'endereco' => $this->input->post("endereco"),
					'numero' => $this->input->post("numero"),
					'bairro' => $this->input->post("bairro"),
					'complemento' => $this->input->post("complemento"),
					'cidade' => $this->input->post("cidade"),
					'estado' => $this->input->post("estado"),
					'status' => $this->input->post("status"),
					'id_centro_comunitario'=> $this->input->post("id_centro_comunitario"),
				);

				$senha = $this->input->post("senha");
				if($senha!=''){
					$arrayAtualiza['senha'] = password_hash($senha, PASSWORD_DEFAULT);
				}

				$this->associacaomodel->atualizar($id_associacao, $arrayAtualiza);

				$rps = array(
					'status' => true,
					'message' => 'Atualizado com sucesso!',
					'obj' => $arrayAtualiza,
					'id_associacao' => $id_associacao
				);
				echo json_encode($rps);

			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Associação não existe!'
				);
				echo json_encode($rps);
			}
		}
	}

	public function deletar(){
		$this->authsession->valida('associacao', 'delete');
		$this->load->model("associacaomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_associacao', 'id da associação', 'trim|required');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_associacao = $this->input->post("id_associacao");
			$associacao = $this->associacaomodel->listar($id_associacao);

			if(count($associacao)>0){
				//echo 'usuário existe';
				$this->associacaomodel->deletar($id_associacao);

				$rps = array(
					'status' => true,
					'message' => 'Deletado com sucesso!',
					'id_associacao' => $id_associacao
				);
				echo json_encode($rps);

			} else {
				//echo 'usuário não existe';
				$rps = array(
					'status' => false,
					'erro' => 'Associação não existe!'
				);
				echo json_encode($rps);
			}


		}
	}

}
