<?php if ( ! defined('BASEPATH')) exit('Acesso direto ao drive: Negado!');

class Documento extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}

	public function inserir() {
		$this->authsession->valida('documento', 'write');

		$this->load->model("postagemmodel");
		$this->load->library("form_validation");
		$this->form_validation->set_rules('nome_arquivo', 'nome do arquivo', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {

			$config['upload_path'] = "./uploads";
			$config['allowed_types'] = '*';
			$config['max_size']             = 9999999999999;


			$this->load->library('upload', $config);
			if (! $this->upload->do_upload("arquivo")) {
				$rps = array(
                    'status' => false,
                    'erro' => $this->upload->display_errors()
				);
				
				die(json_encode($rps));
			} else {
				$data = $this->upload->data();
				$fgc = file_get_contents($config['upload_path']."/".$data['file_name']);

				$this->load->model("documentomodel");
				$arrayDocumento = array(
					'id_usuario' => $this->id_usuario,
					'nome_arquivo' => $this->input->post("nome_arquivo"),
					'documento' => $fgc,
					'filename' => $data['file_name']
				);						
				$id_documento = $this->documentomodel->inserir($arrayDocumento);

				unlink($config['upload_path']."/".$data['file_name']);

				$rps = array(
					'status' => true,
					'obj' => array(
						'message' => 'Inserido com sucesso!',
						'documento' => $arrayDocumento,
						'id_documento' => $id_documento
						)
					);
				echo json_encode($rps);
			}
		}
	}

	public function download($id_documento){
		//$this->authsession->valida('documento', 'read');
		$this->load->model("documentomodel");
		$documento = $this->documentomodel->listar($id_documento);

		if(count($documento)>0){
			$documento = $documento[0];

			//session_start();
			$filename = $documento->filename;
			$file = $documento->documento;
			header("Content-Disposition: attachment; filename=$filename");
			ob_clean();
			flush();
			echo $file;
		} else{
			die("Documento inválido");
		}
	}

	public function listar($id_documento=null){
		//$this->authsession->valida('documento', 'read');
		$this->load->model("documentomodel");
		
		$documentos = $this->documentomodel->listar($id_documento);
		$nDoc = array();
		foreach($documentos as $doc){
			unset($doc->documento);
			$nDoc[] = $doc;
		}
	
		$rps = array(
			'status' => true,
			'obj' => array(
				'documento' => $nDoc
				)
			);
		echo json_encode($rps);
	}

	public function deletar(){
		$this->authsession->valida('documento', 'delete');
		$this->load->model("documentomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_documento', 'id do documento', 'trim|required');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_documento = $this->input->post("id_documento");
			$documento = $this->documentomodel->listar($id_documento);

			if(count($documento)>0){
				$this->documentomodel->deletar($id_documento);

				$rps = array(
					'status' => true,
					'message' => 'Deletado com sucesso!',
					'id_documento' => $id_documento
				);
				echo json_encode($rps);

			} else {
				//echo 'usuário não existe';
				$rps = array(
					'status' => false,
					'erro' => 'Documento não existe!'
				);
				echo json_encode($rps);
			}
		}
	}


}
