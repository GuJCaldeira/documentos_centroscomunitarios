<?php
class Locacao extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}

	public function inserir() {
		$this->authsession->valida('locacao', 'write');

		$this->load->model("eventomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('data_inicio', 'data inicial', 'trim|required');		
		$this->form_validation->set_rules('data_final', 'data termino', 'trim|required');		
		$this->form_validation->set_rules('tipo_evento', 'tipo do evento', 'trim|required');		
		$this->form_validation->set_rules('comentario', 'comentário', 'trim');		
		$this->form_validation->set_rules('valor', 'valor', 'trim');		
		$this->form_validation->set_rules('id_centro_comunitario', 'id centro comunitário', 'trim|required');		
		$this->form_validation->set_rules('status', 'status (A, I, P)', 'trim|required');		
		//$this->form_validation->set_rules('nome', 'nome', 'trim|required');
		//$this->form_validation->set_rules('email', 'email', 'trim');		
		//$this->form_validation->set_rules('telefone1', 'telefone 1', 'trim|required');		
		//$this->form_validation->set_rules('telefone2', 'telefone 2', 'trim');		
	
		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			/*$detalhes = array(
				'nome' => $this->input->post('nome'),
				'email' => $this->input->post('email'),
				'telefone1' => $this->input->post('telefone1'),
				'telefone2' => $this->input->post('telefone2'),
			);*/


			$arrayEvento = array(
				'status' => $this->input->post("status"),
				'locacao_evento' => 'L',
				'nome_evento' => 'Locação',
				'data_inicio' => $this->input->post("data_inicio"),
				'data_final' => $this->input->post("data_final"),
				'tipo_evento' => $this->input->post("tipo_evento"),
				'dados_evento' => $this->input->post("comentario"),
				'valor' => $this->input->post("valor"),
				'id_centro_comunitario' => $this->input->post("id_centro_comunitario"),
				'id_responsavel' => $this->input->post("id_responsavel"),
				//'detalhes' => json_encode($detalhes)
			);

			$id_evento = $this->eventomodel->inserir($arrayEvento);

			//unset($arrayEvento['detalhes']);
			unset($arrayEvento['locacao_evento']);
			unset($arrayEvento['nome_evento']);

			//$arrayEvento = array_merge($arrayEvento, $detalhes);

			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Inserido com sucesso!',
					'evento' => $arrayEvento,
					'id_evento' => $id_evento
				)
			);
			echo json_encode($rps);
		}
	}

	public function listar($id_conta=null){
		$this->authsession->valida('locacao', 'read');
		$this->load->model("eventomodel");
		$this->load->model("responsavelmodel");

		$busca = array();
		if(!$this->id_adm){
			$this->load->model("associacaomodel");
			$associacao = $this->associacaomodel->listar($this->id_associacao)[0];

			$busca['evento.id_centro_comunitario'] = $associacao->id_centro_comunitario;
		}

		$busca['locacao_evento'] = 'L';

		$locacao = $this->eventomodel->listar($id_conta, $busca);
		$locacaoA = array();

		foreach ($locacao as $loc) {
			$responsavel = $this->responsavelmodel->listar($loc->id_responsavel)[0];

			$loc->responsavel_nome = $responsavel->nome;
			$loc->responsavel_cpf = $responsavel->cpf;

			unset($loc->locacao_evento);
			unset($loc->nome_evento);
			unset($loc->descricao);

			$locacaoA[] = $loc;
		}

		$rps = array(
			'status' => true,
			'obj' => array(
				'locacao' => $locacaoA
			)
		);
		echo json_encode($rps);
	}

	public function atualizar(){
		$this->authsession->valida('locacao', 'update');
		$this->load->model("eventomodel");

		$this->load->library("form_validation");
		$this->form_validation->set_rules('data_inicio', 'data inicial', 'trim|required');		
		$this->form_validation->set_rules('data_final', 'data termino', 'trim|required');		
		$this->form_validation->set_rules('tipo_evento', 'tipo do evento', 'trim|required');		
		$this->form_validation->set_rules('comentario', 'comentário', 'trim');		
		$this->form_validation->set_rules('valor', 'valor', 'trim');		
		$this->form_validation->set_rules('id_centro_comunitario', 'id centro comunitário', 'trim|required');		
		$this->form_validation->set_rules('status', 'status (A, I, P)', 'trim|required');	


		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_evento = $this->input->post("id_evento");
			$evento = $this->eventomodel->listar($id_evento);

			if(count($evento)>0){

				$arrayAtualiza = array(
					'status' => $this->input->post("status"),
					'data_inicio' => $this->input->post("data_inicio"),
					'data_final' => $this->input->post("data_final"),
					'tipo_evento' => $this->input->post("tipo_evento"),
					'dados_evento' => $this->input->post("comentario"),
					'valor' => $this->input->post("valor"),
					'id_centro_comunitario' => $this->input->post("id_centro_comunitario"),
					'id_responsavel' => $this->input->post("id_responsavel"),
					//'locacao_evento' => 'L',
					//'nome_evento' => 'Locação',
					//'detalhes' => json_encode($detalhes)
				);


				$this->eventomodel->atualizar($id_evento, $arrayAtualiza);

				$rps = array(
					'status' => true,
					'message' => 'Atualizado com sucesso!',
					'obj' => $arrayAtualiza,
					'id_evento' => $id_evento
				);
				echo json_encode($rps);

			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Evento não existe!'
				);
				echo json_encode($rps);
			}
		}
	}


	public function deletar(){
		$this->authsession->valida('evento', 'delete');
		$this->load->model("eventomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_evento', 'id do evento', 'trim|required');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_evento = $this->input->post("id_evento");
			$evento = $this->eventomodel->listar($id_evento);

			if(count($evento)>0){
				//echo 'usuário existe';
				$this->eventomodel->deletar($id_evento);

				$rps = array(
					'status' => true,
					'message' => 'Deletado com sucesso!',
					'id_evento' => $id_evento
				);
				echo json_encode($rps);

			} else {
				//echo 'usuário não existe';
				$rps = array(
					'status' => false,
					'erro' => 'Evento não existe!'
				);
				echo json_encode($rps);
			}
		}
	}


}
