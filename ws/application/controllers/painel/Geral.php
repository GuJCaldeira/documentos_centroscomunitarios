<?php
class Geral extends CI_Controller {

	public function listarCentros(){
		$this->load->model("centro_comunitariomodel");

		//TODO SEPARAR AS MINHAS (DO USUARIO)
		$lista = $this->centro_comunitariomodel->listar();

		$rps = array(
			'status' => true,
			'obj' => $lista
		);
		echo json_encode($rps);
	}

	public function listarAssociacoes(){
		$this->load->model("associacaomodel");

		//TODO SEPARAR AS MINHAS (DO USUARIO)
		$lista = $this->associacaomodel->listar();

		$rps = array(
			'status' => true,
			'obj' => $lista
		);
		echo json_encode($rps);
	}
}