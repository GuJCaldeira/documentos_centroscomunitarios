<?php
class Conta extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}

	public function inserir() {
		$this->authsession->valida('conta', 'write');

		$this->load->model("contamodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome_conta', 'descrição da conta', 'trim|required');
		$this->form_validation->set_rules('id_associacao', 'id da associação', 'trim');		
		$this->form_validation->set_rules('status', 'status da conta', 'trim|required');		
	
		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {

			$conta = $this->contamodel->listar(null, array(
				'nome_conta' => $this->input->post("nome_conta"),
				'id_associacao' => $this->input->post("id_associacao")
			));

			if(count($conta)==0){
				$arrayConta = array(
					'nome_conta' => $this->input->post("nome_conta"),
					'id_associacao' => $this->input->post("id_associacao"),				
					'status' => $this->input->post("status"),
				);

				$id_conta = $this->contamodel->inserir($arrayConta);
				$rps = array(
					'status' => true,
					'obj' => array(
						'message' => 'Inserido com sucesso!',
						'conta' => $arrayConta,
						'id_conta' => $id_conta
					)
				);
			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Essa associação já possui um grupo de conta chamada '.
						$this->input->post("nome_conta").'. Utilize outro nome'
				);
			}
			echo json_encode($rps);
		}
	}

	public function listar($id_conta=null){
		$this->authsession->valida('conta', 'read');
		$this->load->model("contamodel");

		$busca = array();
		if(!$this->id_adm)
			$busca['id_associacao'] = $this->id_associacao;

		$conta = $this->contamodel->listar($id_conta, $busca);

		$rps = array(
			'status' => true,
			'obj' => array(
				'conta' => $conta
			)
		);
		echo json_encode($rps);
	}

	public function atualizar(){
		$this->authsession->valida('conta', 'update');
		$this->load->model("contamodel");
		$this->load->model("caixamodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome_conta', 'descrição da conta', 'trim|required');
		$this->form_validation->set_rules('id_associacao', 'id da associação', 'trim');		
		$this->form_validation->set_rules('status', 'status da conta', 'trim|required');	
	
		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_conta = $this->input->post("id_conta");
			$conta = $this->contamodel->listar($id_conta);
			$caixa = $this->caixamodel->listarconta($id_conta);
			if(count($caixa)==0){
				$contaBusca = $this->contamodel->listar(null, array(
					'nome_conta' => $this->input->post("nome_conta"),
					'id_associacao' => $this->input->post("id_associacao")
				));
	
				if(count($contaBusca)==0){
					if(count($conta)>0){
						$arrayAtualiza = array(
						'nome_conta' => $this->input->post("nome_conta"),
						'id_associacao' => $this->input->post("id_associacao"),
						'status' => $this->input->post("status"),
						);

						$this->contamodel->atualizar($id_conta, $arrayAtualiza);

						$rps = array(
							'status' => true,
							'message' => 'Atualizado com sucesso!',
							//'contaBusca' => $contaBusca,
							'obj' => $arrayAtualiza,
							'id_conta' => $id_conta
						);
						echo json_encode($rps);

					} else {
						$rps = array(
							'status' => false,
							'erro' => 'Grupo de contas não existe!'
						);
						echo json_encode($rps);
					}
				} else {
					$rps = array(
						'status' => false,
						'erro' => 'Essa associação já possui um grupo de conta chamada '.
							$this->input->post("nome_conta").'. Utilize outro nome'
					);

					echo json_encode($rps);
				}
			} else {
					//echo 'usuário não existe';
					$rps = array(
						'status' => false,
						'erro' => 'Você não pode alterar o nome de um grupo de contas que já possua movimentação!'
					);
					echo json_encode($rps);
			}
		}
	}

	public function deletar(){
		$this->authsession->valida('conta', 'delete');
		$this->load->model("contamodel");
		$this->load->model("caixamodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_conta', 'id da conta', 'trim|required');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_conta = $this->input->post("id_conta");
			$conta = $this->contamodel->listar($id_conta);
			$caixa = $this->caixamodel->listarconta($id_conta);
			if(count($caixa)==0){
				if(count($conta)>0){
					$this->contamodel->deletar($id_conta);

					$rps = array(
						'status' => true,
						'message' => 'Deletado com sucesso!',
						'id_conta' => $id_conta
					);
					echo json_encode($rps);

				} else {
					$rps = array(
						'status' => false,
						'erro' => 'Grupo de contas não existe!'
					);
					echo json_encode($rps);
				}
			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Não pode deletar um grupo de contas que já esteja em uso!'
				);
				echo json_encode($rps);
			}
			


		}
	}

}