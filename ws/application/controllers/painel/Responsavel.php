<?php
class Responsavel extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}

	public function inserir() {
		$this->authsession->valida('responsavel', 'write');

		$this->load->model("responsavelmodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_associacao', 'associação', 'trim|required');
		$this->form_validation->set_rules('status', 'status do responsavel', 'trim|required');
		$this->form_validation->set_rules('nome', 'nome do responsavel', 'trim');
		$this->form_validation->set_rules('cpf', 'CPF do responsavel', 'trim');
		$this->form_validation->set_rules('email', 'e-mail do responsavel', 'trim|valid_email');
		$this->form_validation->set_rules('telefone1', 'telefone do responsavel', 'trim|required');
		$this->form_validation->set_rules('telefone2', 'celular do responsavel', 'trim');
		$this->form_validation->set_rules('cep', 'CEP do responsavel', 'trim|required');
		$this->form_validation->set_rules('endereco', 'endereco do responsavel', 'trim|required');
		$this->form_validation->set_rules('numero', 'número do responsavel', 'trim|required');
		$this->form_validation->set_rules('bairro', 'bairro do responsavel', 'trim|required');
		$this->form_validation->set_rules('complemento', 'complemento do endereço responsavel', 'trim');
		$this->form_validation->set_rules('cidade', 'cidade do responsavel', 'trim|required');
		$this->form_validation->set_rules('estado', 'Estado do responsavel', 'trim|required');
		$this->form_validation->set_rules('detalhes', 'detalhes do responsavel', 'trim');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$senha = password_hash($this->input->post("senha"), PASSWORD_DEFAULT);

			$arrayResponsavel = array(
				'id_associacao' => $this->input->post("id_associacao"),
				'nome' => $this->input->post("nome"),
				'cpf' => $this->input->post("cpf"),
				'email' => $this->input->post("email"),
				'telefone1' => $this->input->post("telefone1"),
				'telefone2' => $this->input->post("telefone2"),				
				'cep' => $this->input->post("cep"),
				'endereco' => $this->input->post("endereco"),
				'numero' => $this->input->post("numero"),
				'bairro' => $this->input->post("bairro"),
				'complemento' => $this->input->post("complemento"),
				'cidade' => $this->input->post("cidade"),
				'estado' => $this->input->post("estado"),
				'detalhes' => $this->input->post("detalhes"),
				'status' => $this->input->post("status")
			);

			$id_responsavel = $this->responsavelmodel->inserir($arrayResponsavel);
			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Inserido com sucesso!',
					'responsavel' => $arrayResponsavel,
					'id_responsavel' => $id_responsavel
				)
			);
			echo json_encode($rps);
		}
	}

	public function listar($id_responsavel=null){
		$this->authsession->valida('responsavel', 'read');
		$this->load->model("responsavelmodel");

		$busca = array();
		if(!$this->id_adm)
			$busca['id_associacao'] = $this->id_associacao;

		$responsavel = $this->responsavelmodel->listar($id_responsavel, $busca);

		$rps = array(
			'status' => true,
			'obj' => array(
				'responsavel' => $responsavel
			)
		);
		echo json_encode($rps);
	}

	public function atualizar(){
		$this->authsession->valida('responsavel', 'update');
		$this->load->model("responsavelmodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_associacao', 'associação', 'trim|required');
		$this->form_validation->set_rules('status', 'status do responsavel', 'trim|required');
		$this->form_validation->set_rules('nome', 'nome do responsavel', 'trim');
		$this->form_validation->set_rules('cpf', 'CPF do responsavel', 'trim');
		$this->form_validation->set_rules('email', 'e-mail do responsavel', 'trim|valid_email');
		$this->form_validation->set_rules('telefone1', 'telefone do responsavel', 'trim|required');
		$this->form_validation->set_rules('telefone2', 'celular do responsavel', 'trim');
		$this->form_validation->set_rules('cep', 'CEP do responsavel', 'trim|required');
		$this->form_validation->set_rules('endereco', 'endereco do responsavel', 'trim|required');
		$this->form_validation->set_rules('numero', 'número do responsavel', 'trim|required');
		$this->form_validation->set_rules('bairro', 'bairro do responsavel', 'trim|required');
		$this->form_validation->set_rules('complemento', 'complemento do endereço responsavel', 'trim');
		$this->form_validation->set_rules('cidade', 'cidade do responsavel', 'trim|required');
		$this->form_validation->set_rules('estado', 'Estado do responsavel', 'trim|required');
		$this->form_validation->set_rules('detalhes', 'detalhes do responsavel', 'trim');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_responsavel = $this->input->post("id_responsavel");
			$responsavel = $this->responsavelmodel->listar($id_responsavel);

			if(count($responsavel)>0){
				$arrayAtualiza = array(
				'id_associacao' => $this->input->post("id_associacao"),
				'status' => $this->input->post("status"),
				'cpf' => $this->input->post("cpf"),
				'email' => $this->input->post("email"),
				'telefone1' => $this->input->post("telefone1"),
				'telefone2' => $this->input->post("telefone2"),				
				'cep' => $this->input->post("cep"),
				'endereco' => $this->input->post("endereco"),
				'numero' => $this->input->post("numero"),
				'bairro' => $this->input->post("bairro"),
				'complemento' => $this->input->post("complemento"),
				'cidade' => $this->input->post("cidade"),
				'estado' => $this->input->post("estado"),
				'detalhes' => $this->input->post("detalhes"),
				);

				$this->responsavelmodel->atualizar($id_responsavel, $arrayAtualiza);

				$rps = array(
					'status' => true,
					'message' => 'Atualizado com sucesso!',
					'obj' => $arrayAtualiza,
					'id_responsavel' => $id_responsavel
				);
				echo json_encode($rps);

			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Responsavel não existe!'
				);
				echo json_encode($rps);
			}
		}
	}

	public function deletar(){
		$this->authsession->valida('responsavel', 'delete');
		$this->load->model("responsavelmodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_responsavel', 'id do responsavel', 'trim|required');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_responsavel = $this->input->post("id_responsavel");
			$responsavel = $this->responsavelmodel->listar($id_responsavel);

			if(count($responsavel)>0){
				//echo 'usuário existe';
				$this->responsavelmodel->deletar($id_responsavel);

				$rps = array(
					'status' => true,
					'message' => 'Deletado com sucesso!',
					'id_responsavel' => $id_responsavel
				);
				echo json_encode($rps);

			} else {
				//echo 'usuário não existe';
				$rps = array(
					'status' => false,
					'erro' => 'Responsavel não existe!'
				);
				echo json_encode($rps);
			}


		}
	}

}