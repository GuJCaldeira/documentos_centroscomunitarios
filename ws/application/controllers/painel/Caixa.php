<?php
class Caixa extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}
	
	public function inserir() {
		$this->authsession->valida('caixa', 'write');

		$this->load->model("caixamodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('descricao', 'descrição do lançamento', 'trim|required');
		$this->form_validation->set_rules('valor', 'valor do lançamento', 'trim|required');
		$this->form_validation->set_rules('id_associacao', 'associacao da conta', 'trim|required');
		$this->form_validation->set_rules('id_conta', 'identificação da conta', 'trim|required');
		$this->form_validation->set_rules('data_lancamento', 'data do lançamento', 'trim|required');
		$this->form_validation->set_rules('movimento', 'movimento', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
		
			$arrayCaixa = array(
				'descricao' => $this->input->post("descricao"),
				'valor' => $this->input->post("valor"),
				'id_conta' => $this->input->post("id_conta"),
				'id_associacao' => $this->input->post("id_associacao"),
				'data_lancamento' => $this->input->post("data_lancamento"),
				'movimento' => $this->input->post("movimento"),
			);
			$id_caixa = $this->caixamodel->inserir($arrayCaixa);

			/* Tratar parte de saldo - inicio */
			$this->load->model("associacaomodel");			
			$associacao = $this->associacaomodel->listar($this->input->post("id_associacao"))[0];
			if($arrayCaixa['movimento']=='E'){
				$aAssociacao['saldo'] = $associacao->saldo + $this->input->post("valor");
			} else {
				$aAssociacao['saldo'] = $associacao->saldo - $this->input->post("valor");
			}
			$this->associacaomodel->atualizar($this->input->post("id_associacao"), $aAssociacao);
			/* Tratar parte de saldo - final */

			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Inserido com sucesso!',
					'caixa' => $arrayCaixa,
					'id_caixa' => $id_caixa
				)
			);
			echo json_encode($rps);
		}
	}

	public function listar($id_caixa=null){
		$this->authsession->valida('caixa', 'read');
		$this->load->model("caixamodel");

		$busca = array();
		$exibeSaldo = false;
		$saldo = 0.00;
		if(!$this->id_adm){
			$busca['id_associacao'] = $this->id_associacao;
			$exibeSaldo = true;
			$saldo = $associacao = $this->associacaomodel->listar($this->id_associacao)[0]->saldo;
		}

		$caixa = $this->caixamodel->listar($id_caixa, $busca);

		$rps = array(
			'status' => true,
			'obj' => array(
				'caixa' => $caixa,
				'saldo' => $saldo,
				'exibeSaldo' => $exibeSaldo
			)
		);
		echo json_encode($rps);
	}

	public function geraRelatorio(){
		//$this->authsession->valida('caixa', 'read');
		$this->adm = true;
		$this->load->model("caixamodel");

		$id_conta = $this->input->post("id_conta");
		$id_associacao = $this->input->post("id_associacao");
		$data_inicial = $this->input->post("data_inicial");
		$data_final = $this->input->post("data_final");
		$movimento = $this->input->post("movimento");
		$val_min = $this->input->post("val_min");
		$val_max = $this->input->post("val_max");

		//var_dump($this->input->post());

		$busca = array();
	
		if(!$this->id_adm){
			//$busca['caixa.id_associacao'] = $this->id_associacao;
		}


		if(!is_null($id_associacao) && $id_associacao!='')
			$busca['caixa.id_associacao'] = $id_associacao;

		if(!is_null($id_conta) && $id_conta!=''){
			$busca['caixa.id_conta'] = $id_conta;
			$this->load->model("contamodel");
			$data['conta'] = $this->contamodel->listar($id_conta)[0];
		}

		if(!is_null($data_inicial) && $data_inicial!='')
			$busca['caixa.data_lancamento>='] = $data_inicial;

		if(!is_null($data_final) && $data_final!='')
			$busca['caixa.data_lancamento<='] = $data_final;
		
		if(!is_null($movimento) && $movimento!='')
			$busca['caixa.movimento'] = $movimento;

		if(!is_null($val_min) && $val_min!='')
			$busca['caixa.valor>='] = $val_min * 1;

		if(!is_null($val_max) && $val_max!='')
			$busca['caixa.valor<='] = $val_max * 1;

		$caixa = $this->caixamodel->listar(null, $busca);

		//var_dump($busca);
		//var_dump($caixa);
		
		$data['movimentacoes'] = $caixa;
		$data['busca'] = $busca;

		$pagina = $this->load->view("caixa_relatorio", $data, true);

		$this->load->library("mpdf");
		$cfg = array(
			'mode' => 'c',
			'margin_left' => 12,
			'margin_right' => 12,
			'margin_top' => 8,
			'margin_bottom' => 12,
			'margin_header' => 0,
			'margin_footer' => 0
		);
		$pdf = $this->mpdf->load($cfg);
		$pdf->WriteHTML($pagina);
		$pdf->Output();
	}

	
}