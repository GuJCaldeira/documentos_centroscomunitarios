<?php
class Usuario_grupo extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}


	public function listarPermissoes(){
		$this->authsession->valida('usuario_grupo','read');

		$permissoesDisponiveis = $this->authsession->permissoesDisponiveis();
		$permissoesDisponiveisA = array();
		foreach ($permissoesDisponiveis as $pd) {
			$pdA = array();
			foreach($pd['permissoes'] as $p){
				$p['checked'] = true;
				$pdA[] = $p;
			}
			$pd['permissoes'] = $pdA;

			$permissoesDisponiveisA[] = $pd;
		}

		$rps = array(
			'status' => true,
			'permissoes' => $permissoesDisponiveisA
		);
		
		echo json_encode($rps);
	}

	public function permissao($id){
		$this->authsession->valida('usuario_grupo','read');

		$grupo = $this->grupomodel->listar($id)[0];
		$gj = json_decode($grupo->json);
		$gj = json_decode(json_encode($gj), true);

		$permissoesDisponiveis = $this->authsession->permissoesDisponiveis();
		$permissoesDisponiveisA = array();
		foreach ($permissoesDisponiveis as $pd) {
			$pdA = array();
			foreach($pd['permissoes'] as $p){
				$perm = $gj[$pd['id']][$p['id']];
				if(is_string($perm) && $perm=='true') $perm = true;
				if(is_string($perm) && $perm=='false') $perm = false;

				$p['checked'] = $perm;
				$pdA[] = $p;
			}
			$pd['permissoes'] = $pdA;

			$permissoesDisponiveisA[] = $pd;
		}

		//var_dump($grupo);
		$rps = $grupo;
		unset($grupo->json);
		$grupo->permissoes = $permissoesDisponiveisA;
		
		echo json_encode($rps);
	}

	public function inserir(){
		$this->authsession->valida('usuario_grupo','write');

		$this->load->model("grupomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_associacao', 'associação', 'trim|required');	
		$this->form_validation->set_rules('nome_grupo', 'nome do grupo', 'trim|required');	

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
		} else {
			$post = $this->input->post();

			$permT = array();
			foreach ($post['modules'] as $mol) {
				//$modules[] = $mol['name'];
				$aPerm = array();
				//var_dump($mol['permissions']);

				foreach($mol['permissions'] as $perm){
					$aPerm[$perm['id']] = $perm['checked'];
				}

				$permT[$mol['id']] = $aPerm;
			}

			$arrayGrupo = array(
				'nome_grupo' => $this->input->post("nome_grupo"),
				'json' => json_encode($permT),
				'id_associacao' => $this->input->post("id_associacao"),
				'status' => $this->input->post("status"),
			);

			$id_grupo = $this->grupomodel->inserir($arrayGrupo);

			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Inserido com sucesso!',
					'grupo' => $arrayGrupo,
					'id_grupo' => $id_grupo
				)
			);
		}
		echo json_encode($rps);
	}

	public function listar($id=null){
		$this->authsession->valida('usuario_grupo','read');
		$this->load->model("grupomodel");

		$busca = array();
		if(!$this->id_adm)
			$busca['id_associacao'] = $this->id_associacao;

		$grupos = $this->grupomodel->listar($id, $busca);

		$rps = array(
			'status' => true,
			'obj' => array(
				'grupos' => $grupos
			)
		);
		echo json_encode($rps);
	}

	public function atualizar(){
		//$this->authsession->valida('usuario_grupo','read');
		$this->authsession->valida('usuario_grupo','update');

		$this->load->model("grupomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_associacao', 'associação', 'trim|required');	
		$this->form_validation->set_rules('id_usuario_grupo', 'associação', 'trim|required');	
		$this->form_validation->set_rules('nome_grupo', 'nome do grupo', 'trim|required');	

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
		} else {
			$post = $this->input->post();
			$id_grupo = $this->input->post('id_usuario_grupo');

			$permT = array();
			foreach ($post['modules'] as $mol) {
				//$modules[] = $mol['name'];
				$aPerm = array();
				//var_dump($mol['permissions']);

				foreach($mol['permissions'] as $perm){
					$aPerm[$perm['id']] = $perm['checked'];
				}

				$permT[$mol['id']] = $aPerm;
			}

			$arrayGrupo = array(
				'nome_grupo' => $this->input->post("nome_grupo"),
				'json' => json_encode($permT),
				'id_associacao' => $this->input->post("id_associacao"),
				'status' => $this->input->post("status"),
			);

			$this->grupomodel->atualizar($id_grupo, $arrayGrupo);

			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Atualizado com sucesso!',
					'grupo' => $arrayGrupo,
					'id_grupo' => $id_grupo
				)
			);
		}
		echo json_encode($rps);
	}

	public function deletar(){
		$this->authsession->valida('usuario_grupo','read');

		$this->load->model("grupomodel");
		$this->load->model("usuariomodel");

		$id_grupo = $this->input->post('id_grupo');

		//Localizar usuário grupo
		$grupos = $this->grupomodel->listar($id_grupo);

		if(count($grupos)==1 && $id_grupo!='' && !is_null($id_grupo)){

			$usuarios = $this->usuariomodel->listar(null, array('id_grupo' => $id_grupo));
			if(count($usuarios)==0) {
				
				$this->grupomodel->deletar($id_grupo);
				
				$rps = array(
					'status' => true,
					'message' => 'Deletado com sucesso!',
					'obj' => array()
				);
			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Existe '.count($usuarios).' usuário que pertence a este grupo de permissão.',
				);
			}
		} else {
			$rps = array(
				'status' => false,
				'erro' => 'Grupo não existe!'
			);
		}

		echo json_encode($rps);
	}


	
}
