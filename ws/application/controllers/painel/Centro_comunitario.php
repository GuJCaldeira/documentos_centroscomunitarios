<?php
class Centro_comunitario extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}

	public function inserir() {
		$this->authsession->valida('centro_comunitario', 'write');

		$this->load->model("centro_comunitariomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome_centro', 'nome do centro', 'trim|required');
		$this->form_validation->set_rules('status', 'status do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('telefone1', 'telefone do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('telefone2', 'celular do centro comunitário', 'trim');
		$this->form_validation->set_rules('detalhes', 'detalhes do centro comunitário', 'trim');
		$this->form_validation->set_rules('cep', 'CEP da associação', 'trim|required');
		$this->form_validation->set_rules('endereco', 'endereço da associação', 'trim|required');
		$this->form_validation->set_rules('numero', 'número do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('bairro', 'bairro do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('complemento', 'complemento do endereço do centro comunitário', 'trim');
		$this->form_validation->set_rules('cidade', 'cidade do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('estado', 'Estado do centro comunitário', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$senha = password_hash($this->input->post("senha"), PASSWORD_DEFAULT);

			$arrayCentrocomunitario = array(
				'nome_centro' => $this->input->post("nome_centro"),
				'status' => $this->input->post("status"),
				'telefone1' => $this->input->post("telefone1"),
				'telefone2' => $this->input->post("telefone2"),
				'detalhes' => $this->input->post("detalhes"),
				'cep' => $this->input->post("cep"),
				'endereco' => $this->input->post("endereco"),
				'numero' => $this->input->post("numero"),
				'bairro' => $this->input->post("bairro"),
				'complemento' => $this->input->post("complemento"),
				'cidade' => $this->input->post("cidade"),
				'estado' => $this->input->post("estado"),
			);

			$id_centro = $this->centro_comunitariomodel->inserir($arrayCentrocomunitario);
			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Inserido com sucesso!',
					'centro_comunitario' => $arrayCentrocomunitario,
					'id_centro' => $id_centro
				)
			);
			echo json_encode($rps);
		}
	}

	public function listar($id_centro=null){
		$this->authsession->valida('centro_comunitario', 'read');
		$this->load->model("centro_comunitariomodel");

		$busca = array();
		if(!$this->id_adm){
			$this->load->model("associacaomodel");
			$associacao = $this->associacaomodel->listar($this->id_associacao)[0];

			$busca['id_centro_comunitario'] = $associacao->id_centro_comunitario;
		}

		$centro_comunitario = $this->centro_comunitariomodel->listar($id_centro, $busca);

		$rps = array(
			'status' => true,
			'obj' => array(
				'centro_comunitario' => $centro_comunitario
			)
		);
		echo json_encode($rps);
	}

	public function atualizar(){
		$this->authsession->valida('centro_comunitario', 'update');
		$this->load->model("centro_comunitariomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('nome_centro', 'nome do centro', 'trim|required');
		$this->form_validation->set_rules('status', 'status do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('telefone1', 'telefone do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('telefone2', 'celular do centro comunitário', 'trim');
		$this->form_validation->set_rules('detalhes', 'detalhes do centro comunitário', 'trim');
		$this->form_validation->set_rules('cep', 'CEP da associação', 'trim|required');
		$this->form_validation->set_rules('endereco', 'endereço da associação', 'trim|required');
		$this->form_validation->set_rules('numero', 'número do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('bairro', 'bairro do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('complemento', 'complemento do endereço do centro comunitário', 'trim');
		$this->form_validation->set_rules('cidade', 'cidade do centro comunitário', 'trim|required');
		$this->form_validation->set_rules('estado', 'Estado do centro comunitário', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_centro = $this->input->post("id_centro");
			$centro_comunitario = $this->centro_comunitariomodel->listar($id_centro);

			if(count($centro_comunitario)>0){
				$arrayAtualiza = array(
				'nome_centro' => $this->input->post("nome_centro"),
				'status' => $this->input->post("status"),
				'telefone1' => $this->input->post("telefone1"),
				'telefone2' => $this->input->post("telefone2"),
				'detalhes' => $this->input->post("detalhes"),
				'cep' => $this->input->post("cep"),
				'endereco' => $this->input->post("endereco"),
				'numero' => $this->input->post("numero"),
				'bairro' => $this->input->post("bairro"),
				'complemento' => $this->input->post("complemento"),
				'cidade' => $this->input->post("cidade"),
				'estado' => $this->input->post("estado"),
				);

				$this->centro_comunitariomodel->atualizar($id_centro, $arrayAtualiza);

				$rps = array(
					'status' => true,
					'message' => 'Atualizado com sucesso!',
					'obj' => $arrayAtualiza,
					'id_centro' => $id_centro
				);
				echo json_encode($rps);

			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Centro comunitario não existe!'
				);
				echo json_encode($rps);
			}
		}
	}

	public function deletar(){
		$this->authsession->valida('centro_comunitario', 'delete');
		$this->load->model("centro_comunitariomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_centro', 'id do centro', 'trim|required');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_centro = $this->input->post("id_centro");
			$centro_comunitario = $this->centro_comunitariomodel->listar($id_centro);

			if(count($centro_comunitario)>0){
				//echo 'usuário existe';
				$this->centro_comunitariomodel->deletar($id_centro);

				$rps = array(
					'status' => true,
					'message' => 'Deletado com sucesso!',
					'id_centro' => $id_centro
				);
				echo json_encode($rps);

			} else {
				//echo 'usuário não existe';
				$rps = array(
					'status' => false,
					'erro' => 'Centro comunitario não existe!'
				);
				echo json_encode($rps);
			}


		}
	}
	
}