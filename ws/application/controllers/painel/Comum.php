<?php
class Comum extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}

	public function listarCargos($id_cargo=null){
		$this->authsession->valida('comum','');

		$this->load->model("cargomodel");
		$busca = array();
		if(!$this->id_adm){
			$busca['id_associacao'] = $this->id_associacao;
		}
		$busca['status'] = 'A';

		$cargos = $this->cargomodel->listar($id_cargo, $busca);
		$cargoA = array();

		foreach ($cargos as $cargo) {

			$cargoA[] = array(
				'id_cargo' => $cargo->id_cargo,
				'nome_cargo' => $cargo->nome_cargo,
				'status' => $cargo->status
			);
		}

		$rps = array(
			'status' => true,
			'obj' => $cargoA
		);	
		echo json_encode($rps);	
	}

	public function listarAssociacoes($id_associacao=null){
		$this->authsession->valida('comum','');

		$this->load->model("associacaomodel");
		$busca = array();
		if(!$this->id_adm){
			$busca['id_associacao'] = $this->id_associacao;
		}
		$busca['status'] = 'A';

		$associacoes = $this->associacaomodel->listar($id_associacao, $busca);
		$associacaoA = array();

		foreach ($associacoes as $associacao) {

			$associacaoA[] = array(
				'id_associacao' => $associacao->id_associacao,
				'razao_social' => $associacao->razao_social,
				'status' => $associacao->status
			);
		}

		$rps = array(
			'status' => true,
			'obj' => array(
				'associacoes' => $associacaoA
			)
		);	
		echo json_encode($rps);	
	}
	public function listarContas($id_conta=null){
		$this->authsession->valida('comum','');
		$this->load->model("contamodel");
		$busca = array();
		if(!$this->id_adm){
			$busca['id_associacao'] = $this->id_associacao;
		}
		$busca['status'] = 'A';

		$contas = $this->contamodel->listar($id_conta, $busca);
		//var_dump($contas);

		$contasA = array();

		foreach ($contas as $conta) {

			$contasA[] = array(
				'id_conta' => $conta->id_conta,
				'nome_conta' => $conta->nome_conta,
				'status' => $conta->status
			);
		}

		$rps = array(
			'status' => true,
			'obj' => array(
				'conta' => $contasA
			)
		);
		echo json_encode($rps);
	}
	public function listarResponsaveis($id_responsavel=null){
		$this->authsession->valida('comum','');
		$this->load->model("responsavelmodel");
		$busca = array();
		if(!$this->id_adm){
			$busca['id_associacao'] = $this->id_associacao;
		}
		$busca['status'] = 'A';

		$responsaveis = $this->responsavelmodel->listar($id_responsavel, $busca);
		$responsaveisA = array();

		foreach ($responsaveis as $responsavel) {

			$responsaveisA[] = array(
				'id_responsavel' => $responsavel->id_responsavel,
				'nome' => $responsavel->nome,
				'status' => $responsavel->status
			);
		}

		$rps = array(
			'status' => true,
			'obj' => array(
				'responsavel' => $responsaveisA
			)
		);
		echo json_encode($rps);
	}
	public function listarCentros($id_centro=null){
		$this->authsession->valida('comum','');
		$this->load->model("centro_comunitariomodel");
		$busca = array();
		if(!$this->id_adm){
			$this->load->model("associacaomodel");
			$associacao = $this->associacaomodel->listar($this->id_associacao)[0];

			$busca['id_centro_comunitario'] = $associacao->id_centro_comunitario;
		}
		$busca['status'] = 'A';

		$centros = $this->centro_comunitariomodel->listar($id_centro, $busca);
		$centrosA = array();
		
		foreach($centros as $centro){
			$centrosA[] = array(
				'id_centro_comunitario' => $centro->id_centro_comunitario,
				'nome_centro' => $centro->nome_centro,
				'status' => $centro->status
			);

		}

		$rps = array(
			'status' => true,
			'obj' => array(
				'centro_comunitario' => $centrosA
			)
		);
		echo json_encode($rps);
	}
	public function listarGrupos($id=null){
		$this->authsession->valida('comum','');
		$this->load->model("grupomodel");

		$busca = array();
		if(!$this->id_adm)
			$busca['id_associacao'] = $this->id_associacao;

		$grupos = $this->grupomodel->listar($id, $busca);
		$gruposA = array();

		foreach ($grupos as $grupo) {
			$gruposA[] = array(
				'id_grupo' => $grupo->id_grupo,
				'nome_grupo' => $grupo->nome_grupo,
				'status' => $grupo->status
			);
		}

		$rps = array(
			'status' => true,
			'obj' => array(
				'grupos' => $gruposA
			)
		);
		echo json_encode($rps);
	}


}
