<?php
class Cargo extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}

	public function inserir(){
		$this->authsession->valida('cargo','write');
		$this->load->model("cargomodel");
		$this->load->library("form_validation");
				
		$this->form_validation->set_rules('id_associacao','associação','trim|required');
		$this->form_validation->set_rules('nome_cargo','nome do cargo','trim|required');
		$this->form_validation->set_rules('detalhes','detalhes do cargo','trim');
		$this->form_validation->set_rules('status','status do cargo','trim');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			
			echo json_encode($rps);

		}else{
			$arraycargo = array(
				'id_associacao' => $this->input->post("id_associacao"),
				'nome_cargo' => $this->input->post("nome_cargo"),
				'detalhes' => $this->input->post("detalhes"),
				'status' => $this->input->post("status")
			);
			//var_dump($arrayusuario);
			$id_cargo = $this->cargomodel->inserir($arraycargo);
			//echo $id_cargo;
			$rps = array(
				'status' => true,
				'message' => 'Inserido com sucesso',
				'obj' => $arraycargo,
				'id_cargo' => $id_cargo
			);
			echo json_encode($rps);
		}		
	}

	public function listar($id_cargo=null){
		$this->authsession->valida('cargo','read');
		$this->load->model("cargomodel");

		$busca = array();
		if(!$this->id_adm)
			$busca['id_associacao'] = $this->id_associacao;
		
		$cargo = $this->cargomodel->listar($id_cargo, $busca);

		$rps = array(
			'status' => true,
			'obj' => $cargo
		);	
		echo json_encode($rps);	
	}

	public function atualizar(){
		$this->authsession->valida('cargo','update');
		$this->load->model("cargomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_cargo','id do cargo','trim|required');	
		$this->form_validation->set_rules('id_associacao','associação','trim|required');
		$this->form_validation->set_rules('nome_cargo','nome do cargo','trim|required');
		$this->form_validation->set_rules('detalhes','detalhes do cargo','trim');
		$this->form_validation->set_rules('status','status do cargo','trim');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			
			echo json_encode($rps);

		}else {
			$id_cargo = $this->input->post("id_cargo");
			$cargo = $this->cargomodel->listar($id_cargo);
			if (count($cargo)>0) {
				//echo 'usuario existe';
				$arrayatualiza = array(
					'id_associacao' => $this->input->post("id_associacao"),
					'nome_cargo' => $this->input->post("nome_cargo"),
					'detalhes' => $this->input->post("detalhes"),
					'status' => $this->input->post("status")
				);

				$this->cargomodel->atualizar($id_cargo, $arrayatualiza);

				$rps = array(
					'status' => true,
					'message' => 'Atualizado com sucesso',
					'obj' => $arrayatualiza,
					'id_cargo' => $id_cargo
				);
				echo json_encode($rps);

			}else{
				//'usuario inexistente';
				$rps = array(
				'status' => false,
				'erro' => 'usuário não existe'
			);			
			echo json_encode($rps);
			}
		}
	}

	public function deletar(){
		$this->authsession->valida('cargo','delete');
		$this->load->model("cargomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_cargo','id do cargo','trim|required');	

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			
			echo json_encode($rps);

		}else {
			$id_cargo = $this->input->post("id_cargo");
			$cargo = $this->cargomodel->listar($id_cargo);

			if (count($cargo)>0) {
				$this->cargomodel->deletar($id_cargo);

				$rps = array(
					'status' => true,
					'message' => 'Deletado com sucesso',
					'id_cargo' => $id_cargo
				);
				echo json_encode($rps);

			}else{
				//'usuario inexistente';
				$rps = array(
					'status' => false,
					'erro' => 'cargo não existe'
				);			
				echo json_encode($rps);
			}
		}
	}
}
