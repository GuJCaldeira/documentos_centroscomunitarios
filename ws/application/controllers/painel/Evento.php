<?php
class evento extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->id_usuario = $this->authsession->get_item('id_usuario');
		if($this->id_usuario<0){
			$this->id_adm = true;
		} else {
			$this->id_adm = false;
			$this->id_associacao = $this->authsession->get_item('id_associacao');
		}
	}

	public function inserir() {
		$this->authsession->valida('evento', 'write');

		$this->load->model("eventomodel");
		$this->load->library("form_validation");
		$this->form_validation->set_rules('nome_evento', 'nome do evento', 'trim|required');
		$this->form_validation->set_rules('status', 'status do evento', 'trim|required');
		$this->form_validation->set_rules('descricao', 'descrição do evento', 'trim|required');
		$this->form_validation->set_rules('data_inicio', 'data de início do evento', 'trim|required');
		$this->form_validation->set_rules('data_final', 'data de encerramento do evento', 'trim|required');
		$this->form_validation->set_rules('tipo_evento', 'tipo de evento', 'trim');
		$this->form_validation->set_rules('id_responsavel', 'identificação do responsável', 'trim');
		$this->form_validation->set_rules('dados_evento', 'dados do evento', 'trim');
		$this->form_validation->set_rules('valor', 'valor do evento', 'trim');
		$this->form_validation->set_rules('id_centro_comunitario', 'identificação do centro comunitário', 'trim');
		$this->form_validation->set_rules('detalhes', 'detalhes do evento', 'trim');
		
		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$arrayevento = array(
				'nome_evento' => $this->input->post("nome_evento"),
				'status' => $this->input->post("status"),			
				'descricao' => $this->input->post("descricao"),
				'data_inicio' => $this->input->post("data_inicio"),
				'data_final' => $this->input->post("data_final"),
				'tipo_evento' => $this->input->post("tipo_evento"),
				'id_responsavel' => $this->input->post("id_responsavel"),
				'dados_evento' => $this->input->post("dados_evento"),
				'valor' => $this->input->post("valor"),
				'id_centro_comunitario' => $this->input->post("id_centro_comunitario"),
				'detalhes' => $this->input->post("detalhes"),	

			);

			$id_evento = $this->eventomodel->inserir($arrayevento);
			$rps = array(
				'status' => true,
				'obj' => array(
					'message' => 'Inserido com sucesso!',
					'evento' => $arrayevento,
					'id_evento' => $id_evento
				)
			);
			echo json_encode($rps);
		}
	}
	

	public function listar($id_evento=null){
		$this->authsession->valida('evento', 'read');
		$this->load->model("eventomodel");

		$busca = array();
		if(!$this->id_adm){
			$this->load->model("associacaomodel");
			$associacao = $this->associacaomodel->listar($this->id_associacao)[0];

			$busca['evento.id_centro_comunitario'] = $associacao->id_centro_comunitario;
		}

		$busca['locacao_evento'] = 'E';

		$evento = $this->eventomodel->listar($id_evento, $busca);

		$rps = array(
			'status' => true,
			'obj' => array(
				'evento' => $evento
			)
		);
		echo json_encode($rps);
	}

	public function atualizar(){
		$this->authsession->valida('evento', 'update');
		$this->load->model("eventomodel");
		$this->load->library("form_validation");
		$this->form_validation->set_rules('nome_evento', 'nome do evento', 'trim|required');
		$this->form_validation->set_rules('status', 'status do evento', 'trim|required');
		$this->form_validation->set_rules('descricao', 'descrição do evento', 'trim|required');
		$this->form_validation->set_rules('data_inicio', 'data de início do evento', 'trim|required');
		$this->form_validation->set_rules('data_final', 'data de encerramento do evento', 'trim|required');
		$this->form_validation->set_rules('tipo_evento', 'tipo de evento', 'trim');
		$this->form_validation->set_rules('id_responsavel', 'identificação do responsável', 'trim');
		$this->form_validation->set_rules('dados_evento', 'dados do evento', 'trim');
		$this->form_validation->set_rules('valor', 'valor do evento', 'trim');
		$this->form_validation->set_rules('id_centro_comunitario', 'identificação do centro comunitário', 'trim');
		$this->form_validation->set_rules('detalhes', 'detalhes do evento', 'trim');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_evento = $this->input->post("id_evento");
			$evento = $this->eventomodel->listar($id_evento);

			if(count($evento)>0){

				$arrayAtualiza = array(
					'nome_evento'	  => $this->input->post("nome_evento"),
					'status'	  => $this->input->post("status"),
					'descricao'  	  => $this->input->post("descricao"),
					'data_inicio' 	  => $this->input->post("data_inicio"),
					'data_final'  	  => $this->input->post("data_final"),
					'tipo_evento'	  => $this->input->post("tipo_evento"),
					'id_responsavel'  => $this->input->post("id_responsavel"),
					'dados_evento' 	  => $this->input->post("dados_evento"),
					'valor' 		  => $this->input->post("valor"),
					'id_centro_comunitario' => $this->input->post("id_centro_comunitario"),
					'detalhes' 		  => $this->input->post("detalhes"),
				);


				$this->eventomodel->atualizar($id_evento, $arrayAtualiza);

				$rps = array(
					'status' => true,
					'message' => 'Atualizado com sucesso!',
					'obj' => $arrayAtualiza,
					'id_evento' => $id_evento
				);
				echo json_encode($rps);

			} else {
				$rps = array(
					'status' => false,
					'erro' => 'Evento não existe!'
				);
				echo json_encode($rps);
			}
		}
	}

	public function deletar(){
		$this->authsession->valida('evento', 'delete');
		$this->load->model("eventomodel");
		$this->load->library("form_validation");

		$this->form_validation->set_rules('id_evento', 'id do evento', 'trim|required');

		if($this->form_validation->run()==FALSE){
			//echo validation_errors();
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			echo json_encode($rps);
		} else {
			$id_evento = $this->input->post("id_evento");
			$evento = $this->eventomodel->listar($id_evento);

			if(count($evento)>0){
				//echo 'usuário existe';
				$this->eventomodel->deletar($id_evento);

				$rps = array(
					'status' => true,
					'message' => 'Deletado com sucesso!',
					'id_evento' => $id_evento
				);
				echo json_encode($rps);

			} else {
				//echo 'usuário não existe';
				$rps = array(
					'status' => false,
					'erro' => 'Evento não existe!'
				);
				echo json_encode($rps);
			}
		}
	}
}