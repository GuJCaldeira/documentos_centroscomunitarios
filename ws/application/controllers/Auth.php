<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
  	public function __construct() {
    	parent::__construct();
  	}

	//echo password_hash('123123', PASSWORD_DEFAULT);
	

	public function login(){
		$this->load->model("usuariomodel");
		$this->load->library("form_validation");

		//Regras de validação do formulário
		//$this->form_validation->set_data(((array)$this->utils->post()));
		$this->form_validation->set_rules('cpf', 'cpf do usuário', 'trim|required');
		$this->form_validation->set_rules('senha', 'senha do usuário', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$rps = array(
				'status' => false,
				'erro' => validation_errors()
			);
			//Retornar erro de formulário
			echo json_encode($rps);
		} else {
			//Remover pontuação do usuário
			$cpf   = str_replace(array(".", "-"), "", $this->input->post("cpf"));
			$senha = $this->input->post("senha");

			//Buscar usuário p/ comparar a senha
			$usuarios = $this->usuariomodel->buscar(array('cpf' => $cpf, 'status' => 'A'));

			//var_dump($usuarios);

			//Verificar se o usuário existe
			if(count($usuarios)==1){
				$usuario = $usuarios[0];

				//Usuário existe, comparar a senha
				if(password_verify($senha, $usuario->senha)){
					//Usuário existe, criar sessão
					$novaSessao = array(
						'id_usuario' => $usuario->id_usuario,
						'id_associacao' => $usuario->id_associacao,
						'nome' => $usuario->nome
					);
					$hash = $this->authsession->create($novaSessao);

					//Remover senha do usuário
					unset($usuario->senha);

					//Sessão criada, devolver informações
					$rps = array(
						'status' => true,
						'obj' => array(
							'usuario' => $usuario,
							'id_associacao' => $usuario->id_associacao,
							'sessao' => $hash['hash'],
							'expiration_timestamp' => $hash['expiration_timestamp'],
							'modulos' => $this->authsession->permissoes($usuario->id_usuario)
						)
					);

					echo json_encode($rps);

				} else {
					//Usuário existe, porém, senha inválida!
					$rps = array(
						'status' => false,
						'erro' => 'Usuário ou senha não são válidos.'
					);
					echo json_encode($rps);
				}
			} else {
				//Usuário não existe, retornar erro genérico.
				$rps = array(
					'status' => false,
					'erro' => 'Usuário ou senha não são válidos.'
				);
				echo json_encode($rps);
			}
		}
	}

	public function logout(){
		$this->load->model("authsession");
		$result = $this->authsession->destroy();

		if($result==true){
			$rps = array(
				'status' => true,
				'mensagem' => 'Sucesso!'
			);
		} else {
			$rps = array(
				'status' => false,
				'mensagem' => 'Sessão não localizada'
			);
		}
		echo json_encode($rps);
		
	}
}
